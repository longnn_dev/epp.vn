<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 07-Dec-18
 * Time: 10:58 AM
 */
return [
    'page_type'=>[
        'frontend.home'=> 'Home page',
        'frontend.productCategory'=> 'Product Category',
        'frontend.product.details'=> 'Product Details',
        'frontend.services'=> 'Services',
        'frontend.news'=> 'News',
        'frontend.news.detail.seo' => 'News Details',
        'frontend.contact'=> 'Contact',
        'frontend.about'=> 'About',
        'frontend.partners'=> 'Partners',
        'frontend.partner.detail'=> 'Partners',
        'frontend.cart.partial.product'=> 'Product Details',
    ]
];
