$(document).ready(function () {
    $('#home_slider_gallery img').on('click', function () {
        $('#p_slider_modal').modal('show');
    });

    $("#p_slider_modal").on("shown.bs.modal", function (e) {
        // The slider being synced must be initialized first
        $('#p_carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 110,
            itemMargin: 5,
            asNavFor: '#p_slider'
        });

        $('#p_slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#p_carousel"
        });
    });
});