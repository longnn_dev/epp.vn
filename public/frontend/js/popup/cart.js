$(document).ready(function () {
    $('#modal_cart').on('show.bs.modal', function (e) {
        renderListProductInCart();
    });
    updateCartSize();
    handlerFormSubmit();
    jQuery.validator.addMethod("phone", function (value, element) {
        return this.optional(element) || /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g.test(value);
    }, "Please enter a valid phone number");
});

function handlerFormSubmit() {
    //modal_cart form
    $('#modal_cart form').validate({ // initialize the plugin
        rules: {
            input_name: {
                required: true,
                minlength: 5
            },
            input_email: {
                required: true,
                email: true
            },
            input_phone: {
                required: true,
                phone: true,
            }
        },
        submitHandler: function (form) {
            $('#modal_cart').modal('hide');
            if (countProductInCart() === 0) {
                showMessageDialog('Alert', 'You have no items in the list');
                return;
            }

            axios({
                method: 'post',
                url: '/product/order',
                data: {
                    'products': JSON.parse(sessionStorage.pFavorite),
                    'name': $('#input_name').val(),
                    'email': $('#input_email').val(),
                    'phone': $('#input_phone').val(),
                    'company': $('#input_company').val(),
                    'message': $('#input_message').val(),
                },
                config: {headers: {'Content-Type': 'multipart/form-data'}}
            }).then(function (response) {
                //handle success
                sessionStorage.pFavorite = '';
                updateCartSize();
                showMessageDialog(response.data.title, response.data.message);
            }).catch(function (response) {
                //handle error
                console.log(response);
            });
        },
        invalidHandler: function (event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                    ? 'You missed 1 field. It has been highlighted'
                    : 'You missed ' + errors + ' fields. They have been highlighted';
                $("div.error span").html(message);
                $("div.error").show();
            } else {
                $("div.error").hide();
            }
        }
    });
}

function renderListProductInCart() {
    let $htmlListProduct = '';
    if (sessionStorage.pFavorite) {
        let jArr = JSON.parse(sessionStorage.pFavorite);
        jArr.forEach(function (product, index, arr) {
            $htmlListProduct += "<div class='cart-item'>" +
                "                      <img src='" + product.avatar + "'>" +
                "                      <div style='padding-left: 15px;width: 100%;'>" +
                "                            <p class='theader'>" + product.name + "</p>" +
                "                            <p class='price'>" + product.price + "</p>" +
                "                            <div class='d-flex justify-content-between'>" +
                "                            <p class='price2'>" + product.minOrder + "</p>" +
                "                            <a href='#' class='del'  pid ='" + product.id + "'>Xóa</a>" +
                "                            </div>" +
                "                      </div>" +
                "                </div>";
        });
    }
    $('.list-product').html($htmlListProduct);

    $('.cart-item .del').on('click', function (e) {
        let pid = $(e.target).attr('pid');
        onRemoveProductInCartClick(pid, e.target);
    });
}

function onAddToCartClick(pId, e, detailPage) {
    let index = getProductInCart(pId);
    // noinspection JSValidateTypes
    if (index !== undefined) {
        showMessageDialog('Alert', 'The product already exists in the list of interest');
        return;
    }
    let jArr = [];
    if (sessionStorage.pFavorite) {
        jArr = JSON.parse(sessionStorage.pFavorite);
    }
    //get value of product
    let product = {
        'id': pId
    };
    if (detailPage === undefined || detailPage === false) {
        let nodeParent = $(e).closest('.product-item')[0];
        product.avatar = $(nodeParent).find('.product-item-cover img').attr('src');
        product.name = $(nodeParent).find('.product-item-name').text();
        product.price = $(nodeParent).find('.product-item-price1').text();
        product.minOrder = $(nodeParent).find('.product-item-price2').text();
    } else {
        let nodeParent = $(e).attr('pid').split('|');
        product.avatar = nodeParent[0];
        product.name = nodeParent[1];
        product.price = nodeParent[2];
        product.minOrder = nodeParent[3];
    }

    jArr.push(product);
    sessionStorage.pFavorite = JSON.stringify(jArr);
    showMessageDialog('Alert', 'Added to interest list')
    updateCartSize();
}

function getProductInCart(pId) {
    let index = -1;
    if (sessionStorage.pFavorite) {
        let jArr = JSON.parse(sessionStorage.pFavorite);
        if (jArr.some(function (product, i, arr) {
            if (pId === product.id) {
                index = i;
                return true;
            }
        }) === true) {
            return index;
        }
        return undefined;
    } else {
        return undefined;
    }
}

function onRemoveProductInCartClick(pId, e) {
    let index = getProductInCart(pId);
    let jArr = JSON.parse(sessionStorage.pFavorite);
    // noinspection JSValidateTypes
    if (index !== undefined) {
        jArr.splice(index, 1);
        //remove html
        sessionStorage.pFavorite = JSON.stringify(jArr);
        $(e).closest('.cart-item').remove();

        updateCartSize();
    }

}

function countProductInCart() {
    if (sessionStorage.pFavorite) {
        let jArr = JSON.parse(sessionStorage.pFavorite);
        return jArr.length;
    }
    return 0;
}

function updateCartSize() {
    $('#text_cartsize').text(countProductInCart());
    $('#text_cartsize_mobile').text(countProductInCart());
}

