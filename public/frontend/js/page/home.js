$(document).ready(function () {
    $('.ui-menu-divider').last().removeClass('ui-widget-content');
    initSlider2();
    initHandlerEvent();
});

function initHandlerEvent() {
    $('.menu-cate .li-cate').on('click', function () {
        $('.menu-cate li').removeClass('active');
        $(this).addClass('active');
        let $cateId = $(this).attr('cid');
        $.ajax({
            dataType: "html",
            url: 'home/partial/product/' + $cateId,
            success: function (data) {
                $('.b-list-product').html(data);
            },
        });
    });

    //product
    $('.product-item-cover').on('click', function () {
        let pId = $(this).attr('pid');
        window.location.href = '/product/' + pId;
    });
}

function initSlider2() {
    let partnerItemSize = window.isMobile() ? 160 : 270;
    let partnerItemPadding = 20;
    let galleryItemSize = window.isMobile() ? 150 : 250;
    let galleryItemPadding = 15;

    $('#home_slider_gallery').flexslider({
        slideshow: false,
        animation: "slide",
        animationLoop: false,
        itemWidth: galleryItemSize,
        itemMargin: galleryItemPadding,
        /*minItems: 2,
        maxItems: 6,*/
        controlNav: false,
        directionNav: true,
    });

    $('#slider_partner').flexslider({
        slideshow: false,
        animation: "slide",
        animationLoop: false,
        itemWidth: partnerItemSize,
        itemMargin: partnerItemPadding,
        /*minItems: 2,
        maxItems: 6,*/
        controlNav: false,
        directionNav: true,
    });
}
