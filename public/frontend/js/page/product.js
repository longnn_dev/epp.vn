$(document).ready(function () {
    initSlider();
    initHandlerEventProduct();

    function initSlider() {
        $('#slider_product').flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: window.isMobile() ? 180 : 250,
            itemMargin: 15,
            controlNav: false,
            directionNav: true,
        });

        //remove absolute icon addcart
        let $img = $('#slider_product .slides').find('img.product-item-bt-shop');
        /*$img.css('position', 'unset');*/
        $img.css('width', '37px');
        $img.css('height', '37px');

        /*partner*/
        $('#slider_partner').flexslider({
            slideshow: false,
            animation: "slide",
            animationLoop: false,
            itemWidth: window.isMobile() ? 160 : 270,
            itemMargin: 20,
            /*minItems: 2,
            maxItems: 6,*/
            controlNav: false,
            directionNav: true,
        });
    }

    function initHandlerEventProduct() {
        /*menu cate*/
        $('.p-product .box-left-rect1-item').on('click', function () {
            let pId = $(this).attr('cid');
            $('.box-left-rect1 div').removeClass('fa fa-node-blue active');
            $(this).addClass(' fa fa-node-blue active');

            axios({
                method: 'post',
                url: '/product/partial/cate',
                data: {
                    'cid': pId, 'page': 1
                },
                config: {headers: {'Content-Type': 'multipart/form-data'}}
            }).then(function (response) {
                $('.b-list-product').html(response.data);
            }).catch(function (response) {
                //handle error
                console.log(response);
            });
        });
    }
});


