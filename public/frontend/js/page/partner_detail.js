$(document).ready(function () {
    initSlider();
    function initSlider() {
        // The slider being synced must be initialized first
        $('#slider_detail_bottom').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 120,
            itemMargin: 10,
            asNavFor: '#slider_detail_top'
        });

        $('#slider_detail_top').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
        });
    }
});


