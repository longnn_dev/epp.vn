$(document).ready(function () {
    initSlider();
    initHandlerEventQuantity();
    initHandlerBoxCColor();

    function initSlider() {
        // The slider being synced must be initialized first
        $('#slider_detail_bottom').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 120,
            itemMargin: 10,
            asNavFor: '#slider_detail_top'
        });

        $('#slider_detail_top').flexslider({
            animation: "slide",
            controlNav: true,
            animationLoop: false,
            slideshow: true,
            sync: "#carousel"
        });
    }

    function initHandlerEventQuantity() {
        $('#bt-plus,#bt-minus').on('click', function () {
            let quantity = Number($('#tx-quantity').text());
            quantity = this.id === 'bt-plus' ? (quantity + 1) : (quantity - 1);
            if (quantity < 0) {
                quantity = 0;
            }
            $('#tx-quantity').text(quantity);
        });
    }

    function initHandlerBoxCColor() {
        $('.b-color .item').on('click', function () {
            $('.b-color .item').html('');
            $(this).html('<img src="/frontend/img/ic-node.png">');
        });
    }
});


