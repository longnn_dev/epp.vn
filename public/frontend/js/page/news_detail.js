$(document).ready(function () {
    if (sessionStorage.news_recent) {
        axios({
            method: 'post',
            url: '/news/partial/recent',
            data: {
                'recent': JSON.parse(sessionStorage.news_recent),
            },
            config: {headers: {'Content-Type': 'multipart/form-data'}}
        }).then(function (response) {
            $('.b-recent-list').html(response.data);
        }).catch(function (response) {
            console.log(response);
        });
    }

    let jArr = [];
    if (sessionStorage.news_recent) {
        jArr = JSON.parse(sessionStorage.news_recent);
    }
    let pId = $('.b-main .news-title').attr('pid');
    if (!jArr.includes(pId)) {
        jArr.push(pId);
        sessionStorage.news_recent = JSON.stringify(jArr);
    }
});
