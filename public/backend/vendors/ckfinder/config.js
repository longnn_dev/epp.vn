/*
 Copyright (c) 2007-2018, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or https://ckeditor.com/sales/license/ckfinder
 */

var config = {
};

// Set your configuration options below.

// Examples:
// config.language = 'pl';
// config.skin = 'jquery-mobile';
config.licenseKey = 'FRDRTLS2FL7J3GND2S1746Y6FC8MW';
config.licenseName = 'localhost';
CKFinder.define( config );
