/**
 * Created by longn on 28-Nov-18.
 */
(function($){
    $.fn.singleImageSelect = function(options){
        const settings = $.extend({},options);
        const $this = $(this);
        const $image = $this.find('img').first();
        const $input = $this.find('input').first();
        let selected = false;
        // EVENT
        $this.on('click','img',$image,function(e){
            chooseImage(function (url) {
                $image.attr('src',url);
                $input.val(url);
            })
        });
        let singleImageSelect = {
            getImageUrl:function () {
                return $input.val();
            }
        }
        return singleImageSelect;
    }
    $.fn.multiImageSelect = function(options){
        const defaults = {
            inputField:'image'
        };
        const settings = $.extend({},defaults,options);
        const $this = $(this);
        const $controlPanel = $this.find(".toolbar");
        const $addButton = $controlPanel.find("button[data-control=add]");
        const $clearButton = $controlPanel.find("button[data-control=clear]");
        const $list = $this.find(".items");
        $addButton.on('click',function(e){
            e.preventDefault();
            chooseImage(function (imgUrl) {
               createItem(imgUrl);
            });
        });
        $this.on('click','.item-remove',function(e){
            $(this).parent().remove();
        });
        $clearButton.on('click',function(e){
            e.preventDefault();
            $list.empty();
        });
        function createItem(imgUrl){
            let item = $.parseHTML(`<div class="col-md-4 mb-3 item" id="${imgUrl}"> <button type="button" class="item-remove"><i class="la la-trash"></i> </button><img class="shadow-sm img-fluid" src="${imgUrl}" /><input type="hidden" name="${settings.inputField}" value="${imgUrl}" /></div>`);

            $list.append(item);
        }
        let multiImageSelect = {
            getImageUrls:function () {
                let imageUrls = [];
                $list.find('.item>input').each(function(index,item){
                   imageUrls.push($(item).val());
                });
                return imageUrls;
            }
        }
        return multiImageSelect;
    }
    function chooseImage(callback){
        CKFinder.modal({
            chooseFiles: true,
            width: 800,
            height: 500,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    let file = evt.data.files.first();
                    callback(file.getUrl());
                });
                finder.on( 'file:choose:resizedImage', function( evt ) {
                    callback(evt.data.resizedUrl);
                } );
            }
        });
    }
}(jQuery));
