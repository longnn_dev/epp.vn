/**
 * Created by longn on 28-Nov-18.
 */
(function($){
    $.fn.multiFields = function(options){
        const defaults = {
            keyText:'Property',
            valueText:'Value',
            inputKeyName:'key',
            inputValueName:'value'
        };
        const settings = $.extend({},defaults,options);
        const keyValueTemp = `<div class="row mb-3 item">
                                <label class="col-2 col-form-label text-right">${settings.keyText}</label>
                                <div class="col-sm-3"><input name="${settings.inputKeyName}" class="form-control" /> </div>
                                <label class="col-2 col-form-label text-right">${settings.valueText}</label>
                                <div class="col-sm-3"><input name="${settings.inputValueName}" class="form-control" /> </div>
                                <div class="col-sm-2"><button type="button" class="btn btn-primary item-remove"><i class="fa fa-trash"></i> Delete</button> </div>
                            </div>`;
        const simpleTemp = `<div class="input-group mb-3 item">
                            <input type="text" name="${settings.inputValueName}" class="form-control" placeholder="">
                            <div class="input-group-append"><button type="button" class="input-group-text item-remove" id="basic-addon2"><i class="la la-trash"></i></button></div>
                          </div>`;
        const $this = $(this);
        const $controlPanel = $this.find(".toolbar");
        const $addButton = $controlPanel.find("button[data-control=add]");
        const $clearButton = $controlPanel.find("button[data-control=clear]");
        const $list = $this.find(".items");
        const template = settings.type==='simple'?simpleTemp:keyValueTemp;
        $addButton.on('click',function(e){
            e.preventDefault();
           createItem(template);
        });
        $clearButton.on('click',function(e){
            e.preventDefault();
            $list.empty();
        });
        $(this).on('click','.item-remove',function(e){
            $(this).closest('.item').remove();
        });
        function createItem(template){
            let item = $.parseHTML(template);
            $(item).find('input').on('change',function(e){
                if($(this).val()){
                    $this.removeClass('is-invalid');
                }
            })
            $list.append(item);
        }
        let multiFields = {
            getValues:function () {
                let values = [];
                if(settings.type === 'simple'){
                    let $inputs = $list.find('input');
                    $inputs.each(function (index,input) {
                        values.push($(input).val());
                    })
                }else if(settings.type === 'keyValue'){
                    $list.find('.item').each(function(index,item){
                       let keyInput = $(item).find('input[name='+settings.inputKeyName+']');
                       let valueInput = $(item).find('input[name='+settings.inputValueName+']');
                       if(keyInput && valueInput) {
                           values.push({key: keyInput.val(), value: valueInput.val()});
                       }
                    });
                }
                return values;
            },
            valid:function(){
                let isValid = true;
                if(settings.type === 'simple'){
                    let $inputs = $list.find('input');
                    $inputs.each(function (index,input) {
                        if(!$(input).val()){
                            $(input).addClass('is-invalid');
                            isValid = false;
                        }
                    })
                }else if(settings.type === 'keyValue'){
                    let isValid = true;
                    $list.find('.item').each(function(index,item){
                        let $keyInput = $(item).find("input[name='"+settings.inputKeyName+"']").first();
                        let $valueInput = $(item).find("input[name='"+settings.inputValueName+"']").first();
                        if(!$keyInput.val()) {
                            $keyInput.addClass('is-invalid');
                            isValid = false;
                        }
                        if(!$valueInput.val()){
                            $valueInput.addClass('is-invalid');
                            isValid = false;
                        }
                    });
                }
                return isValid;
            }
        }
        return multiFields;
    }
}(jQuery));
