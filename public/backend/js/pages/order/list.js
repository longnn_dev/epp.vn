var Datatable = function() {
    var initTable = function() {
        var table = $('#datatable').DataTable({
            dom:'ltipr',
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            "order": [[ 3, "desc" ]],
            ajax: {
                url:'order/datatable',
                method:'POST',
                dataType:'json',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                {data: 'name'},
                {data: 'email'},
                {data: 'phone_number'},
                {data: 'message'},
                {data: 'created_at'},
                {data: 'status'},
                {data: 'id'}
            ],
            columnDefs: [
                {
                    targets: -1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `
                        <a href="javascript:;" class="dt-button__view btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                            <i class="la la-search"></i>
                        </a>
                        <a href="javascript:;" class="dt-button__status btn btn-sm btn-clean btn-icon btn-icon-md ${full.status==0?'kt-hidden':''}" title="Lock">
                            <i class="fa fa-lock"></i> 
                        </a>
                        <a href="javascript:;" class="dt-button__status btn btn-sm btn-clean btn-icon btn-icon-md ${full.status==1?'kt-hidden':''}" title="Unlock">
                            <i class="fa fa-unlock"></i>
                        </a>
                        <a href="javascript:;" class="dt-button__delete btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa">
                            <i class="la la-trash"></i>
                        </a>
                        `;
                    },
                },
                {
                    targets: 5,
                    width:"5%",
                    render: function(data, type, full, meta) {
                        return `
                            <span class="dt-status kt-badge kt-badge--success kt-badge--inline kt-badge--pill ${data==0?'kt-hidden':''}">Processed</span>
                            <span class="dt-status kt-badge kt-badge--danger kt-badge--inline kt-badge--pill ${data==1?'kt-hidden':''}">Pending</span>
                            `;
                    },
                },
            ],
        });
        var searchForm = $('#kt-search-form');
        var inputTitle = $(searchForm).find('input[name=title]');
        var selectStatus = $(searchForm).find('select[name=status]');
        $('#datatable tbody').on('click', 'a.dt-button__delete', function (e){
            e.preventDefault();
            //todo
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            if (confirm("Are you sure to delete this item ?") == false) {
                return;
            }
            axios.delete('order/'+data.id).then(function(response){
                console.log(response.data);
                var responseData = response.data;
                if(responseData.errorCode == 0){
                    table.row(nRow).remove().draw();
                }
            }).catch(function(error){

            });
        });
        $('#datatable tbody').on('click', 'a.dt-button__status', function (e){
            e.preventDefault();
            //todo
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            axios.post(`order/${data.id}/updateStatus`).then(function(response){
                console.log(response.data);
                var data = response.data;
                if(data.errorCode == 0){
                    $(nRow).find('.dt-button__status').toggleClass('kt-hidden');
                    $(nRow).find('.dt-status').toggleClass('kt-hidden');
                }
            }).catch(function(error){});
        });
        $('#datatable tbody').on('click', 'a.dt-button__view',function(e){
            e.preventDefault();
            e.preventDefault();
            //todo
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            axios.post(`order/${data.id}/updateStatus`).then(function(response){
                console.log(response.data);
                var data = response.data;
                $('#modal_cart').find('button').hide();
                $('#modal_cart').modal('show');

            }).catch(function(error){});

        });
        $('#kt_search').on('click',function(e){
            e.preventDefault();
            var title = $(inputTitle).val();
            var status = $(selectStatus).val();
            table.column(1).search(title);
            table.column(4).search(status);
            table.table().draw();
        });
        $('#kt_reset').on('click',function(e){
            e.preventDefault();
            $(inputTitle).val('');
            $(selectStatus).val('');
            $('#kt_search').trigger('click');
        });
    };
    return {
        //main function to initiate the module
        init: function() {
            initTable();
        },

    };
}();
jQuery(document).ready(function() {
    Datatable.init();
});
