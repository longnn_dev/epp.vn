var Datatable = function() {
    var initTable = function() {
        var table = $('#datatable').DataTable({
            dom:'ltipr',
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            "order": [[ 3, "desc" ]],
            ajax: {
                url:'document/datatables',
                method:'POST',
                dataType:'json',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                {data: 'cover_url'},
                {data: 'name'},
                {data: 'created_at'},
                {data: 'updated_at'},
                {data: 'status'},
                {data: 'locale'},
                {data: 'id'}
            ],
            columnDefs: [
                {
                    targets: -1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `
                        <a href="document/${full.id}/edit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                            <i class="la la-edit"></i>
                        </a>
                        <a href="javascript:;" class="dt-button__status btn btn-sm btn-clean btn-icon btn-icon-md ${full.status==0?'kt-hidden':''}" title="Lock">
                            <i class="fa fa-lock"></i>
                        </a>
                        <a href="javascript:;" class="dt-button__status btn btn-sm btn-clean btn-icon btn-icon-md ${full.status==1?'kt-hidden':''}" title="Unlock">
                            <i class="fa fa-unlock"></i>
                        </a>
                        <a href="javascript:;" class="dt-button__delete btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa">
                            <i class="la la-trash"></i>
                        </a>
                        `;
                    },
                },
                {
                    targets: 0,
                    orderable: false,
                    width:"110px",
                    className:'dt-body-center',
                    render: function(data, type, full, meta) {
                        return '<img class="img-fluid" height="75" src="'+data+'"/>';
                    },
                },
                {
                    targets: 4,
                    width:"5%",
                    render: function(data, type, full, meta) {
                        return `
                            <span class="dt-status kt-badge kt-badge--success kt-badge--inline kt-badge--pill ${data==0?'kt-hidden':''}">Active</span>
                            <span class="dt-status kt-badge kt-badge--danger kt-badge--inline kt-badge--pill ${data==1?'kt-hidden':''}">InActive</span>
                            `;
                    },
                },
                {
                    targets: 5,
                    width:"5%",
                    render: function(data, type, full, meta) {
                        console.log(data);
                        var render=`<a href="#" class="flag-button"><img class="flag-icon" src="backend/media/flags/${data}.png"/></a>`;
                        return render;
                    },
                },
            ],
        });
        var searchForm = $('#kt-search-form');
        var inputCode = $(searchForm).find('input[name=code]');
        var selectStatus = $(searchForm).find('select[name=status]');
        $('#datatable tbody').on('click', 'a.dt-button__delete', function (e){
            e.preventDefault();
            //todo
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            if (confirm("Are you sure to delete this item ?") == false) {
                return;
            }
            axios.delete('product/'+data.id).then(function(response){
                console.log(response.data);
                var responseData = response.data;
                if(responseData.errorCode == 0){
                    table.row(nRow).remove().draw();
                }
            }).catch(function(error){

            });
        });
        $('#datatable tbody').on('click', '.dt-button__type', function (e){
            e.preventDefault();
            //todo
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            axios.post(`product/${data.id}/setHot`).then(function(response){
                var data = response.data;
                if(data.errorCode == 0){
                    $(nRow).find('.dt-type').toggleClass('kt-font-danger');
                }
            }).catch(function(error){});
        });
        $('#datatable tbody').on('click', 'a.dt-button__status', function (e){
            e.preventDefault();
            //todo
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            axios.post(`product/${data.id}/updateStatus`).then(function(response){
                console.log(response.data);
                var data = response.data;
                if(data.errorCode == 0){
                    $(nRow).find('.dt-button__status').toggleClass('kt-hidden');
                    $(nRow).find('.dt-status').toggleClass('kt-hidden');
                }
            }).catch(function(error){});
        });
        $('#kt_search').on('click',function(e){
            e.preventDefault();
            var code = $(inputCode).val();
            var status = $(selectStatus).val();
            table.column(1).search(code);
            table.column(4).search(status);
            table.table().draw();
        });
        $('#kt_reset').on('click',function(e){
            e.preventDefault();
            $(inputCode).val('');
            $(selectStatus).val('');
            $('#kt_search').trigger('click');
        });
    };
    return {
        //main function to initiate the module
        init: function() {
            initTable();
        },

    };
}();
jQuery(document).ready(function() {
    Datatable.init();
});
