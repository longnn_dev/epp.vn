var Articles = function () {

    var initModule = function () {
        var $formSearch = $("#formSearch");
        var $category = $formSearch.find('select[name=category]');
        var $updatedDate = $formSearch.find('.updatedDate');
        var $author = $formSearch.find('select[name=author]');
        var $btnSearch = $formSearch.find('button[name=search]');
        var $avaivList = $("#availableList");
        var $selList = $("#selectedList");

        if (jQuery().datepicker) {
            $updatedDate.datepicker({
                orientation: "right",
                autoclose: true,
                clearBtn: true
            });
        }
        $category.select2({
            theme: "bootstrap",
            placeholder: 'Select an option',
            allowClear: true,
            ajax: {
                url: '../../api/articleCategory/getList',
                data: function (params) {
                    var query = {
                        name: params.term,
                        type: 'public'
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data, page) {
                    var dataParsed = $.map(data, function (obj) {
                        obj.id = obj.id || obj._id;
                        obj.text = obj.text || obj.name;
                        return obj;
                    });
                    return {
                        results: dataParsed
                    };
                },
            }
        });
        $author.select2({
            placeholder: 'Select an option',
            theme: "bootstrap",
            allowClear: true,
            ajax: {
                url: '../../api/user/getList',
                data: function (params) {
                    var query = {
                        name: params.term,
                        type: 'public'
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data, page) {
                    var dataParsed = $.map(data, function (obj) {
                        obj.id = obj.id || obj._id;
                        obj.text = obj.text || obj.name;
                        return obj;
                    });
                    return {
                        results: dataParsed
                    };
                },
            }
        });
        $btnSearch.on('click',function (e) {
            var data = {};
            data.catId = $('select[name=category] option:selected').val();
            data.authorId = $author.val();
            data.updatedDate = $formSearch.find('input[name=updatedDate]').val();
            console.log(data);
            axios.post('../api/article/getList',data).then(function(response){
                var responseData = response.data;
                $avaivList.empty();
                if(responseData.length > 0){
                    $.each(responseData,function(i,data){
                        var $item = $('<div>',{class:'article-item-type-1'});
                        $item.append($('<div>',{class:'article-title'}).text(data.name))
                        $item.append($('<div>',{class:'article-info'}).text(data.updatedAt+" • "+data.author));
                        $avaivList.append($item);
                    });
                }
            }).catch(function(error){

            });
        });

        $("#availableList").sortable({
            connectWith: "#selectedList",
            start: function (event, ui) {
                ui.item.toggleClass("highlight");
            },
            stop: function (event, ui) {
                ui.item.toggleClass("highlight");
            }
        });
        $("#selectedList").sortable({
            connectWith: "#availableList",
            receive: function(event,ui){
                var sortedIDs = $( "#selectedList" ).sortable( "toArray" );
                saveSort(sortedIDs);
            },
            remove: function(event,ui){
                var sortedIDs = $( "#selectedList" ).sortable( "toArray" );
                saveSort(sortedIDs);
            },
            update: function(event,ui){
                var sortedIDs = $( "#selectedList" ).sortable( "toArray" );
                saveSort(sortedIDs);
            }
        });

        function saveSort(ids){
            axios.post('../../api/article/orderSlide',{ids:ids}).then(function(response){
                var responseData = response.data;
                if(responseData.errorCode == 0){

                }
            }).catch(function(error){

            });

        }
    }

    return {
        //main function to initiate the module
        init: function () {
            initModule();
        }
    };
}();

jQuery(document).ready(function() {
    Articles.init();
});
