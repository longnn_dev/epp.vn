/**
 * Created by longn on 21-Oct-18.
 */
var ComponentsDropdowns = function () {
    var handleMultiSelect = function () {
        var $btn_cover = $("button[name=btn_cover]");
        var $cover_preview = $("#cover_preview");
        var $cover_input = $("input[name=meta_image]");
        $btn_cover.on('click',function(){
            selectCover("cover_preview");
        });
        function selectCover( elementId ) {
            CKFinder.modal( {
                chooseFiles: true,
                width: 800,
                height: 500,
                onInit: function( finder ) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        $cover_preview.attr('src',file.getUrl());
                        $cover_input.val(file.getUrl());
                    } );
                    finder.on( 'file:choose:resizedImage', function( evt ) {
                        $cover_preview.attr('src',evt.data.resizedUrl);
                        $cover_input.val(evt.data.resizedUrl);
                    } );
                }
            } );
        }
        var $list = $('.page-url-list');
        $list.sortable();
        $('.page-url-list').on('click','.btn-remove-url',function(e){
           e.preventDefault();
           $(this).parents('.url-item').remove();
        });
        $('.btn-add-url').on('click',function(e){
            e.preventDefault();
            var html = `<div class="input-group url-item mb-2">
                            <input type="text" name="url[]" class="form-control" placeholder="" value="">
                            <div class="input-group-append">
                                <button class="btn btn-warning btn-remove-url" type="button">Xóa</button>
                            </div>
                        </div>`;
            $list.append(html);
        });
    }
    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
        }
    };
}();

jQuery(document).ready(function() {
    ComponentsDropdowns.init();
});
