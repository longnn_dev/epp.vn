/**
 * Created by longn on 21-Oct-18.
 */
var ComponentsDropdowns = function () {
    var handleMultiSelect = function () {
        $('#permission_select').multiSelect();
    }
    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
        }
    };
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {
        ComponentsDropdowns.init();
    });
}