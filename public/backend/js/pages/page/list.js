var Datatable = function() {
    var initTable = function() {
        var table = $('#datatable').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url:'page/datatables',
                method:'POST',
                dataType:'json',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: [
                {data: 'meta_image'},
                {data: 'meta_title'},
                {data: 'route'},
                {data: 'locale'},
                {data: 'id'}
            ],
            columnDefs: [
                {
                    targets: -1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `
                        <a href="page/`+full.id+`/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Cập nhật">
                            <i class="la la-edit"></i>
                        </a>
                        <a href="javascript:;" class="dt-button dt-button__delete m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Xóa">
                            <i class="la la-trash"></i>
                        </a>
                        `;
                    },
                },

                {
                    targets: 3,
                    orderable: false,
                    width:"10px",
                    className:'dt-body-center',
                    render: function(data, type, full, meta) {
                        var flagImage = 'backend/media/flags/'+data+'.png';
                        return '<img class="" style="width:30px;" src="'+flagImage+'"/>';
                    },
                },
                {
                    targets: 0,
                    orderable: false,
                    width:"100px",
                    className:'dt-body-center',
                    render: function(data, type, full, meta) {
                        return '<img class="img-fluid" style="height: 70px" src="'+data+'"/>';
                    },
                }
            ],
        });
        $('#datatable tbody').on('click', 'a.dt-button__delete', function (e){
            e.preventDefault();
            //todo
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            if (confirm("Are you sure to delete this item ?") == false) {
                return;
            }
            axios.delete('page/'+data.id).then(function(response){
                console.log(response.data);
                var responseData = response.data;
                if(responseData.errorCode == 0){
                    table.row(nRow).remove().draw();
                }
            }).catch(function(error){

            });
        });
        $('#datatable tbody').on('click', 'a.dt-button__status', function (e){
            e.preventDefault();
            //todo
            var $this = $(this);
            var nRow = $(this).parents('tr');
            var data = table.row(nRow).data();
            axios.post('page/updateStatus',{_id:data.id}).then(function(response){
                console.log(response.data);
                var responseData = response.data;
                if(responseData.errorCode == 0){
                    var $statusField = $(nRow).find('.m-item--status');
                    if(responseData.status){
                        $statusField.text('Hoạt động');
                        $statusField.removeClass('m-badge--danger');
                        $statusField.addClass('m-badge--success');
                        $this.attr("title","Khóa");
                        $this.find("i").removeClass("fa-unlock");
                        $this.find("i").addClass("fa-lock");
                    }else{
                        $statusField.text('Không hoạt động');
                        $statusField.addClass('m-badge--danger');
                        $statusField.removeClass('m-badge--success');
                        $this.attr("title","Mở khóa");
                        $this.find("i").addClass("fa-unlock");
                        $this.find("i").removeClass("fa-lock");
                    }
                }
            }).catch(function(error){

            });
        });
    };

    return {

        //main function to initiate the module
        init: function() {
            initTable();
        },

    };

}();

jQuery(document).ready(function() {
    Datatable.init();
});
