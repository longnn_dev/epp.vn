$(document).ready(function(){
    let photoSelect = $('#slides').multiImageSelect({inputField:'photo'});
    $("#sortable").sortable({
        receive: function(event,ui){
            var sortedIDs = $( "#sortable" ).sortable( "toArray" );
            saveSort(sortedIDs);
        },
        remove: function(event,ui){
            var sortedIDs = $( "#sortable" ).sortable( "toArray" );
            saveSort(sortedIDs);
        },
        update: function(event,ui){
            var sortedIDs = $( "#sortable" ).sortable( "toArray" );
            saveSort(sortedIDs);
        }
    });

    $("button[type=submit]").on('click',function(e){
        var sortedIDs = $( "#sortable" ).sortable( "toArray" );
        saveSort(sortedIDs);
    });

    function saveSort(sortedIDs){
        console.log(sortedIDs);
        axios.post('/slideshow/save',{ids:sortedIDs}).then(function(response){
            const data = response.data;
            swal.fire({
                "title": "System Notification",
                "text": data.message,
                "type": data.type,
                onClose:()=>{
                    location.href = './'
                }
            });
        }).catch(function(error){

        });
    }
});
