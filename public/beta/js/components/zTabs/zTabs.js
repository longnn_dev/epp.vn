(function($){
    $.fn.zTabs = function(options){
        const settings = $.extend({},options);
        return this.each(function() {
            const $this = $(this);
            const $tabNav = $this.find('.tab-nav');
            $tabNav.on('click','a',function(e){
                e.preventDefault();
                let $currentNav = $tabNav.find('a.active');
                let $currentContent = $this.find('.tab-content.active');
                $currentNav.removeClass('active');
                $currentContent.removeClass('active');
                $(this).addClass('active');
                let id = $(this).attr('href');
                console.log(id);
                $this.find(id).addClass('active');
            });
        });
    }
}(jQuery));

