$(document).ready(function () {
    if (sessionStorage.news_recents) {
        axios({
            method: 'post',
            url: '/news/partial/recent',
            data: {
                'recents': JSON.parse(sessionStorage.news_recents),
            },
            config: {headers: {'Content-Type': 'multipart/form-data'}}
        }).then(function (response) {
            $('.hotnews-list .t-container').html(response.data);
        }).catch(function (response) {
            console.log(response);
        });
    }

    let jArr = [];
    if (sessionStorage.news_recents) {
        jArr = JSON.parse(sessionStorage.news_recents);
    }
    let pId = $('.hotnews .t-title').attr('pid');
    if (!jArr.includes(pId)) {
        jArr.push(pId);
        sessionStorage.news_recents = JSON.stringify(jArr);
    }
});
