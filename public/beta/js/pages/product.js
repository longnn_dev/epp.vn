$(document).ready(function(){
    $(document).on('click','img[data-action=open-slide]',function(e){
        var index = $('img[data-action=open-slide]').index(this);
        fullSlide(index);
    });
    $('.full-screen-act').on('click',function(e){
        $(".fullscreen-slide").hide();
    });
    $(document).find('img[data-action=open-slide]').each(function(index){
        var imgSrc = $(this).attr('src');
        var alt = $(this).attr('alt');
        var thumb = `<li>
                        <a href="javascript:;" class="cover" title="`+alt+`">
                            <img src="`+imgSrc+`">
                        </a>
                    </li>;`
        var full = `<li>
                        <a class="img cover" href="javascript:;" title="`+alt+`">
                            <img class="image-normalized" src="`+imgSrc+`">
                        </a>
                    </li>`;
        $("#slide-thumbs").append(thumb);
        $("#slide-full").append(full);
    });
    function fullSlide(index){
        $('.fullscreen-slide').show();
        $('#ft-slider').flexslider({
            controlsContainer: $(".slider-buttons2"),
            customDirectionNav: $(".slider-buttons2 a"),
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            itemWidth:110,
            itemMargin:5,
            slideshow: false,
            asNavFor:"#fm-slider"
        });
        $('#fm-slider').flexslider({
            controlsContainer: $(".slider-buttons"),
            customDirectionNav: $(".slider-buttons a"),
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            slideshow: false,
            sync:"#ft-slider"
        });
        $('#fm-slider').flexslider(index);
    }
})
