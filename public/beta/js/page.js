$(document).ready(function () {
    $('#chk_lang').on('click', function () {
        axios({
            method: 'post',
            url: '/changeLang',
            config: {headers: {'Content-Type': 'application/json'}}
        }).then(function (response) {
            window.location.reload();
        }).catch(function (response) {
            console.log(response);
        });
    });
    $("#search-input").devbridgeAutocomplete({
        serviceUrl:"/search/",
        type:"GET",
        paramName:"q",
        dataType:"json",
        width:"450px",
        orientation:"auto",
        transformResult: function(response) {
            return {
                suggestions: $.map(response, function(dataItem) {
                    return { value: dataItem.name, data: dataItem.url };
                })
            };
        },
        onSelect:function (sugestion) {
            location.href = sugestion.data;
        }
    });
});
