$(document).ready(function () {

    updateCartSize();

    $('.show-cart').on('click', function () {
        $('#modal_cart').modal('show');
    });

    $('.add-cart, #btn_addcart').on('click', function (e) {
        addToCart($(this).attr('pid'), e)
    });

    $('#modal_cart').on('shown.bs.modal', function (e) {
        let $product = [];
        if (sessionStorage.pFavorite) {
            $product = JSON.parse(sessionStorage.pFavorite);
        }
        axios({
            method: 'post',
            url: '/cart/partial/products',
            data: {'products': $product},
            config: {headers: {'Content-Type': 'application/json'}}
        }).then(function (response) {
            $('.cart-products').html(response.data);

            //assign event click
            $('.cart-product button').on('click', function (e) {
                removeProductInCart($(this).attr('pid'), e)
            });
        }).catch(function (response) {
            console.log(response);
        });
    });

    $('#modal_cart form').on("submit", function (e) {
        e.preventDefault();
        if (cartSize() <= 0) {
            Swal.fire(
                'Alert!',
                'You have no items in your interest list!',
                'warning'
            );
            return;
        }
        let $product = [];
        if (sessionStorage.pFavorite) {
            $product = JSON.parse(sessionStorage.pFavorite);
        }
        axios({
            method: 'post',
            url: '/cart/order',
            data: {
                'products': $product,
                'info': {
                    'name': $('#input_name').val(),
                    'email': $('#input_email').val(),
                    'phone': $('#input_phone').val(),
                    'company': $('#input_company').val(),
                    'message': $('#input_message').val(),
                }
            },
            config: {headers: {'Content-Type': 'multipart/form-data'}}
        }).then(function (response) {
            sessionStorage.pFavorite = '';
            $('.cart-products').html('');
            Swal.fire(
                'Thanks you!',
                'Submitted successfully!',
                'success'
            )
        }).catch(function (response) {
            console.log(response);
        });

        $('#modal_cart').modal('hide');
    });

    function addToCart(pId, e) {
        let index = getProductInCart(pId);
        // noinspection JSValidateTypes
        if (index !== undefined) {
            Swal.fire(
                'Alert!',
                'The product already exists in the list of interest!',
                'error'
            )
            return;
        }
        let jArr = [];
        if (sessionStorage.pFavorite) {
            jArr = JSON.parse(sessionStorage.pFavorite);
        }
        jArr.push(pId);
        sessionStorage.pFavorite = JSON.stringify(jArr);
        /*Swal.fire({
            title: 'Alert!',
            text: 'The product has added to cart!',
            type: 'success',
            timer: '1500',
        })*/
        animFly(e);
        updateCartSize();
    }

    function removeProductInCart(pId, e) {
        let index = getProductInCart(pId);
        // noinspection JSValidateTypes
        if (index !== undefined) {
            let jArr = JSON.parse(sessionStorage.pFavorite);
            jArr.splice(index, 1);
            //remove html
            sessionStorage.pFavorite = JSON.stringify(jArr);
            $(e.target).closest('.cart-product').remove();
            updateCartSize();
        }
    }

    function getProductInCart(pId) {
        let index = -1;
        if (sessionStorage.pFavorite) {
            let jArr = JSON.parse(sessionStorage.pFavorite);
            if (jArr.some(function (productId, i, arr) {
                if (pId === productId) {
                    index = i;
                    return true;
                }
            }) === true) {
                return index;
            }
            return undefined;
        } else {
            return undefined;
        }
    }

    function animFly(e) {
        let cart = $('.show-cart');
        let imgtodrag;
        if (e.target.id === 'btn_addcart') {
            imgtodrag = $(e.target).closest('.product-summary').find("img.project-image").eq(0);
        }
        else {
            imgtodrag = $(e.target).closest('.product').find("img").eq(0);
        }
        if (imgtodrag) {
            let imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
                }, 1000, 'easeInOutExpo');

            /*setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);*/

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
    }

    function updateCartSize() {
        let $showCart = $('.show-cart');
        if (cartSize() > 0) {
            $showCart.addClass('active');
        }
        else {
            $showCart.removeClass('active');
        }
    }

    function cartSize() {
        if (sessionStorage.pFavorite) {
            let jArr = JSON.parse(sessionStorage.pFavorite);
            return jArr.length;
        }
        return 0;
    }
});