<?php
return [
    'en' => 'Tiếng Anh',
    'vi' => 'Tiếng Việt',
    'main_info' => 'Thông tin cơ bản',
    'brand_name' => 'Tên thương hiệu',
    'full_name' => 'Tên đầy đủ',
    'address' => 'Địa chỉ',
    'alt_address' => 'Địa chỉ dự phòng',
    'phone_number' => 'Số điện thoại',
    'email_address' => 'Địa chỉ email',
    'social_channel' => 'Kênh mạng xã hội',
    'properties' => 'Thuộc tính',
    'about' => 'Giới thiệu',
    'avatar' => 'Ảnh đại diện',
    'bom' => 'Sơ đồ tổ chức',
    'medias' => 'Đa phương tiện',
    'photos' => 'Hình ảnh',
    'videos' => 'Videos',
    'add' => 'Thêm',
    'remove_all' => 'Xóa hết',
    'save' => 'Lưu',
    'cancel' => 'Hủy',
    'pieces' => 'Chiếc',
    'cart' => 'Giỏ hàng',
    'login' => 'Đăng nhập',
    'currency' => 'VNĐ',
    'min_order'=>'Nhỏ nhất',
    'field.avatar' => 'Ảnh',
    'field.title' => 'Tiêu đề',
    'field.code' => 'Mã',
    'field.brand' => 'Tên thương hiệu',
    'field.name' => 'Tên',
    'field.address' => 'Địa chỉ',
    'field.created_at' => 'Ngày tạo',
    'field.updated_at' => 'Ngày cập nhật',
    'field.actions' => 'Thao tác',
    'field.mark' => 'Đánh dấu',
    'field.status' => 'Trạng thái',
    'field.type' => 'Loại',
    'field.logo' => 'Logo',
    'field.langs' => 'Bản dịch',
    'field.sapo' => 'Tóm tắt',
    'field.content' => 'Nội dung',
    'field.phone_number' => 'Số điện thoại',
    'field.company' => 'Công ty',
    'field.companySize' => 'Quy mô công ty',
    'field.contactPerson' => 'Đơn vị tiếp nhận',
    'field.message' => 'Tin nhắn',
    'field.email' => 'Địa chỉ email',
    'field.select.null' => 'Chọn...',
    'field.category' => 'Nhóm',
    'field.color' => 'Màu sắc',
    'field.price' => 'Giá',
    'field.prices' => 'Chiếc',
    'field.size' => 'Kích cỡ',
    'field.minOrder' => 'Tối thiểu',
    'field.leadTime' => 'Thời gian giao hàng',
    'field.quickDetails' => 'Chi tiết',
    'field.supplyAbility' => 'Khả năng cung ứng',
    'field.packagingDelivery' => 'Đóng kiện & Giao hàng',
    'field.location' => 'Vị trí',
    'field.phone' => 'Số điện thoại',
    'field.photo' => 'Hình ảnh',
    'field.contact' => 'Liên hệ',
    'section.overview' => 'Tổng quan',
    'message.system.say' => 'Hệ thống thông báo',
    'message.company.save.success' => 'Lưu thông tin công ty thành công',
    'message.company.save.error' => 'Lưu thông tin lỗi!',
    'product.leadtime.quantity'=>'Số lượng(Chiếc)',
    'product.leadtime.time'=>'Est. Time(days)',
    'search'=>'Tìm kiếm',
    'nav.home'=>'Trang chủ',
    'nav.product'=>'Sản phẩm',
    'nav.partner'=>'Đối tác',
    'nav.service'=>'Dịch vụ',
    'nav.document'=>'Tài liệu',
    'nav.news'=>'Tin tức',
    'nav.about'=>'Thông tin',
    'nav.contact'=>'Liên hệ',
    'field.productCategory'=>'DANH MỤC SẢN PHẨM',
    'home.profile.bigTitle'=>'VỀ CHÚNG TÔI',
    'field.readMore'=>'Đọc thêm',
    'field.partner'=>'ĐỐI TÁC',
    'product.featureProduct' =>'SẢN PHẨM LIÊN QUAN',
    'partner.boardOfManager' => 'Sơ đồ tổ chức',
    'contact.agreeMsg' => 'Tôi đồng ý với các điều khoản về lưu trữ.',
];
