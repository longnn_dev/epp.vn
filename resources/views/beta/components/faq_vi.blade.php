<section class="container no-background no-border">
    <div class="row service-list">
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_phone_1243689.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Báo giá</div>
                <div class="desc mt-3">Cung cấp báo giá cho sản phẩm bao Jumbo</div>
            </div>
        </div>
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_message_1243690.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Tư vấn</div>
                <div class="desc mt-3">Tư vấn thiết kế, sản xuất bao Jumbo</div>
            </div>
        </div>
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_bag_1243671.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Đặt hàng</div>
                <div class="desc mt-3">Nhận đặt hàng sản xuất bao Jumbo</div>
            </div>
        </div>
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_register_1243707.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Hỗ trợ</div>
                <div class="desc mt-3">Hỗ trợ thắc mắc và khiếu nại của khách hàng 24/7</div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <h1 class="section-title"><span>FAQ</span></h1>
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="nav nav-pills faq-nav" id="faq-tabs" role="tablist" aria-orientation="vertical">
                        <a href="#tab1" class="nav-link active" data-toggle="pill" role="tab" aria-controls="tab1" aria-selected="true">
                            <i class="mdi mdi-help-circle"></i> Công ty chúng tôi
                        </a>
                        <a href="#tab2" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab2" aria-selected="false">
                            <i class="mdi mdi-account"></i> Bao FIBC
                        </a>
                        <a href="#tab3" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab3" aria-selected="false">
                            <i class="mdi mdi-account-settings"></i> Báo giá
                        </a>
                        <a href="#tab4" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab4" aria-selected="false">
                            <i class="mdi mdi-heart"></i> Đơn hàng
                        </a>
                        <a href="#tab5" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab5" aria-selected="false">
                            <i class="mdi mdi-coin"></i> Thông tin kỹ thuật
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content" id="faq-tab-content">
                        <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
                            <div class="accordion" id="accordion-tab-1">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-1-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-1" aria-expanded="false" aria-controls="accordion-tab-1-content-1">
                                                1.	Sản phẩm chính của công ty là gì?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-1-content-1" aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                                        <div class="card-body">
                                            <p>Công ty chúng tôi chuyên sản xuất và cung cấp bao FIBC hay còn được biết đến với nhiều tên gọi như Bao Jumbo, Bao big bag, bao bulk bag, bao container bag. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-1-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-2" aria-expanded="false" aria-controls="accordion-tab-1-content-2">
                                                2.	Trước khi đặt hàng, tôi có thể đến thăm nhà máy không?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-1-content-2" aria-labelledby="accordion-tab-1-heading-2" data-parent="#accordion-tab-1">
                                        <div class="card-body">
                                            <p>Chúng tôi rất hoan nghênh quý khách hàng. Vui lòng cho chúng tôi biết trước lịch trình, thời gian đến, chúng tôi có thể đến đón bạn. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2" role="tabpanel" aria-labelledby="tab2">
                            <div class="accordion" id="accordion-tab-2">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-1" aria-expanded="false" aria-controls="accordion-tab-2-content-1">
                                                1.	Bao FIBC là gì?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-2-content-1" aria-labelledby="accordion-tab-2-heading-1" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>“Flexible Intermediate Bulk Containers” (Bao bì tải trọng lớn linh hoạt) là tên quốc tế chính thức cho bao FIBC, tuy nhiên nhiều công ty còn gọi bao FIBC với các tên khác như bao bulka bag, bao tote jumbo bag, bao bulk bag, v.v.</p>
                                            <p>Bao bulk bag có nhiều kích thước khác nhau tùy vào nhu cầu sử dụng, nhưng thông thường có tải trọng từ 500kg – 2000 kg. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-2" aria-expanded="false" aria-controls="accordion-tab-2-content-2">
                                                2.	Bao FIBC được làm từ nguyên liệu gì?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-2" aria-labelledby="accordion-tab-2-heading-2" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>Nguyên liệu chính dùng để sản xuất bao FIBCs là hạt nhựa polypropylene, đây là một loại nhựa có nguồn gốc từ dầu mỏ. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-3" aria-expanded="false" aria-controls="accordion-tab-2-content-3">
                                                3.	Bao FIBC có kích thước như thế nào?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-3" aria-labelledby="accordion-tab-2-heading-3" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>Bao Bulk bag có nhiều kích thước, hình dáng khác nhau và có thể tuy chỉnh kích thước 3 chiều cho phù hợp với sản phẩm. Bao bulk bag vẫn có kích thước giới hạn, nhưng kích thước chuẩn của đáy bao bulk bag là 90x90cm (35x 35 inch) còn chiều cao của bao có thể lên tới hơn 120cm (96 inch).</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-4">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-4" aria-expanded="false" aria-controls="accordion-tab-2-content-4">
                                                4.	Loại bao FIBC nào phổ biến và thường được sử dụng trong lĩnh vực nào?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-4" aria-labelledby="accordion-tab-2-heading-4" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>Có khá nhiều loại bao FIBC phổ biến trên thị trường. Loại bao thông dụng nhất được cấu tạo theo cấu trúc U-panel (tạo hình chữ U) hoặc dạng tròn và có thể kết hợp thêm miếng lót PE tùy ý. Phần lớn các loại bao được gọi tên dựa vào cấu trúc của chúng ví dụ Bao 4 mảnh (4 Panel), Bao chữ U (U panel), Bao tròn hoặc dựa vào mục đích sử dụng như bao loại B (được làm từ manh PP cách điện) hoặc bao vách ngăn. </p>
                                            <p>Bao FIBC thường được sử dụng trong ngành nông nghiệp, hóa chất, dược phẩm, thực phẩm, nhựa, khai thác mỏ, thép, sản xuất nhựa và các lĩnh vực sản xuất khác liên quan đến các sản phẩm bột.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-5">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-5" aria-expanded="false" aria-controls="accordion-tab-2-content-5">
                                                5.	SF 5: 1 có nghĩa là gì?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-5" aria-labelledby="accordion-tab-2-heading-5" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>
                                                Một bao bulk bag chuẩn được đánh giá bằng Hệ số an toàn (Safety factor) 5:1 theo tiêu chuẩn ISO 21898, có nghĩa rằng bao này có thể chứa đến 5 lần tải trọng cho phép. Hệ số an toàn 5:1 áp dụng cho bao sử dụng 1 lần. Hệ số an toàn cho bao tái sử dụng nhiều lần là 6:1.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3" role="tabpanel" aria-labelledby="tab3">
                            <div class="accordion" id="accordion-tab-3">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-3-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-1" aria-expanded="false" aria-controls="accordion-tab-3-content-1">
                                                1.	Tôi cần cung cấp thông tin gì để nhận được báo giá?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-3-content-1" aria-labelledby="accordion-tab-3-heading-1" data-parent="#accordion-tab-3">
                                        <div class="card-body">
                                            <p>
                                                Nếu bạn quan tâm đến sản phẩm của chúng tôi, vui lòng gửi thêm thông tin về tải trọng, kích thước, màu sắc, công dụng và số lượng bạn cần. Chúng tôi sẽ thiết kế những chiếc bao hoàn hảo cho bạn.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-3-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-2" aria-expanded="false" aria-controls="accordion-tab-3-content-2">
                                                2.	Bao lâu thì tôi sẽ nhận được báo giá?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-3-content-2" aria-labelledby="accordion-tab-3-heading-2" data-parent="#accordion-tab-3">
                                        <div class="card-body">
                                            <p>Bạn chỉ cần chờ 10 phút để nhận báo giá kể từ lúc bạn gửi cho chúng tôi thông tin chi tiết. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-3-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-3" aria-expanded="false" aria-controls="accordion-tab-3-content-3">
                                                3.	Tôi có thể nhận bao mẫu từ công ty không?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-3-content-3" aria-labelledby="accordion-tab-3-heading-3" data-parent="#accordion-tab-3">
                                        <div class="card-body">
                                            <p>Chúng tôi sẵn sàng cũng cấp bao mẫu cho bạn. Bao mẫu sẽ hoàn toàn MIỄN PHÍ, bạn chỉ cần thanh toán chi phí vận chuyển. Chúng tôi có thể gửi mẫu qua các hãng chuyển phát nhanh quốc tế như DHL, FEDEX,….</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4" role="tabpanel" aria-labelledby="tab4">
                            <div class="accordion" id="accordion-tab-4">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-1" aria-expanded="false" aria-controls="accordion-tab-4-content-1">
                                                1.	Công ty có số lượng tối thiểu cho 1 đơn hàng bao bulk bag không?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-4-content-1" aria-labelledby="accordion-tab-4-heading-1" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>Số lượng đặt tối thiểu của chúng tôi rất thấp, bạn hoàn toàn có thể đặt 1 bao mẫu để kiểm tra chất lượng. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-2" aria-expanded="false" aria-controls="accordion-tab-4-content-2">
                                                2.	Thời gian cho 1 đơn hàng là bao lâu?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-4-content-2" aria-labelledby="accordion-tab-4-heading-2" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>Điều này phụ thuộc vào số lượng bạn đặt. Nếu dưới 1000 bao thì sẽ mất khoảng 15 ngày. Đối với đơn hàng trên 5000 bao thì thời gian sẽ là 30 ngày kể từ ngày đặt cọc.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-3" aria-expanded="false" aria-controls="accordion-tab-4-content-3">
                                                3.	Làm thế nào để kiểm tra chất lượng sản phẩm trong đơn hàng của tôi?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-4-content-3" aria-labelledby="accordion-tab-4-heading-3" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>Chúng tôi có phương pháp, trang thiết bị tiên tiến hiện đại và độ ngũ chuyên gia QC để kiểm tra chất lượng sản phẩm. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-4">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-4" aria-expanded="false" aria-controls="accordion-tab-4-content-4">
                                                4.	Làm thế nào để tiến hành 1 đơn đặt hàng bao big bag?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-4-content-4" aria-labelledby="accordion-tab-4-heading-4" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>Đầu tiên, hãy cho chúng tôi biết những yêu cầu của bạn và mục đích sử dụng của bao. </p>
                                            <p>Thứ hai, chúng tôi sẽ báo giá dựa vào những yêu cầu của bạn và góp ý của chúng tôi.</p>
                                            <p>Thứ ba, bạn kiểm tra mẫu và đặt cọc cho đơn đặt hàng chính thức.</p>
                                            <p>Thứ tư, chúng tôi bắt đầu sản xuất.  </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab5" role="tabpanel" aria-labelledby="tab5">
                            <div class="accordion" id="accordion-tab-5">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-1" aria-expanded="false" aria-controls="accordion-tab-5-content-1">
                                                1.	Có thể in lên bao bulk bag không?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-5-content-1" aria-labelledby="accordion-tab-5-heading-1" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>Có thể in lên bao bulk bag và chủ yếu sẽ in lên mặt bên của bao. Màu in thường chỉ có 1 đến 2 màu, tuy nhiên chúng tôi có khả năng in nhiều màu hơn tùy thuộc vào chi phí. Chúng tôi cũng có thể in logo tùy chỉnh theo yêu cầu khách hàng. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-2" aria-expanded="false" aria-controls="accordion-tab-5-content-2">
                                                2.	Làm thế nào để đính kèm tài liệu vào bao FIBC?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-2" aria-labelledby="accordion-tab-5-heading-2" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>Thông thường, tài liệu sẽ được đặt trong một túi tài liệu may vào bao bulka bag. Túi tài liệu này có thể là túi mở ziplock hoặc túi mở thông thường.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-3" aria-expanded="false" aria-controls="accordion-tab-5-content-3">
                                                3.	Bao bulk bag có thể chứa túi lót không?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-3" aria-labelledby="accordion-tab-5-heading-3" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>Có, Túi lót có thể được đặt vào bên trong hoặc đính vào vải bên trong của bao. Có nhiều loại túi lót khác nhau tùy vào mục đích sử dụng, nhưng 2 loại chính là túi lót dạng ống và túi lót định hình. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-4">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-4" aria-expanded="false" aria-controls="accordion-tab-5-content-4">
                                                4.	Tôi có thể chọn màu vải gì?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-4" aria-labelledby="accordion-tab-5-heading-4" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>
                                                Vải PP chuẩn có màu trắng tuy nhiên chúng tôi có thể sản xuất ra vải có màu sắc tùy ý. Bao có màu sẽ là một đơn hàng đặc biệt và có số lượng đặt hàng tối thiểu.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-5">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-5" aria-expanded="false" aria-controls="accordion-tab-5-content-5">
                                                5.	Bao bulk bag có chống thấm nước không?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-5" aria-labelledby="accordion-tab-5-heading-5" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>
                                                Không, bao jumbo cơ bản được làm từ vải dệt nên chúng không thể chống nước. Khách hàng thường sẽ tráng màng vải để chống ẩm.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
