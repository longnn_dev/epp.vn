<div class="product shadow-sm">
    <div class="product-image">
        <a href="{{$product->getUrl($locale)}}"><img class="img-fluid" src="{{$product->avatar}}"></a>
    </div>
    <div class="product-info">
        <div class="product-name"><a href="{{$product->getUrl($locale)}}">{{$product->tran($locale)->name}}</a></div>
        <div class="product-price">{{$product->getPriceRange()}} / {{trans('trans.pieces',[],$locale)}}</div>
        <div class="product-min-order">{{$product->min_order}} {{trans('trans.pieces',[],$locale)}} ({{trans('trans.field.minOrder',[],$locale)}})</div>
        <a href="javascript:;" class="add-cart" pid="{{$product->_id}}"><span><i class="fa fa-icon fa-cart-plus"></i> </span></a>
    </div>
</div>
