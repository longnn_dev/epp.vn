<section class="container">
    <h1 class="section-title"><span>CORE VALUES</span></h1>
    <div class="section-content">
        <div class="row core-value p-3 pl-5 pr-5">
            <div class="core-value-icon col-2">
                <img src="{{asset('beta/images/cvl1.png')}}" class="img-fluid"/>
            </div>
            <div class="core-value-content col-10">
                <div class="core-value-title">QUALITY</div>
                <div class="core-value-text">
                    <p>
                        With the system of quality control according to global standards: from checking the quality of materials, controlling quality products in production process, packing type and shipment. We commit to bring customers these product with <strong>“Stable Quality”.</strong>
                    </p>
                </div>
            </div>
        </div>
        <div class="row core-value p-3 pl-5 pr-5">
            <div class="core-value-icon col-2">
                <img src="{{asset('beta/images/cvl2.png')}}" class="img-fluid"/>
            </div>
            <div class="core-value-content col-10">
                <div class="core-value-title">PRICE</div>
                <div class="core-value-text">
                    <p>
                        We have many data in plastic packaging field, flexible buying method and material resource. We guarantee to bring customers these product with <strong>“Competitive Price”.</strong>
                    </p>
                </div>
            </div>
        </div>
        <div class="row core-value p-3 pl-5 pr-5">
            <div class="core-value-icon col-2">
                <img src="{{asset('beta/images/cvl3.png')}}" class="img-fluid"/>
            </div>
            <div class="core-value-content col-10">
                <div class="core-value-title">SERVICE</div>
                <div class="core-value-text">
                    <p>
                        With professional sales and customer service team, we commit to bring customers <strong>“Professional Service”</strong> such as: quick time for receiving quotation, direct reference price on website, suitable consulting about design, flexible delivery, various term of payment and active customer service after selling.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
