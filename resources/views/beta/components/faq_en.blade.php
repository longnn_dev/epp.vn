<section class="container no-background no-border">
    <div class="row service-list">
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_phone_1243689.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Quotation</div>
                <div class="desc mt-3">Offer the reference quotation for Jumbo bag’s production.</div>
            </div>
        </div>
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_message_1243690.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Consultation</div>
                <div class="desc mt-3">Consulting and designing Jumbo bag.</div>
            </div>
        </div>
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_bag_1243671.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Orders</div>
                <div class="desc mt-3">Take an order to produce Jumbo bag.</div>
            </div>
        </div>
        <div class="service col-6 col-sm-6 col-md-6 col-lg-3">
            <div class="service-inner shadow-sm">
                <div>
                    <img src="{{asset('frontend/img/iconfinder_thefreeforty_register_1243707.png')}}"
                         class="img-fluid">
                </div>
                <div class="title mt-2">Support</div>
                <div class="desc mt-3">24/7 support to customer complaints and inquiries</div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <h1 class="section-title"><span>FAQ</span></h1>
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="nav nav-pills faq-nav" id="faq-tabs" role="tablist" aria-orientation="vertical">
                        <a href="#tab1" class="nav-link active" data-toggle="pill" role="tab" aria-controls="tab1" aria-selected="true">
                            <i class="mdi mdi-help-circle"></i> Our Company
                        </a>
                        <a href="#tab2" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab2" aria-selected="false">
                            <i class="mdi mdi-account"></i> FIBC Bags
                        </a>
                        <a href="#tab3" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab3" aria-selected="false">
                            <i class="mdi mdi-account-settings"></i> Quotation
                        </a>
                        <a href="#tab4" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab4" aria-selected="false">
                            <i class="mdi mdi-heart"></i> Order
                        </a>
                        <a href="#tab5" class="nav-link" data-toggle="pill" role="tab" aria-controls="tab5" aria-selected="false">
                            <i class="mdi mdi-coin"></i> Specification
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content" id="faq-tab-content">
                        <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
                            <div class="accordion" id="accordion-tab-1">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-1-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-1" aria-expanded="false" aria-controls="accordion-tab-1-content-1">
                                                1.	What is your main product?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-1-content-1" aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                                        <div class="card-body">
                                            <p>We mainly focus on FIBC Bags also known as Jumbo bag, big bag, bulk bag, container bag.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-1-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-2" aria-expanded="false" aria-controls="accordion-tab-1-content-2">
                                                2.  Before make the order, can I visit your factory?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-1-content-2" aria-labelledby="accordion-tab-1-heading-2" data-parent="#accordion-tab-1">
                                        <div class="card-body">
                                            <p>Yes, warmly welcome.  Before you come, please let us know your schedule, your arriving time, we can come to pick you up.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2" role="tabpanel" aria-labelledby="tab2">
                            <div class="accordion" id="accordion-tab-2">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-1" aria-expanded="false" aria-controls="accordion-tab-2-content-1">
                                                1.	What are FIBCs?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-2-content-1" aria-labelledby="accordion-tab-2-heading-1" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>“Flexible Intermediate Bulk Containers” is the official name for bulk bags, although many companies refer to FIBCs as a bulka bags, tote jumbo bags, bulk bag, etc.</p>
                                            <p>Bulk bags can have widely varying dimensions, based on user need and usability, but typically hold about 500 kg–2000 kgs of product.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-2" aria-expanded="false" aria-controls="accordion-tab-2-content-2">
                                                2.	What are FIBCs made from?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-2" aria-labelledby="accordion-tab-2-heading-2" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>The main material used to make FIBC bags is polypropylene, which is a petroleum based product.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-3" aria-expanded="false" aria-controls="accordion-tab-2-content-3">
                                                3.	What size do FIBC bags come in?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-3" aria-labelledby="accordion-tab-2-heading-3" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>Bulk bags come in many different sizes and styles, and can be customized into almost any 3 dimensional size possible for your product.  There are limits to the sizes, but a standard bulk bag base dimension is 90 cm x 90 cm (35" x 35”), while the height of a bulk bag can range up to 120cm (96”) plus inches tall.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-4">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-4" aria-expanded="false" aria-controls="accordion-tab-2-content-4">
                                                4.	What types of fibcs are available and what applications are fibcs (bulk bags) typically used for?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-4" aria-labelledby="accordion-tab-2-heading-4" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>There are many common types of FIBCs available in the market place.  The most commonly used bulk bags are constructed in the U Panel or Circular configuration and might incorporate a simple PE liner or no liner at all.  Much of how bulk bags are referred to has to do with their construction, i.e., 4 Panel, U Panel, Circular, or their application, such as Type B, or Baffle Bags.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-2-heading-5">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-2-content-5" aria-expanded="false" aria-controls="accordion-tab-2-content-5">
                                                5.	What does 5:1 SF mean?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-2-content-5" aria-labelledby="accordion-tab-2-heading-5" data-parent="#accordion-tab-2">
                                        <div class="card-body">
                                            <p>A standard bulk bag is rated at a 5:1 Safety Factor Ratio as per ISO 21898 norms, which means that the bag is specified to hold 5 times the amount of the bags safe working load.  A 5:1 Safety Ratio is for single use or single trip bags.   The SF for a multi-trip bag is 6:1.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3" role="tabpanel" aria-labelledby="tab3">
                            <div class="accordion" id="accordion-tab-3">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-3-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-1" aria-expanded="false" aria-controls="accordion-tab-3-content-1">
                                                1.	What information should I provide to get a quotation?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-3-content-1" aria-labelledby="accordion-tab-3-heading-1" data-parent="#accordion-tab-3">
                                        <div class="card-body">
                                            <p>If you are interested in our product, please kindly tell us the loading weight, size, color, usage, the quantity you need. We can design the perfect bag for you. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-3-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-2" aria-expanded="false" aria-controls="accordion-tab-3-content-2">
                                                2.	How long can I get the quotation?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-3-content-2" aria-labelledby="accordion-tab-3-heading-2" data-parent="#accordion-tab-3">
                                        <div class="card-body">
                                            <p>It only takes 10 minutes to receive detail quotation since you send us specification.  </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-3-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-3-content-3" aria-expanded="false" aria-controls="accordion-tab-3-content-3">
                                                3.	How can I get samples from you?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-3-content-3" aria-labelledby="accordion-tab-3-heading-3" data-parent="#accordion-tab-3">
                                        <div class="card-body">
                                            <p>We’re willing to provide sample to you. The samples are FREE, you only need to pay for the freight. We can send sample via global express carrier such as DHL, FEDEX,….</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4" role="tabpanel" aria-labelledby="tab4">
                            <div class="accordion" id="accordion-tab-4">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-1" aria-expanded="false" aria-controls="accordion-tab-4-content-1">
                                                1.	Do you have any MOQ limit for bulk bags order?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-4-content-1" aria-labelledby="accordion-tab-4-heading-1" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>We only have low MOQ, 1 piece for sample checking is available.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-2" aria-expanded="false" aria-controls="accordion-tab-4-content-2">
                                                2.	How long does an order take?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-4-content-2" aria-labelledby="accordion-tab-4-heading-2" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>It depends on your order quantity. If your quantity less than 1000 pcs, it’s about 15 days. If  it’s greater than 5000 pcs, leading time is 30 days since deposited.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-3" aria-expanded="false" aria-controls="accordion-tab-4-content-3">
                                                3.	How do you inspect the quality of our order?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-4-content-3" aria-labelledby="accordion-tab-4-heading-3" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>We have the perfect methods, equipment, and also expert QC to inspect the quality of products.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-4-heading-4">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-4-content-4" aria-expanded="false" aria-controls="accordion-tab-4-content-4">
                                                4.	How to proceed an order for big bag?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-4-content-4" aria-labelledby="accordion-tab-4-heading-4" data-parent="#accordion-tab-4">
                                        <div class="card-body">
                                            <p>Firstly, let us know your requirements or application.</p>
                                            <p>Secondly, We quote according to your requirements or our suggestions.</p>
                                            <p>Thirdly, You confirms the samples and places deposit for formal order.</p>
                                            <p>Fourthly, We arrange the production.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab5" role="tabpanel" aria-labelledby="tab5">
                            <div class="accordion" id="accordion-tab-5">
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-1">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-1" aria-expanded="false" aria-controls="accordion-tab-5-content-1">
                                                1.	Can you print onto bulk bags?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="accordion-tab-5-content-1" aria-labelledby="accordion-tab-5-heading-1" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>Yes, bulk bags can be printed. Usually the printing is on the side panels. Print colours are usually one or two colours, but we have the capability to print with more colours, which will affect the cost. We can also print custom logos.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-2">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-2" aria-expanded="false" aria-controls="accordion-tab-5-content-2">
                                                2.	How do I attach my documents to FIBCs?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-2" aria-labelledby="accordion-tab-5-heading-2" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>Typically documents are attached by use of a document pouch sewn to the seam of the bulka bags. A document pouch can have a ziplock opening or a standard opening.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-3">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-3" aria-expanded="false" aria-controls="accordion-tab-5-content-3">
                                                3.	Can bulk bags have liners?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-3" aria-labelledby="accordion-tab-5-heading-3" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>Yes, liners can be inserted loosely or attached to fabric. There are different types of liners for different applications, but the main two are tube liners and form fit liners.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-4">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-4" aria-expanded="false" aria-controls="accordion-tab-5-content-4">
                                                4.	What colour fabric can I choose from?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-4" aria-labelledby="accordion-tab-5-heading-4" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>
                                                The standard fabric colour is white, however, we can make specific coloured fabric. Coloured fabric is considered a special order and has minimum requirements.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="accordion-tab-5-heading-5">
                                        <h5>
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#accordion-tab-5-content-5" aria-expanded="false" aria-controls="accordion-tab-5-content-5">
                                                5.	Are bulk bags water tight or waterproof?
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="collapse" id="accordion-tab-5-content-5" aria-labelledby="accordion-tab-5-heading-5" data-parent="#accordion-tab-5">
                                        <div class="card-body">
                                            <p>
                                                No, being, at a basic level, made from woven material, they are not “waterproof“. Users usually want a coating or lamination as a moisture barrier.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
