@extends('beta.template.layout')
@section('page_script')
    <script src="{{asset('beta/js/components/flexslider/jquery.flexslider.js')}}" type="text/javascript"></script>
    <script src="{{asset('beta/js/pages/product.js')}}" type="text/javascript" ></script>
@endsection
@section('page_css')
    <link href="{{asset('beta/css/imageGrid.css')}}" rel="stylesheet" />
    <link href="{{asset('beta/js/components/flexslider/flexslider.css')}}" rel="stylesheet" />
    <link href="{{asset('beta/js/components/flexslider/flexslider-fullscreen.css')}}" rel="stylesheet" />
    <style type="text/css">
        .company-about{
            font-family: Harmonia !important;
            color:#8c8c8c !important;
        }
        .company-about pre{
            overflow: hidden;
        }
        .company-about span{
            color:#8c8c8c !important;
            font-family: Harmonia !important;
        }

    </style>
@endsection
@section('topBanner')
    <div class="top-banner-2 shadow-sm">
        <img src="{{asset('beta/images/top_banner_mini.png')}}" class="img-fluid" />
    </div>
@endsection
@section('content')
    <div class="fullscreen-slide preview" style="display:none">
        <a href="javascript:;" class="full-screen-act close" title="Fullscreen"></a>
        <div id="fm-slider" class="flexslider fullscreen">
            <div class="slider-buttons">
                <a href="#" title="next"><span class="but-nextdetail flex-next"><i class=" fa fa-icon fa-chevron-right"></i> </span> </a>
                <a href="#" title="prev"><span class="but-predetail flex-prev"><i class=" fa fa-icon fa-chevron-left"></i></span> </a>
            </div>
            <ul class="slides" id="slide-thumbs">

            </ul>
        </div>
        <div id="ft-slider" class="flexslider img-list-slide">
            <div class="slider-buttons2">
                <a class="font-icon icon-presmall flex-prev flex-disabled" href="#" title="prev">
                    <i class=" fa fa-icon fa-chevron-left"></i>
                </a>
                <a class="font-icon icon-nextsmall flex-next" href="#" title="next">
                    <i class=" fa fa-icon fa-chevron-right"></i>
                </a>
            </div>
            <ul class="slides slideimg" id="slide-full">

            </ul>
        </div>
    </div>
    <section class="company-details container">
        <div class="row company-summary mb-3">
            <div class="col-md-6">
                <div class="company-avatar shadow">
                    <a href="javascript:;"><img class="project-image img-fluid" data-action="open-slide" src="{{$tran->avatar}}" title="{{$tran->name}}" alt="{{$tran->name}}"></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="company-name mb-3">
                    {{$tran->name}}
                </div>
                <div class="address mb-3">
                    <div><i class="fa fa-map-marked"> {{trans('trans.field.location',[],$locale)}}</i></div>
                    <div class="text-detail">{{$tran->address}}</div>
                </div>
                <div class="phonenumber mb-3">
                    <div><i class="fa fa-phone"> {{trans('trans.field.phone',[],$locale)}}</i></div>
                    @foreach($partner->phone_numbers as $phone_number)
                    <div class="text-detail">{{$phone_number}}</div>
                    @endforeach
                </div>
                <div class="email mb-3">
                    <div><i class="fa fa-envelope"> {{trans('trans.field.email',[],$locale)}}</i></div>
                    @foreach($partner->emails as $email)
                        <div class="text-detail">{{$email}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <div class="company-about"> {!! $tran->about !!}</div>
            </div>
        </div>
    </section>
    @if(isset($tran->properties) && count($tran->properties)>0)
    <section class="container">
        <div class="company-attr section-content">
            <div class="row">
            @foreach($tran->properties as $prop)
                <div class="col-md-6 mb-3">
                    <div class="row">
                        <div class="col-md-6 attr-key">{{$prop['key']}} :</div>
                        <div class="col-md-6 attr-value crop-text">{{$prop['value']}}</div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </section>
    @endif
    <section class="company-details container">
        <h1 class="section-title"><span>{{trans('trans.partner.boardOfManager',[],$locale)}}</span></h1>
        <div class="section-content">
            <div class="company-bom row">
                <div class="col-2"></div>
                <div class="col-8">
                <a href="javascript:;">
                    <img src="{{$tran->bom}}" class="img-fluid" data-action="open-slide" />
                </a>
                </div>
                <div class="col-2"></div>
            </div>
        </div>
    </section>
    <section class="company-details container">
        <h1 class="section-title"><span>{{trans('trans.field.photo',[],$locale)}}</span></h1>
        <div class="section-content">
            <div class="image-grid">
                {!! imageGrid($tran->photos,3) !!}
            </div>
        </div>
    </section>
    <section class="container">
        <h1 class="section-title"><span>VIDEOS</span></h1>
        <div class="section-content">
            <div class="row">
                @foreach($tran->videos as $video)
                    <div class="col-md-6 mb-4 ">
                        <div class="company-video">
                            <iframe width="100%" src="{{$video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
