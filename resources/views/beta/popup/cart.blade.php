<div class="modal fade" id="modal_cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="p-4">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6 block-left">
                        <p class="cart-title1">Cart</p>
                        <div class="cart-products">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 block-right">
                        <p class="cart-title1">Information</p>
                        <div class="mb-4">
                            <form style="text-align: center">
                                @csrf
                                <div class="input-group mb-2">
                                    <input class="form-control" type="text" id="input_name" name="input_name" placeholder="Name (*)" required value="name"/>
                                </div>
                                <div class="input-group mb-2">
                                    <input class="form-control" type="email" id="input_email" name="input_email" placeholder="Email (*)" required value="email@email.com"/>
                                </div>
                                <div class="input-group mb-2">
                                    <input class="form-control" type="text" id="input_phone" name="input_phone" placeholder="Phone number (*)" required pattern="(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{2,4})" value="0979477303"/>
                                </div>
                                <div class="input-group mb-2">
                                    <input class="form-control" type="tel" id="input_company" name="input_company" placeholder="Company" value="company"/>
                                </div>
                                <div class="input-group mb-4">
                                    <textarea class="form-control" id="input_message" placeholder="Message" data-bts-init-val="message"></textarea>
                                </div>
                                <div class="input-group">
                                    <button class="btn btn-success" type="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <button type="button" class="close close-modal" data-dismiss="modal">&times;</button>
            </div>
        </div>
    </div>
</div>
</div>
