@extends('beta.template.layout')
@section('page_css')
    <link href="{{asset('beta/components/faq/faq.css')}}" rel="stylesheet" />
@endsection
@section('topBanner')
    <div class="top-banner-2 shadow-sm">
        <img src="{{asset('beta/images/top_banner_mini.png')}}" class="img-fluid"/>
    </div>
@endsection
@section('content')

    @if(\Illuminate\Support\Facades\App::getLocale()==='en')
        @include('beta.components.faq_en')
    @else
        @include('beta.components.faq_vi')
    @endif
@endsection
