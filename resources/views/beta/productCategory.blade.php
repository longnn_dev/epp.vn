@extends('beta.template.layout')
@section('topBanner')
    <div class="top-banner-2 shadow-sm">
        <img src="{{asset('beta/images/top_banner_mini.png')}}" class="img-fluid" />
    </div>
@endsection
@section('content')
<section class="container">
    <h1 class="section-title"><span>{{$productCategory->tran($locale)->name}}</span></h1>
    <div class="section-content">
        <div class="row">
            <div class="col-md-3">
                <div class="select-cat mb-3">
                    <div class="header cat-item">{{trans('trans.field.productCategory')}}</div>
                    @foreach($pCats as $cat)
                        <div class="cat-item"><a href="{{$cat->getUrl($locale)}}" class="{{$cat->_id==$productCategory->_id?'active':''}}">{{$cat->tran($locale)->name}}</a></div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    @foreach($products as $product)
                        <div class="col-md-6 mb-4">
                            @include('beta.components.product',['product'=>$product])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <h1 class="section-title"><span>{{trans('trans.product.featureProduct',[], $locale)}}</span></h1>
    <div class="section-content">
        <div class="row">
            @foreach($fProducts as $product)
                <div class="col-md-4 mb-4 ">
                    @include('beta.components.product',['product'=>$product])
                </div>
            @endforeach
        </div>
    </div>
</section>
<section class="container">
    <h1 class="section-title"><span>{{trans('trans.field.partner',[], $locale)}}</span></h1>
    <div class="section-content">
        <div class="row">
            @foreach($partners as $partner)
                <div class="col-md-3 mb-4 ">
                    <div class="partner-logo shadow-sm">
                        <img src="{{$partner->logo}}" class="img-fluid" />
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
