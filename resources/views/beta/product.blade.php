@extends('beta.template.layout')
@section('page_script')
    <script src="{{asset('beta/js/components/flexslider/jquery.flexslider.js')}}" type="text/javascript"></script>
    <script src="{{asset('beta/js/pages/product.js')}}" type="text/javascript" ></script>
@endsection
@section('page_css')
    <link href="{{asset('beta/js/components/flexslider/flexslider.css')}}" rel="stylesheet" />
    <link href="{{asset('beta/js/components/flexslider/flexslider-fullscreen.css')}}" rel="stylesheet" />
@endsection
@section('topBanner')
<div class="top-banner-2 shadow-sm">
    <img src="{{asset('beta/images/top_banner_mini.png')}}" class="img-fluid" />
</div>
@endsection
@section('content')
<div class="fullscreen-slide preview" style="display:none">
        <a href="javascript:;" class="full-screen-act close" title="Fullscreen"></a>
        <div id="fm-slider" class="flexslider fullscreen">
            <div class="slider-buttons">
                <a href="#" title="next"><span class="but-nextdetail flex-next"><i class=" fa fa-icon fa-chevron-right"></i> </span> </a>
                <a href="#" title="prev"><span class="but-predetail flex-prev"><i class=" fa fa-icon fa-chevron-left"></i></span> </a>
            </div>
            <ul class="slides" id="slide-thumbs">

            </ul>
        </div>
        <div id="ft-slider" class="flexslider img-list-slide">
            <div class="slider-buttons2">
                <a class="font-icon icon-presmall flex-prev flex-disabled" href="#" title="prev">
                    <i class=" fa fa-icon fa-chevron-left"></i>
                </a>
                <a class="font-icon icon-nextsmall flex-next" href="#" title="next">
                    <i class=" fa fa-icon fa-chevron-right"></i>
                </a>
            </div>
            <ul class="slides slideimg" id="slide-full">

            </ul>
        </div>
    </div>
<section class="container">
    <div class="row product-summary mt-3 mb-3">
        <div class="col-md-5">
            <div class="main-image">
                <a href="javascript:;"><img class="project-image img-fluid" data-action="open-slide" src="{{$product->avatar}}" title="{{$tran->name}}" alt="{{$tran->name}}"></a>
            </div>
            <div class="photos row">
                @foreach($product->photos as $photo)
                    <div class="photo col-md-3 col-3">
                        <a href="javascript:;"><img class="project-image img-fluid" data-action="open-slide" src="{{$photo}}" title="{{$tran->name}}" alt="{{$tran->name}}"></a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-7">
            <div class="info-block">
                <div class="block-title product-name">{{$tran->name}}</div>
                <div class="product-prices row">
                    @foreach($product->prices as $price)
                        <div class="product-price col-3">
                            <div class="price-amount">{{$price['key']}} {{trans('trans.field.prices')}}</div>
                            <div class="price-value">{{trans('trans.currency')}} {{$price['value']}}</div>
                        </div>
                    @endforeach
                </div>
                <div class="product-min-order">{{$product->min_order}} {{trans('trans.pieces')}} ( {{trans('trans.field.minOrder')}} )</div>
            </div>
            <div class="divider-dot"></div>
            <div class="info-block">
                <div class="block-title"> {{trans('trans.field.color')}}</div>
                <div class="block-content">
                    <div class="color-list">
                    @foreach($product->colors as $color)
                        <a href="#" class="product-color" style="background:{{$color}}"></a>
                    @endforeach
                    </div>
                </div>
            </div>
            <div class="divider-dot"></div>
            <div class="info-block">
                <div class="block-title"> {{trans('trans.field.size')}}</div>
                <div class="block-content">
                    <div class="color-list">
                        @foreach($product->sizes as $size)
                            <a href="#" class="product-size">{{$size}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="divider-dot"></div>
            <div class="info-block">
                <div class="block-title"> {{trans('trans.product.leadtime.quantity')}}</div>
                <div class="block-content">
                    <form class="order-form row">
                        <div class="input-group mb-3 col-4">
                            <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" id="btn_addcart" pid="{{$product->_id}}">Order</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="divider-dot"></div>
    <div class="info-block product-overview">
        <h2 class="block-title">{{trans('trans.section.overview',[],$locale)}}</h2>
        <div class="row block-content">
            @foreach($tran->quick_details as $property)
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-4 property-name">{{$property['key']}} :</div>
                        <div class="col-8 property-value">{{$property['value']}}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="divider-dot"></div>
    <div class="info-block product-supply">
        <h2 class="block-title">{{trans('trans.field.supplyAbility',[],$locale)}}</h2>
        <div class="row block-content">
            @foreach($tran->supply_ability as $supply)
                <div class="col-3 property-name">{{$supply['key']}} :</div>
                <div class="col-6 property-value">{{$supply['value']}}</div>
            @endforeach
        </div>
    </div>
    <div class="divider-dot"></div>
    <div class="info-block product-pd">
        <h2 class="block-title">{{trans('trans.field.packagingDelivery',[],$locale)}}</h2>
        <div class="block-content">
            @foreach($tran->packaging_delivery as $pd)
                <div class="row">
                    <div class="col-3 property-name">{{$pd['key']}} :</div>
                    <div class="col-6 property-value">{{$pd['value']}}</div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@if(isset($product->videos))
<section class="container">
    <h1 class="section-title"><span>VIDEOS</span></h1>
    <div class="section-content">
        <div class="row">
            @foreach($product->videos as $video)
                <div class="col-md-6 mb-4 ">
                    <div class="company-video">
                        <iframe width="100%" src="{{$video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif
<section class="container">
    <h1 class="section-title"><span>{{trans('trans.product.featureProduct',[],$locale)}}</span></h1>
    <div class="section-content">
        <div class="row">
            @foreach($fProducts as $product)
                <div class="col-md-4 mb-4 ">
                    @include('beta.components.product',['product'=>$product])
                </div>
            @endforeach
        </div>
    </div>
</section>
<section class="container">
    <h1 class="section-title"><span>{{trans('trans.field.partner',[],$locale)}}</span></h1>
    <div class="section-content">
        <div class="row">
            @foreach($partners as $partner)
                <div class="col-md-3 mb-4 ">
                    <div class="partner-logo shadow-sm">
                        <a href="{{$partner->getUrl($locale)}}"> <img src="{{$partner->logo}}" class="img-fluid" /></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
