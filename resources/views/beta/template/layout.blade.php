<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{isset($meta->meta_title)?$meta->meta_title:'EPP Vietnam'}}</title>
    <meta charset="UTF-8">
    <meta name="description" content="{{$meta->meta_description?$meta->meta_description:''}}">
    <meta name="keywords" content="{{$meta->meta_keywords}}">
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:title" content="{{$meta->meta_title}}">
    <meta property="og:description" content="{{$meta->meta_description}}">
    <meta property="og:image" content="{{$meta->meta_image}}">
    <meta property="og:image:url" content="{{$meta->meta_image}}">
    <meta property="fb:app_id" content="2102105389891690">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="320">
    <meta property="og:type" content="article">
    <meta property="og:site_name" content="epp.vn">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--begin common-->
    <script src="https://kit.fontawesome.com/d6f1e4ffb3.js" crossorigin="anonymous"></script>
    <link href="{{asset('beta/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet">
    <link href="{{asset('beta/js/components/jquery-autocomplete/style.css')}}" rel="stylesheet" />
    <link href="{{asset('beta/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('beta/css/font.css')}}" rel="stylesheet">
    <link href="{{asset('beta/css/cart.css')}}" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('http://epp.vn//userfiles/images/company/epp/logo_square.png')}}" />
    @yield('page_css')
</head>
<body>
<div class="nav-bar shadow-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-4">
                <div class="menu-button">
                    <a href="javascript:;"> <i class="fa fa-bars"></i></a>
                </div>
                <div class="welcome">Welcome to EPP Vietnam</div>
            </div>
            <div class="col-md-6 col-8 text-right">
                <ul class="right-menu">
                    <li>
                        <a href="javascript:;"><i class="fa fa-icon fa-user"></i> {{trans('trans.login',[],$locale)}}</a>
                    </li>
                    <li>
                        <a href="javascript:;" class="show-cart"><i class="fa fa-icon fa-shopping-cart"></i> {{trans('trans.cart',[],$locale)}}</a>
                    </li>
                    <li>
                        <a class="dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="flag-icon"><img src="{{asset('beta/images/flags/'.$locale.'.png')}}" /> </span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown09">
                            <a class="dropdown-item" href="{{route('frontend.home.seLocale','en')}}"><span class="flag-icon"><img src="{{asset('beta/images/flags/en.png')}}" /></span>  {{trans('language.english')}}</a>
                            <a class="dropdown-item" href="{{route('frontend.home.seLocale','vi')}}"><span class="flag-icon"><img src="{{asset('beta/images/flags/vi.png')}}" /></span>  {{trans('language.vietnamese')}}</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="header-logo shadow-sm">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <a href="{{route('frontend.home')}}">
                    <img src="{{asset('beta/images/epp-logo.png')}}" class="m-1"/>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="top-menu shadow-sm">
    <div class="container">
        <div class="row">
            <div class="top-menu-inner col-md-9 col-6">
                <ul>
                    <li>
                        <a class="menu-item" href="{{route('frontend.home')}}">{{trans('trans.nav.home',[],$locale)}}</a>
                    </li>
                    <li>
                        <a class="menu-item dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{trans('trans.nav.product',[],$locale)}}</a>
                        <div class="dropdown-menu">
                            @foreach($pCats as $pCat)
                            <a class="dropdown-item" href="{{$pCat->getUrl($locale)}}">{{$pCat->tran($locale)->name}}</a>
                            @endforeach
                        </div>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('frontend.partners')}}">{{trans('trans.nav.partner',[],$locale)}}</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('frontend.services')}}">{{trans('trans.nav.service',[],$locale)}}</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('frontend.documents')}}">{{trans('trans.nav.document',[],$locale)}}</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('frontend.news')}}">{{trans('trans.nav.news',[],$locale)}}</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('frontend.about')}}">{{trans('trans.nav.about',[],$locale)}}</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{route('frontend.contact')}}">{{trans('trans.nav.contact',[],$locale)}}</a>
                    </li>
                </ul>

            </div>
            <div class="col-md-3 col">
                <form class="form-search">
                    <input id="search-input" class="form-control mt-2" placeholder="{{trans('trans.search',[],$locale)}}"/>
                </form>
            </div>
        </div>

        {{--modal cart--}}
        @include('beta.popup.cart')
    </div>
</div>
@yield('topBanner')
<div class="content">
    @yield('content')
</div>
<footer id="footer">
    <div class="inner container">
        <div class="row">
            <div class="col-md col-xs-6">
                <div class="icon">
                    <span class="fa fa-icon fa-phone"></span>
                </div>
                <div class="info">
                    <div class="title">Hotline (24/7)</div>
                    <div class="value"><a href="tel:{{$ourCompany->phone_numbers[0]}}">{{$ourCompany->phone_numbers[0]}}</a></div>
                </div>
            </div>
            <div class="col-md col-xs-6">
                <div class="icon">
                    <span class="fa fa-icon fa-envelope"></span>
                </div>
                <div class="info">
                    <div class="title">Customer Service</div>
                    <div class="value"><a href="tel:{{$ourCompany->phone_numbers[1]}}">{{$ourCompany->phone_numbers[1]}}</a></div>
                </div>
            </div>
            <div class="col-md col-xs-6">
                <div class="icon">
                    <span class="fa fa-icon fa-envelope"></span>
                </div>
                <div class="info">
                    <div class="title">Sales</div>
                    <div class="value"><a href="mailto:{{$ourCompany->emails[1]}}">{{$ourCompany->emails[1]}}</a></div>
                </div>
            </div>
            <div class="col-md col-xs-6">
                <div class="icon">
                    <span class="fa fa-icon fa-envelope"></span>
                </div>
                <div class="info">
                    <div class="title">Contact</div>
                    <div class="value"><a href="mailto:{{$ourCompany->emails[2]}}">{{$ourCompany->emails[2]}}</a></div>
                </div>
            </div>
        </div>
        <div class="divider"></div>
        <div class="row">
            <div class="col-md col-xs-6">
                <div class="logo">
                    <span class="">EPP VIETNAM</span>
                </div>
                <div class="info">
                    <div class="address">Ecopark, Hung Yen</div>
                    <div class="social">
                        <a href="#"><i class="fab fa-facebook-square"></i> </a>
                        <a href="#"><i class="fab fa-twitter-square"></i> </a>
                    </div>
                </div>
            </div>
            <div class="col-md col-xs-6">
                <div class="title">Company</div>
                <div class="footer-link"><a href="{{route('frontend.about')}}">About EPP</a></div>
                <div class="footer-link"><a href="{{route('frontend.services')}}">Services</a></div>
                <div class="footer-link"><a href="{{route('frontend.contact')}}">Contact</a></div>
            </div>
            <div class="col-md col-xs-6">
                <div class="title">Services</div>
                <div class="footer-link"><a href="http://epp.vn/product-category/5e08ba57e46c4a4c951895a2/jumbo-bag">Products</a></div>
                <div class="footer-link"><a href="{{route('frontend.partners')}}">Partners</a></div>
            </div>
            <div class="col-md col-xs-6">
                <div class="title">Information</div>
                <div class="footer-link"><a href="{{route('frontend.news')}}">Market News</a></div>
                <div class="footer-link"><a href="#">Packaging Tech</a></div>
            </div>
        </div>
        <div class="divider"></div>
        <div class="row">
            <div class="col">
                <div class="footer-link"><a href="#"> Copyright © 2019 - EPP. All Rights Reserved.</a></div>
            </div>
            <div class="col">
                <div class="footer-link text-right"><a href="#">Policy</a><a href="#"> Term of uses</a></div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('beta/js/jquery-3.4.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('beta/js/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('beta/js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('beta/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('beta/js/axios.min.js')}}" type="text/javascript"></script>
<script src="{{asset('backend/vendors/general/sweetalert2/dist/sweetalert2.js')}}" type="text/javascript"></script>
<script src="{{asset('beta/js/components/jquery-autocomplete/jquery.autocomplete.min.js')}}"></script>
<script src="{{asset('beta/js/page.js')}}" type="text/javascript"></script>
<script src="{{asset('beta/js/cart.js')}}" type="text/javascript"></script>
@yield('page_script')
</body>
</html>
