@extends('beta.template.layout')

@section('content')
    <div class="container">
        <div class="row partner-list">
            @foreach($docs as $doc)
                <div class="col-12 col-md-6">
                    <div class="partner shadow-sm mb-4">
                        <div class="row ">
                            <div class="col-md-4 ">
                                <div class="partner-logo">
                                    <a href="#">
                                        <img src="{{$doc->cover_url}}" class="img-fluid" />
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="brand"><a href="#">{{$doc->name}}</a></div>
                                <div class="phonenumber"><i class="fa fa-sticky-note-o"></i> {{$doc->content}} </div>
                                <div class="email"><a href="{{$doc->file_url}}" target="_blank"><i class="fa fa-download"></i> Download </a></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
