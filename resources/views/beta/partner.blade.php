@extends('beta.template.layout')

@section('content')
    <div class="container">
        <div class="row partner-list">
            @foreach($partners as $partner)
                <div class="col-12 col-md-6">
                    <div class="partner shadow-sm mb-4">
                    <div class="row ">
                        <div class="col-md-4 ">
                            <div class="partner-logo">
                                <a href="{{$partner->getUrl($locale)}}">
                                    <img src="{{$partner->logo}}" class="img-fluid" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="brand"><a href="{{$partner->getUrl($locale)}}">{{$partner->brand}}</a></div>
                            <div class="phonenumber"><i class="fa fa-phone"></i> {{$partner->phone_numbers[0]}}</div>
                            <div class="email"><i class="fa fa-envelope"></i> {{$partner->emails[0]}}</div>
                        </div>
                    </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
