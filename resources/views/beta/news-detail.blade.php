@extends('beta.template.layout')
@section('page_css')
    <link href="{{asset('beta/css/blog_home.css')}}" rel="stylesheet">
    <link href="{{asset('beta/css/component.css')}}" rel="stylesheet">
    <style rel="stylesheet">
        blockquote {
            background: #f9f9f9;
            border-left: 10px solid #ccc;
            margin: 1.5em 10px;
            padding: 0.5em 10px;
            quotes: "\201C""\201D""\2018""\2019";
        }
        blockquote:before {
            color: #ccc;
            content: open-quote;
            font-size: 4em;
            line-height: 0.1em;
            margin-right: 0.25em;
            vertical-align: -0.4em;
        }
        blockquote p {
            display: inline;
        }
        blockquote a{
            color:#4990e2;
        }
        .article-content a{
            color:#4990e2;
        }
        .fb_iframe_widget_fluid span, iframe.fb_ltr { width: 100% !important; }
    </style>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=2102105389891690&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
@endsection
@section('topBanner')
    <div class="top-banner-2 shadow-sm">
        <img src="{{asset('beta/images/top_banner_mini.png')}}" class="img-fluid"/>
    </div>
@endsection
@section('content')
    <section class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="article-details">
                    <h1 class="article-title">{{$article->tran($locale)->name}}</h1>
                    <div class="article-info"><a href="#">{{$article->category->tran($locale)->name}}</a>  • {{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y')}}</div>
                    <div class="article-content">{!!$article->tran($locale)->content!!}</div>
                </div>
                <hr class="col-xs-12">
                <div class="block-type-2">
                    <div class="head">
                        <span>Related posts </span>
                    </div>
                    <div class="body">
                        <ul>
                            @foreach($sameTopics as $article)
                                <li class="related-article"><a href="{{$article->getUrl($locale)}}">{{$article->tran($locale)->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <hr class="col-xs-12">
                <div class="block-type-2">
                    <div class="head">
                        <span>Comments </span>
                    </div>
                    <div class="body">
                        <div class="fb-comments" data-href="{{$article->getUrl($locale)}}" data-numposts="5" data-width="100%"></div>
                    </div>
                </div>
                <hr class="col-xs-12">
                <div class="block-type-2">
                    <div class="head"></div>
                    <div class="body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                @if($previousPost)
                                    <div><a class="harmonia-semi-bold font-14 blue-link" href="{{$article->getUrl($locale)}}">Previous post</a></div>
                                    <div><span class="harmonia-regular font-14">{{$previousPost->tran($locale)->name}}</span></div>
                                @else
                                    <span class="harmonia-semi-bold font-14 blue-link">No previous post</span>
                                @endif
                            </div>
                            <div class="col-md-6 col-xs-12 text-right">
                                @if($nextPost)
                                    <div><a class="harmonia-semi-bold font-14 blue-link" href="{{$article->getUrl($locale)}}">Next post</a></div>
                                    <div><span class="harmonia-regular font-14">{{$nextPost->tran($locale)->name}}</span></div>
                                @else
                                    <span class="harmonia-semi-bold font-14 blue-link">No next post</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="col-md-12 col-lg-4">
                <div class="form-info-type-1 shadow-sm">
                    <div class="header">
                        <i class="far fa-envelope fa-lg icon"></i>
                        <h2 class="mt-2 title">Đừng bỏ lỡ thông tin</h2>
                    </div>
                    <div class="content">
                        <p>Cùng 50,000 nhà đầu tư bât động sản, nhận những thông tin mới nhất của thị trường được gửi qua email hàng tuần.</p>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <input class="form-control" name="email" placeholder="Địa chỉ email của bạn"/>
                            </div>
                            <button class="btn btn-primary w-100">Đăng ký</button>
                        </form>
                    </div>
                </div>
                @if(isset($randomArticles))
                    <div class="article-list article-top">
                        <h3 class="mt-3 mb-3 title">Hot news</h3>
                        @foreach($randomArticles as $article)
                            @if(isset($article->tran($locale)->name))
                            <div class="article-item article-item-simple row mb-3">
                                <div class="article-cover col-md-4 col-4">
                                    <a href="{{$article->getUrl($locale)}}"><img src="{{$article->avatar}}" class="img-fluid shadow"/></a>
                                </div>
                                <div class="article-content col-md-8 col-8">
                                    <div class="article-title crop-text-3"><a href="{{$article->getUrl($locale)}}">{{$article->tran($locale)->name}}</a></div>
                                    <div class="article-info harmonia-regular">{{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y')}}</div>
                                </div>
                            </div>
                            <hr class="col-xs-12">
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
                -->
        </div>
    </section>
@endsection
