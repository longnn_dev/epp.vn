@foreach($products as $product)
    <div class="cart-product shadow-sm mb-2">
        <div class="cover">
            {{--<img src="{{asset('beta/images/epp-logo.png')}}">--}}
            <img src="{{$product->avatar}}" class="img-fluid">
        </div>
        <div class="info">
            <div class="name">{{$product->tran($locale)->name}}</div>
            <div class="price">$ {{$product->getPriceRange()}} / Piece</div>
        </div>
        <button type="button" class="close close-modal" pid="{{$product->_id}}">&times;</button>
    </div>
@endforeach