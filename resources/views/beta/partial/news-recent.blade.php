@foreach($recents as $news)
    <div class="news">
        <div class="t-cover">
            <img src="{{$news->avatar}}" class="img-fluid">
        </div>
        @if($news->tran($locale))
            <div class="t-title"><a
                    href="{{route('frontend.news.detail',$news)}}">{{$news->tran($locale)->name}}</a>
            </div>
        @endif
    </div>
@endforeach

