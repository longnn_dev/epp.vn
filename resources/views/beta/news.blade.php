@extends('beta.template.layout')
@section('page_css')
    <link href="{{asset('beta/css/blog_home.css')}}" rel="stylesheet">
    <link href="{{asset('beta/css/component.css')}}" rel="stylesheet">
@endsection
@section('topBanner')
    <div class="top-banner-2 shadow-sm">
        <img src="{{asset('beta/images/top_banner_mini.png')}}" class="img-fluid"/>
    </div>
@endsection
@section('content')
    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="mt-3 mb-3 font-size-lg category-title">{{$category->tran($locale)->name}}</h2>
                <div id="carousel" class="carousel slide mt-3" data-ride="carousel" >
                    <div class="carousel-inner">
                        <?php $j=0 ;$slideCount = ceil(count($slides)/3); ?>
                        @for($i=0;$i<$slideCount;$i++)
                            <div class="carousel-item {{$i==0?'active':''}}">
                                <div class="row ">
                                    @for($m=0;$m<3;$m++)
                                        @if(isset($slides[$j]))
                                            <div class="col-md-4 carousel-sub-item">
                                                <a href="{{$slides[$j]->getUrl($locale)}}"><img class="d-block img-fluid" src="{{url($slides[$j]->avatar)}}" alt="First slide"></a>
                                                <div class="article-title crop-text-2">
                                                    <a href="{{$slides[$j]->getUrl($locale)}}">{{$slides[$j]->tran($locale)->name}}</a>
                                                </div>
                                            </div>
                                        @endif
                                        <?php $j++;?>
                                    @endfor
                                </div>
                            </div>
                        @endfor
                    </div>

                </div>
                <hr class="col-xs-12"/>
                <div class="article-list">
                    @foreach($articles as $article)
                        <div class="article-item row">
                            <div class="article-cover col-md-4">
                                <a href="{{$article->getUrl($locale)}}"><img src="{{$article->avatar}}" class="img-fluid "/></a>
                            </div>
                            <div class="article-content col-md-8">
                                <div class="article-title crop-text-2">
                                    <a href="{{$article->getUrl($locale)}}">{{$article->tran($locale)->name}}</a></div>
                                <div class="article-info"><a href="{{$article->getUrl($locale)}}">{{$article->category->tran($locale)->name}}</a> • {{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y')}}</div>
                                <div class="article-sapo crop-text-3">{{$article->tran($locale)->sapo}}</div>
                            </div>
                        </div>
                        <hr class="col-xs-12">
                    @endforeach
                </div>
                {{$articles->links()}}
            </div>
            <!--
            <div class="col-md-4">
                 <div class="form-info-type-1 ">
                    <div class="header">
                        <i class="far fa-envelope fa-lg icon"></i>
                        <h2 class="mt-2 title">Đừng bỏ lỡ thông tin</h2>
                    </div>
                    <div class="content">
                        <p>Cùng Epp Vietnam để có được thông tin về giá cả và các thông số sản phẩm bao bì FIBCs...</p>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <input class="form-control" name="email" placeholder="Địa chỉ email của bạn"/>
                            </div>
                            <button class="btn btn-primary w-100">Đăng ký</button>
                        </form>
                    </div>
                </div>
                <div class="article-list">
                    <h3 class="mt-3 mb-3 title category-title">Bài viết nổi bật</h3>
                    @foreach($randomArticles as $article)
                        <div class="article-item article-item-simple row mb-3">
                            <div class="article-cover col-md-4">
                                <a href="{{$article->getUrl($locale)}}"><img src="{{$article->avatar}}" class="img-fluid "/></a>
                            </div>
                            <div class="article-content col-md-8">
                                <div class="article-title crop-text-2"><a href="{{$article->getUrl($locale)}}">{{$article->tran($locale)->name}}</a></div>
                                <div class="article-info">{{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y')}}</div>
                            </div>
                        </div>
                        <hr class="col-xs-12">
                    @endforeach
                </div>
            </div>
            -->
        </div>
    </section>

@endsection
