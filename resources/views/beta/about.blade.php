@extends('beta.template.layout')
@section('page_css')
    <link href="{{asset('beta/css/page.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="container pt-4">
        <!--layout content-->
        <div class="shadow-sm t-about">
            <h1 class="section-title"><span>Overview</span></h1>
            <div class="sapo">
                {!! $company->tran($locale)->about !!}
            </div>

            <div class="row mt-5 activity-list">
                <div class="col-12 col-md-4 l-our mb-5">
                    <div class="item">
                        <img src="{{asset('frontend/img/ic_our_history.png')}}">
                        <div class="title mt-3">Our History</div>
                        <div class="content mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sagittis
                            egestas ante, sed viverra nunc tincidunt nec nteger nonsed condimntum elit, sit amet feugiat
                            lorem. Proin tempus sagittis velit vitae scelerisque.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 l-our mb-5">
                    <div class="item">
                        <img src="{{asset('frontend/img/ic_our_mission.png')}}">
                        <div class="title mt-3">Our History</div>
                        <div class="content mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sagittis
                            egestas ante, sed viverra nunc tincidunt nec nteger nonsed condimntum elit, sit amet feugiat
                            lorem. Proin tempus sagittis velit vitae scelerisque.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 l-our mb-5">
                    <div class="item">
                        <img src="{{asset('frontend/img/ic_whoweare.png')}}">
                        <div class="title mt-3">Our History</div>
                        <div class="content mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sagittis
                            egestas ante, sed viverra nunc tincidunt nec nteger nonsed condimntum elit, sit amet feugiat
                            lorem. Proin tempus sagittis velit vitae scelerisque.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--Our-Awesome-Team--}}
        <div class="shadow-sm mt-4 t-about">
            <h1 class="section-title"><span>Our Awesome Team</span></h1>
            <div class="row">
                @foreach($company->tran($locale)->photos as $photo)
                    <div class="col-6 col-md-4 mb-3">
                        <div class="shadow-sm item">
                            <img src="{{$photo}}" class="img-fluid">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


        {{--board--}}
        <div class="shadow-sm mt-4 t-about">
            <h1 class="section-title"><span>Board Of Manager</span></h1>
            <div class="mt-5 mb-5">
                <img src="{{asset('frontend/img/demo/board.png')}}" class="img-fluid">
            </div>
        </div>

    </div>
@endsection