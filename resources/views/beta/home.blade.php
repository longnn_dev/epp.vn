@extends('beta.template.layout')
@section('topBanner')
    <div class="top-banner shadow-sm carousel slide" data-ride="carousel" id="myCarousel">
        <ol class="carousel-indicators">
            @for($i=0;$i<count($slides);$i++)
            <li data-target="#myCarousel" data-slide-to="{{$i}}" class="{{$i==0?'active':''}}"></li>
            @endfor
        </ol>
        <div class="carousel-inner">
            @for($i=0;$i<count($slides);$i++)
            <div class="carousel-item {{$i==0?'active':''}}">
                <img src="{{$slides[$i]->url}}" class="img-fluid" style="width:100% !important;">
            </div>
            @endfor
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
@endsection
@section('content')
<section class="container">
    <h1 class="section-title"><span>{{trans('trans.field.productCategory')}}</span></h1>
    <div class="section-content">
        <div class="product-categories-tab zTabs">
            <div class="tab-nav">
                @for($i=0;$i<count($productCat);$i++)
                    <a href="#{{$productCat[$i]->_id}}" class="{{$i==0?'active':''}}">{{$productCat[$i]->tran($locale)->name}}</a>
                @endfor
            </div>
            @for($i=0;$i<count($productCat);$i++)
                <div class="tab-content {{$i==0?'active':''}}" id="{{$productCat[$i]->_id}}">
                    <div class="row">
                        @foreach($productCat[$i]->products as $product)
                            <div class="col-md-4 mb-3">
                                @include('beta.components.product',['product'=>$product])
                            </div>
                        @endforeach
                    </div>
                </div>
            @endfor
        </div>
    </div>
</section>
<section class="container">
    <h1 class="section-title"><span>{{trans('trans.home.profile.bigTitle')}}</span></h1>
    <div class="section-content">
        <div class="company-intro">
            <div class="row">
                <div class="col-md-6"  style="padding:30px 15px 30px 40px;">
                    <div class="company-video">

                        <img src="{{$ourCompanyTrans->avatar}}" class="img-fluid shadow-sm">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="company-about">
                        <div class="truncate-overflow">
                            {!! $ourCompany->tran($locale)->about !!}
                        </div>
                        <div class="mt-3">
                            <a href="{{$ourCompany->getUrl($locale)}}" class="read-more">{{trans('trans.field.readMore',[],$locale)}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="company-attr">
            <div class="row ">
                @foreach($ourCompanyTrans->properties as $prop)
                    <div class="col-md-6 mb-3">
                        <div class="row">
                            <div class="col-6 attr-key">{{$prop['key']}} :</div>
                            <div class="col-6 attr-value crop-text">{{$prop['value']}}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="company-photos">
            <div class="row">
                @foreach($ourCompanyTrans->photos as $photo)
                    <div class="col-md-4 mb-4">
                        <img src="{{$photo}}" class="img-fluid" />
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@if(\Illuminate\Support\Facades\App::getLocale()==='en')
    @include('beta.components.coreValue_en')
@else
    @include('beta.components.coreValue_vi')
@endif

<section class="container">
    <h1 class="section-title"><span>VIDEOS</span></h1>
    <div class="section-content">
        <div class="row">
            @if(isset($ourCompanyTrans->videos))
            @foreach($ourCompanyTrans->videos as $video)
                <div class="col-md-6 mb-4 ">
                    <div class="company-video">
                        <iframe width="100%" src="{{$video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<section class="container">
    <h1 class="section-title"><span>{{trans('trans.field.partner',[],$locale)}}</span></h1>
    <div class="section-content">
        <div class="row">
            @foreach($partners as $partner)
                <div class="col-md-3 mb-4 ">
                    <div class="partner-logo shadow-sm">
                        <a href="{{$partner->getUrl($locale)}}"> <img src="{{$partner->logo}}" class="img-fluid" /></a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
@section('page_css')
    <link rel="stylesheet" href="{{asset('beta/js/components/zTabs/zTabs.css')}}" />
@section('page_script')
    <script src="{{asset('beta/js/components/zTabs/zTabs.js')}}" type="text/javascript"></script>
    <script src="{{asset('beta/js/pages/home.js')}}" type="text/javascript"></script>
@endsection

