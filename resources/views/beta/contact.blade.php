@extends('beta.template.layout')
@section('page_css')
    <link href="{{asset('beta/css/page.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="container pt-4">
        <!--layout content-->
        <div class="t-contact shadow-sm">
            <div class="row">
                <div class="col-12 header">
                    <h1 class="section-title"><span>{{trans('trans.field.contact',[],$locale)}}</span></h1>
                    <div class="title1 mt-4">Get in touch with us</div>
                    <div class="title2 mt-1">We welcome you at our facility at any time!</div>
                </div>
            </div>
            <div class="row mt-3 mt-sm-5">
                <div class="col-12 col-sm-6">
                    <div class="location">
                        <div class="title">{{trans('trans.field.location',[],$locale)}}</div>
                        <div class="description">{{$tran->address}}</div>
                    </div>
                    <div class="location">
                        <div class="title">{{trans('trans.field.phone_number',[],$locale)}}</div>
                        @foreach($partner->phone_numbers as $phone_number)
                            <div class="description">{{$phone_number}}</div>
                        @endforeach
                    </div>
                    <div class="location">
                        <div class="title">{{trans('trans.field.email',[],$locale)}}</div>
                        @foreach($partner->emails as $email)
                            <div class="description">{{$email}}</div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <form class="contact-form">
                        <div class="input-group mb-2">
                            <input class="form-control" placeholder="{{trans('trans.field.name',[],$locale)}} *" type="text" id="input_name">
                        </div>
                        <div class="input-group mb-2">
                            <input class="form-control" placeholder="{{trans('trans.field.email',[],$locale)}} *" type="email" id="input_email">
                        </div>
                        <div class="input-group mb-2">
                            <textarea class="form-control" placeholder="{{trans('trans.field.message',[],$locale)}} *" type="text" id="input_message"></textarea>
                        </div>
                        <div class="form-check mt-3">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">{{trans('trans.contact.agreeMsg',[],$locale)}}</label>
                        </div>
                        <div class="mt-3">
                            <button type="submit" class="submit">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_js')
    <script src="{{asset('frontend/js/page/contact.js')}} "></script>
@endsection
