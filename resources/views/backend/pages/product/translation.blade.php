@extends('backend.template.layout')
@section('content')
    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><img src="{{asset('backend/media/flags/'.$locale.'.png')}}" class="flag-icon"/>
                    Product Details <small>{{$product->code}} translation</small></h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="{{route('backend.product.index')}}" class="btn btn-clean kt-margin-r-10">
                    <i class="la la-arrow-left"></i>
                    <span class="kt-hidden-mobile">Back</span>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form class="kt-form form-item" action="{{route('backend.product.trans.save',[$product->_id,$locale])}}" method="post" id="kt-form">
                <div class="row">
                    <div class="col-xl-1"></div>
                    <div class="col-xl-10">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label text-right">{{trans('trans.field.name',[],$locale)}}</label>
                                    <div class="col-9">
                                        <input class="form-control" name="name" type="text" value="{{$tran->name?$tran->name:''}}">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                        <div class="kt-section kt-section">
                            <div class="kt-section__body">
                                <h3 class="kt-section__title kt-section__title-lg">{{trans('trans.section.overview',[],$locale)}}:</h3>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label ">{{trans('trans.field.quickDetails',[],$locale)}}</label>
                                    <div class="col-9" id="quickDetails">
                                        <div class="toolbar">
                                            <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add',[],$locale)}}</button>
                                            <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all',[],$locale)}}</button>
                                        </div>
                                        <div class="mt-3 items">
                                            @if($tran->quick_details)
                                                @foreach($tran->quick_details as $qkd)
                                                    <div class="row mb-3 item">
                                                        <label class="col-2 col-form-label text-right">Property</label>
                                                        <div class="col-sm-3"><input name="key" value="{{$qkd['key']}}" class="form-control"> </div>
                                                        <label class="col-2 col-form-label text-right">Value</label>
                                                        <div class="col-sm-3"><input name="value" value="{{$qkd['value']}}" class="form-control"> </div>
                                                        <div class="col-sm-2"><button type="button" class="btn btn-primary item-remove"><i class="fa fa-trash"></i> Delete</button> </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label ">{{trans('trans.field.supplyAbility',[],$locale)}}</label>
                                    <div class="col-9" id="supplyAbility">
                                        <div class="toolbar">
                                            <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add',[],$locale)}}</button>
                                            <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all',[],$locale)}}</button>
                                        </div>
                                        <div class="mt-3 items">
                                            @if($tran->supply_ability)
                                                @foreach($tran->supply_ability as $sab)
                                                    <div class="row mb-3 item">
                                                        <label class="col-2 col-form-label text-right">Property</label>
                                                        <div class="col-sm-3"><input name="key" value="{{$sab['key']}}" class="form-control"> </div>
                                                        <label class="col-2 col-form-label text-right">Value</label>
                                                        <div class="col-sm-3"><input name="value" value="{{$sab['value']}}" class="form-control"> </div>
                                                        <div class="col-sm-2"><button type="button" class="btn btn-primary item-remove"><i class="fa fa-trash"></i> Delete</button> </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label ">{{trans('trans.field.packagingDelivery',[],$locale)}}</label>
                                    <div class="col-9" id="packagingDelivery">
                                        <div class="toolbar">
                                            <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add',[],$locale)}}</button>
                                            <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all',[],$locale)}}</button>
                                        </div>
                                        <div class="mt-3 items">
                                            @if($tran->packaging_delivery)
                                                @foreach($tran->packaging_delivery as $pkd)
                                                    <div class="row mb-3 item">
                                                        <label class="col-2 col-form-label text-right">Property</label>
                                                        <div class="col-sm-3"><input name="key" value="{{$pkd['key']}}" class="form-control"> </div>
                                                        <label class="col-2 col-form-label text-right">Value</label>
                                                        <div class="col-sm-3"><input name="value" value="{{$pkd['value']}}" class="form-control"> </div>
                                                        <div class="col-sm-2"><button type="button" class="btn btn-primary item-remove"><i class="fa fa-trash"></i> Delete</button> </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                        <div class="kt-section kt-section--last">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-9 kt-form__actions">
                                    <button type="submit" class="btn btn-primary">{{trans('trans.save',[],$locale)}}</button>
                                    <a href="{{route('backend.product.index')}}" class="btn btn-secondary">{{trans('trans.cancel',[],$locale)}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1"></div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/vendors/ckeditor/adapters/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/components/multiFields/multiFields.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let action = $form.attr('action');
            let $inputName = $form.find('input[name=name]');
            let quickDetails = $form.find('#quickDetails').multiFields({type: 'keyValue'});
            let supplyAbility = $form.find('#supplyAbility').multiFields({type: 'keyValue'});
            let packagingDelivery = $form.find('#packagingDelivery').multiFields({type: 'keyValue'});
            let validator = $form.validate({
                rules: {
                    name: {required: true},
                }
            });
            $form.on('submit',function (e) {
                e.preventDefault();
                if($form.valid()){
                    let data = {
                        name:$inputName.val(),
                        quickDetails:quickDetails.getValues(),
                        supplyAbility:supplyAbility.getValues(),
                        packagingDelivery:packagingDelivery.getValues()
                    };
                    console.log(data);
                    axios.post(action,data)
                        .then(function(response){
                            const data = response.data;
                            swal.fire({
                                "title": '@lang('trans.message.system.say')',
                                "text": data.message,
                                "type": data.type,
                                onClose:()=>{
                                    location.href = '../'
                                }
                            });

                        })
                        .catch(function(error){
                            console.log(error)
                        });
                }
            })
        });
    </script>
@endsection
