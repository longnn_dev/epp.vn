@extends('backend.template.layout')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Product Category <small>new item</small></h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <a href="{{route('backend.product.index')}}" class="btn btn-clean kt-margin-r-10">
                <i class="la la-arrow-left"></i>
                <span class="kt-hidden-mobile">Back</span>
            </a>
        </div>
    </div>
    <div class="kt-portlet__body">
        <form class="kt-form form-item" action="{{route('backend.product.store')}}" method="post" id="kt-form">
            @csrf
            <div class="row">
                <div class="col-xl-1"></div>
                <div class="col-xl-10">
                    <div class="kt-section kt-section--first">
                        <div class="kt-section__body">
                            <h3 class="kt-section__title kt-section__title-lg">{{trans('trans.properties')}}:</h3>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.code')}}</label>
                                <div class="col-9">
                                    <input class="form-control" name="code" type="text" value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.category')}}</label>
                                <div class="col-9">
                                    <select name="catId" id="selectCat" class="form-control">
                                        <option value=""></option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->_id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">Colors *</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <div class="kt-checkbox-list">
                                        <label class="kt-checkbox kt-checkbox--stroke kt-checkbox--white">
                                            <input type="checkbox" name="color" value="#FFFFFF"> White
                                            <span></span>
                                        </label>
                                        <label class="kt-checkbox kt-checkbox--stroke kt-checkbox--black">
                                            <input type="checkbox" name="color" value="#000000"> Black
                                            <span></span>
                                        </label>
                                        <label class="kt-checkbox kt-checkbox--stroke kt-checkbox--wheat">
                                            <input type="checkbox" name="color" value="#F5DEB3"> Wheat
                                            <span></span>
                                        </label>
                                    </div>
                                    <span class="form-text text-muted"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.price')}}</label>
                                <div class="col-9" id="prices">
                                    <div class="toolbar">
                                        <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                        <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                    </div>
                                    <div class="mt-3 items">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.minOrder')}}</label>
                                <div class="col-3" id="prices">
                                    <input name="minOrder" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.leadTime')}}</label>
                                <div class="col-9" id="leadTimes">
                                    <div class="toolbar">
                                        <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                        <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                    </div>
                                    <div class="mt-3 items">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label ">{{trans('trans.field.size')}}</label>
                                <div class="col-9" id="size">
                                    <div class="toolbar">
                                        <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                        <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                    </div>
                                    <div class="mt-3 items">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">{{trans('trans.field.status')}}</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio">
                                            <input type="radio" name="status" value="1" checked> Active
                                            <span></span>
                                        </label>
                                        <label class="kt-radio">
                                            <input type="radio" name="status" value="0"> InActive
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                    <div class="kt-section kt-section--first">
                        <div class="kt-section__body">
                            <h3 class="kt-section__title kt-section__title-lg">{{trans('trans.medias')}}:</h3>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.avatar')}}</label>
                                <div class="col-9">
                                    <div class="avatar" id="avatar">
                                        <img class="image-preview shadow-sm img-fluid" src="{{asset('backend/media/files/image.jpg')}}" />
                                        <input name="avatar" type="hidden" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.photos')}}</label>
                                <div class="col-9 photos" id="photos">
                                    <div class="toolbar">
                                        <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                        <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                    </div>
                                    <div class="mt-3 row items">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.videos')}}</label>
                                <div class="col-9" id="videos">
                                    <div class="toolbar">
                                        <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                        <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                    </div>
                                    <div class="mt-3 items">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                    <div class="kt-section kt-section--last">
                        <div class="row">
                            <div class="col-3"></div>
                            <div class="col-9 kt-form__actions">
                                <button type="submit" class="btn btn-primary">{{trans('trans.save')}}</button>
                                <button type="reset" class="btn btn-secondary">{{trans('trans.cancel')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1"></div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />
    <style type="text/css" rel="stylesheet">
        .social-channel-input{margin-bottom: 2rem;}
        .kt-checkbox--stroke > span{
            border:1px solid #0d0d0d !important;
        }
        .kt-checkbox--black > span,.kt-checkbox--black > input:checked ~ span{
            background: #0d0d0d;
        }
        .kt-checkbox--black > span,.kt-checkbox--black > input:checked ~ span:after{
            border: solid #FFFFFF;
        }
        .kt-checkbox--wheat > span,.kt-checkbox--wheat > input:checked ~ span{
            background: #F5DEB3;
        }
        .kt-checkbox--wheat > span,.kt-checkbox--wheat > input:checked ~ span:after{
            border: solid #0d0d0d;
        }
        .red-border{
            border-color: #fd397a !important;
        }
    </style>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/components/multiFields/multiFields.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let action = $form.attr('action');
            let $inputCode = $form.find('input[name=code]');
            let $inputMinOrder = $form.find('input[name=minOrder]');
            let $selectCat = $form.find('select[name=catId]');
            let $chkColors = $form.find('input[name=color]');
            let $radStatus = $form.find('input[name=status]');
            let sizeFields = $form.find('#size').multiFields({type: 'simple',inputValueName:'size'});
            let priceFields = $form.find('#prices').multiFields({
                    type: 'keyValue',
                    keyText: 'Amount',
                    valueText: 'Price'

                });
            let leadTime = $form.find('#leadTimes').multiFields({
                    type: 'keyValue',
                    keyText: 'Amount',
                    valueText: 'Days'

                });
            let videoFields = $form.find('#videos').multiFields({type: 'simple',inputValueName: 'video'});
            let photoSelect = $form.find('#photos').multiImageSelect({inputField:'photo'});
            let avatarSelect = $form.find('#avatar').singleImageSelect();
            $selectCat.select2({placeholder: '{{trans('trans.field.select.null')}}'});
            let validator = $form.validate({
                rules: {
                    avatar: {required: true},
                    code: {required: true},
                    status:{required:true},
                    catId:{required:true},
                    color:{required:true,minlength: 1},
                    minOrder:{required:true,min:1}
                }
            });
            $form.on('submit',function(e){
                e.preventDefault();
                let data = {color:getColors()}
                if($form.valid()){
                    let data = {
                        code:$inputCode.val(),
                        catId:$selectCat.val(),
                        colors:getColors(),
                        prices:priceFields.getValues(),
                        minOrder:$inputMinOrder.val(),
                        leadTimes:leadTime.getValues(),
                        sizes:sizeFields.getValues(),
                        status:$radStatus.val(),
                        avatar:avatarSelect.getImageUrl(),
                        photos:photoSelect.getImageUrls(),
                        videos:videoFields.getValues()
                    };

                    console.log(data);
                    axios.post(action,data)
                        .then(function(response){
                            const data = response.data;
                            swal.fire({
                                "title": "@lang('trans.message.system.say')",
                                "text": data.message,
                                "type": data.type,
                                onClose:()=>{
                                    location.href = './'
                                }
                            });
                        })
                        .catch(function(error){
                            console.log(error)
                        });
                }
            })
            function getColors(){
                let colors = [];
                $form.find("input[name=color]:checked").each(function(){
                   colors.push($(this).val())
                });
                return colors;
            }
        });

    </script>

@endsection
