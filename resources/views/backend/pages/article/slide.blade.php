@extends('backend.template.layout')
@section('page_css')
    <link href="{{asset('backend/vendors/general/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/vendors/general/select2/dist/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('backend/js/pages/article/slide.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('page_script')
    <script
            src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
            integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
            crossorigin="anonymous"></script>
    <script src="{{asset('backend/vendors/general/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{asset('backend/js/pages/article/slide.js')}}" type="text/javascript"></script>
@endsection
@section('page_script')
@endsection
@section('content')
    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile mb-4" id="kt_page_portlet">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Article Slide <small>adjust order</small></h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="{{route('backend.article.index')}}" class="btn btn-clean kt-margin-r-10">
                    <i class="la la-arrow-left"></i>
                    <span class="kt-hidden-mobile">Back</span>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12">
                        <div class="portlet-body form">
                            <form action="#" class="horizontal-form" id="formSearch">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Article Category</label>
                                            <select class="form-control" name="category">
                                                <option value="">Select...</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Ngày cập nhật:</label>
                                        <div class="input-group date">
                                            <input type="text" name="updatedDate" class="form-control updatedDate m-input" readonly="" value="">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" name="search" class="btn btn-primary">
                                    <i class="fa fa-search"></i> Search
                                </button>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12">
                    <div class="kt-portlet--solid-light" id="articleSlideTool">
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            Bài viết khả dụng
                                        </div>
                                        <div class="card-body">
                                            <div class="article-list" id="availableList">
                                                @foreach($availableArticles as $article)
                                                    <div class="article-item-type-1" id="{{$article->_id}}">
                                                        <div class="article-title">{{$article->tran($locale)->name}}</div>
                                                        <div class="article-info">{{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y')}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            Đã chọn
                                        </div>
                                        <div class="card-body">
                                            <div class="article-list" id="selectedList">
                                                @foreach($selectedArticles as $article)
                                                    <div class="article-item-type-1" id="{{$article->_id}}">
                                                        <div class="article-title">{{$article->tran($locale)->name}}</div>
                                                        <div class="article-info">{{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y')}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
    </div>
@endsection
