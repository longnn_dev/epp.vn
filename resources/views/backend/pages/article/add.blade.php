@extends('backend.template.layout')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Article <small>new item</small></h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <a href="{{route('backend.article.index')}}" class="btn btn-clean kt-margin-r-10">
                <i class="la la-arrow-left"></i>
                <span class="kt-hidden-mobile">Back</span>
            </a>
        </div>
    </div>
    <div class="kt-portlet__body">
        <form class="kt-form form-item" action="{{route('backend.article.store')}}" method="post" id="kt-form">
            @csrf
            <div class="row">
                <div class="col-xl-1"></div>
                <div class="col-xl-10">
                    <div class="kt-section kt-section--first">
                        <div class="kt-section__body">
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.avatar')}}</label>
                                <div class="col-9">
                                    <div class="avatar" id="avatar">
                                        <img class="image-preview shadow-sm img-fluid" src="{{asset('backend/media/files/image.jpg')}}" />
                                        <input name="avatar" type="hidden" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.category')}}</label>
                                <div class="col-9">
                                    <select name="catId" class="form-control">
                                        @foreach($categories as $category)
                                            <option value="{{$category->_id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">{{trans('trans.field.type')}}</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio">
                                            <input type="radio" name="type" value="1" > Hot News
                                            <span></span>
                                        </label>
                                        <label class="kt-radio">
                                            <input type="radio" name="type" value="0" checked> Normal
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">{{trans('trans.field.status')}}</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio">
                                            <input type="radio" name="status" value="1" checked> Active
                                            <span></span>
                                        </label>
                                        <label class="kt-radio">
                                            <input type="radio" name="status" value="0"> InActive
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                    <div class="kt-section kt-section--last">
                        <div class="row">
                            <div class="col-3"></div>
                            <div class="col-9 kt-form__actions">
                                <button type="submit" class="btn btn-primary">{{trans('trans.save')}}</button>
                                <button type="reset" class="btn btn-secondary">{{trans('trans.cancel')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1"></div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let avatarSelect = $form.find('#avatar').singleImageSelect();
            let validator = $form.validate({
                rules: {
                    avatar: {required: true},
                    status:{required:true},
                },
                submitHandler: function (form) {
                    form.submit();
                },
                invalidHandler: function (event, validator) {
                    console.log("INVALID")
                }
            });
        });

    </script>

@endsection
