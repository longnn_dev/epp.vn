@extends('backend.template.layout')
@section('content')
    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><img src="{{asset('backend/media/flags/'.$locale.'.png')}}" class="flag-icon"/> {{$company->brand}} <small>details translation</small></h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="{{route('backend.company.index')}}" class="btn btn-clean kt-margin-r-10">
                    <i class="la la-arrow-left"></i>
                    <span class="kt-hidden-mobile">Back</span>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form class="kt-form form-item" action="{{route('backend.company.trans.save',[$company->_id,$locale])}}" method="post" id="kt-form">
                <div class="row">
                    <div class="col-xl-1"></div>
                    <div class="col-xl-10">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.full_name')}}</label>
                                    <div class="col-9">
                                        <input class="form-control" name="name" type="text" value="{{$tran->name?$tran->name:''}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.address')}}</label>
                                    <div class="col-9">
                                        <input class="form-control" name="address" type="text" value="{{$tran->address?$tran->address:''}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.alt_address')}}</label>
                                    <div class="col-9">
                                        <input class="form-control" name="alt_address" type="text" value="{{$tran->alt_address?$tran->alt_address:''}}">
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <label class="col-3 col-form-label">{{trans('trans.social_channel')}}</label>
                                    <div class="col-9">
                                        <div class="input-group social-channel-input mb-3">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-facebook"></i></span></div>
                                            <input name="facebook" type="text" class="form-control" placeholder="facebook" value="{{$tran->facebook?$tran->facebook:''}}">
                                        </div>
                                        <div class="input-group social-channel-input mb-3">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-twitter"></i></span></div>
                                            <input name="twitter" type="text" class="form-control" placeholder="twitter" value="{{$tran->twitter?$tran->twitter:''}}">
                                        </div>
                                        <div class="input-group social-channel-input mb-3">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-instagram"></i></span></div>
                                            <input name="instagram" type="text" class="form-control" placeholder="instagram" value="{{$tran->instagram?$tran->instagram:''}}">
                                        </div>
                                        <div class="input-group social-channel-input mb-3">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-youtube"></i></span></div>
                                            <input name="youtube" type="text" class="form-control" placeholder="youtube" value="{{$tran->youtube?$tran->youtube:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                        <div class="kt-section ">
                            <div class="kt-section__body">
                                <h3 class="kt-section__title kt-section__title-lg">{{trans('trans.properties')}}:</h3>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.about')}}</label>
                                    <div class="col-9">
                                        <textarea name="about" class="text-editor">{{$tran->about?$tran->about:''}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.properties')}}</label>
                                    <div class="col-9" id="properties">
                                        <div class="toolbar">
                                            <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                            <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                        </div>
                                        <div class="items mt-3 ">
                                            @if($tran->properties)
                                                @foreach($tran->properties as $property)
                                                    <div class="row mb-3 item">
                                                        <label class="col-2 col-form-label text-right">Property</label>
                                                        <div class="col-sm-3"><input name="key" value="{{$property['key']}}" class="form-control" /> </div>
                                                        <label class="col-2 col-form-label text-right">Value</label>
                                                        <div class="col-sm-3"><input name="value" value="{{$property['value']}}" class="form-control" /> </div>
                                                        <div class="col-sm-2"><button type="button" class="btn btn-primary item-remove"><i class="fa fa-trash"></i> Delete</button> </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                        <div class="kt-section">
                            <div class="kt-section__body">
                                <h3 class="kt-section__title kt-section__title-lg">{{trans('trans.main_info')}}:</h3>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.avatar')}}</label>
                                    <div class="col-9">
                                        <div class="cover" id="avatar">
                                            <img class="image-preview shadow-sm img-fluid" src="{{$tran->avatar?$tran->avatar:asset('backend/media/files/image.jpg')}}" />
                                            <input name="avatar" value="{{$tran->avatar?$tran->avatar:''}}" type="hidden" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.bom')}}</label>
                                    <div class="col-9">
                                        <div class="cover" id="bom">
                                            <img class="image-preview shadow-sm img-fluid" src="{{$tran->bom?$tran->bom:asset('backend/media/files/image.jpg')}}" />
                                            <input name="bom" value="{{$tran->bom?$tran->bom:''}}" type="hidden" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.photos')}}</label>
                                    <div class="col-9 photos" id="photos">
                                        <div class="toolbar">
                                            <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                            <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                        </div>
                                        <div class="mt-3 row items">
                                            @if($tran->photos)
                                                @foreach($tran->photos as $photo)
                                                    <div class="col-md-4 mb-3 item">
                                                        <button type="button" class="item-remove"><i class="la la-trash"></i> </button>
                                                        <img class="shadow-sm img-fluid" src="{{$photo}}" />
                                                        <input type="hidden" value="{{$photo}}" />
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.videos')}}</label>
                                    <div class="col-9" id="videos">
                                        <div class="toolbar">
                                            <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                            <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                        </div>
                                        <div class="mt-3 items">
                                            @if($tran->videos)
                                                @foreach($tran->videos as $video)
                                                    <div class="input-group mb-3 item">
                                                        <input type="text" name="value" value="{{$video}}" class="form-control" placeholder="">
                                                        <div class="input-group-append"><button type="button" class="input-group-text item-remove" id="basic-addon2"><i class="la la-trash"></i></button></div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                        <div class="kt-section kt-section--last">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-9 kt-form__actions">
                                    <button type="submit" class="btn btn-primary">{{trans('trans.save')}}</button>
                                    <a href="{{route('backend.company.index')}}" class="btn btn-secondary">{{trans('trans.cancel')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1"></div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />
    <style type="text/css" rel="stylesheet">
        .social-channel-input{margin-bottom: 2rem;}
    </style>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/vendors/ckeditor/adapters/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/components/multiFields/multiFields.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let action = $form.attr('action');
            let $inputName = $form.find('input[name=name]');
            let $inputAddress = $form.find('input[name=address]');
            let $inputAddress2 = $form.find('input[name=alt_address]');
            let $inputFacebook = $form.find('input[name=facebook]');
            let $inputTwitter = $form.find('input[name=facebook]');
            let $inputInstagram = $form.find('input[name=instagram]');
            let $inputYoutube = $form.find('input[name=youtube]');
            let $editorAbout = $('textarea[name=about]').ckeditor(function () {
                CKFinder.setupCKEditor(this, '/ckfinder/');
            });
            let avatarSelect = $form.find('#avatar').singleImageSelect();
            let bomSelect = $form.find('#bom').singleImageSelect();
            let photoSelect = $form.find('#photos').multiImageSelect();
            let videoFields = $form.find('#videos').multiFields({type: 'simple'});
            let propertyFields = $form.find('#properties').multiFields({type: 'keyValue'});
            let validator = $form.validate({
                rules: {
                    name: {required: true},
                    address: {required: true},
                    phoneNumber: {required: true},
                    email: {required: true, email: true},
                    about: {required: true},
                    avatar:{required:true}
                },
                submitHandler: function (form) {

                },
                invalidHandler: function (event, validator) {
                    console.log("INVALID")
                }
            });
            $form.on('submit',function (e) {
                e.preventDefault();
                if($form.valid()){
                    let data = {
                        name:$inputName.val(),
                        address:$inputAddress.val(),
                        alt_address:$inputAddress2.val(),
                        facebook:$inputFacebook.val(),
                        twitter:$inputTwitter.val(),
                        instagram:$inputInstagram.val(),
                        youtube:$inputYoutube.val(),
                        about:$editorAbout.val(),
                        avatar:avatarSelect.getImageUrl(),
                        bom:bomSelect.getImageUrl(),
                        photos:photoSelect.getImageUrls(),
                        properties:propertyFields.getValues(),
                        videos:videoFields.getValues()
                    };
                    console.log(data);
                    axios.post(action,data)
                        .then(function(response){
                            const data = response.data;
                            swal.fire({
                                "title": '@lang('trans.message.system.say')',
                                "text": data.message,
                                "type": data.type,
                                onClose:()=>{
                                    location.href = '../'
                                }
                            });
                        })
                        .catch(function(error){
                            console.log(error)
                        });
                }
            })
        });
    </script>
@endsection
