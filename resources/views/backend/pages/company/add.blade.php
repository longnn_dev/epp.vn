@extends('backend.template.layout')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Company Profiles <small>complete information</small></h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <a href="{{route('backend.company.index')}}" class="btn btn-clean kt-margin-r-10">
                <i class="la la-arrow-left"></i>
                <span class="kt-hidden-mobile">Back</span>
            </a>
        </div>
    </div>
    <div class="kt-portlet__body">
        <form class="kt-form form-item" action="{{route('backend.company.store')}}" method="post" id="kt-form">
            @csrf
            <div class="row">
                <div class="col-xl-1"></div>
                <div class="col-xl-10">
                    <div class="kt-section kt-section--first">
                        <div class="kt-section__body">
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.logo')}}</label>
                                <div class="col-9">
                                    <div class="logo" id="logo">
                                        <img class="image-preview shadow-sm img-fluid" src="{{asset('backend/media/files/image.jpg')}}" />
                                        <input name="logo" type="hidden" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.brand')}}</label>
                                <div class="col-9">
                                    <input class="form-control" name="brand" type="text" value="" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.email')}}</label>
                                <div class="col-9" id="emails">
                                    <div class="toolbar">
                                        <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                        <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                    </div>
                                    <div class="mt-3 items">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.phone_number')}}</label>
                                <div class="col-9" id="phone_numbers">
                                    <div class="toolbar">
                                        <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                                        <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                                    </div>
                                    <div class="mt-3 items">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">{{trans('trans.field.type')}}</label>
                                <div class="col-9">
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio">
                                            <input type="radio" name="type" value="main" {{$type=='main'?'checked':''}}> Main
                                            <span></span>
                                        </label>
                                        <label class="kt-radio">
                                            <input type="radio" name="type" value="partner" {{$type=='partner'?'checked':''}}> Partner
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3 col-sm-12">{{trans('trans.field.status')}}</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio">
                                            <input type="radio" name="status" value="1" checked> Active
                                            <span></span>
                                        </label>
                                        <label class="kt-radio">
                                            <input type="radio" name="status" value="0"> InActive
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                    <div class="kt-section kt-section--last">
                        <div class="row">
                            <div class="col-3"></div>
                            <div class="col-9 kt-form__actions">
                                <button type="submit" class="btn btn-primary">{{trans('trans.save')}}</button>
                                <button type="reset" class="btn btn-secondary">{{trans('trans.cancel')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1"></div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/components/multiFields/multiFields.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let action = $form.attr('action');
            //
            let $btnSubmit = $form.find('button[type=submit]');
            let $btnCancel = $form.find('button[type=reset]');
            let $radioType = $form.find('input[name=type]');
            let $radioStatus = $form.find('input[name=status]');
            let $inputBrand = $form.find('input[name=brand]');
            let logoSelect = $form.find('#logo').singleImageSelect();
            let emails = $form.find('#emails').multiFields({type:'simple',inputValueName:'email[]'});
            let phones = $form.find('#phone_numbers').multiFields({type:'simple',inputValueName:'phone_number[]'});
            let validator = $form.validate({
                rules: {
                    logo: {required: true},
                    brand: {required: true},
                    type:{required:true},
                    status:{required:true},
                },
                submitHandler: function (form) {
                    form.submit();
                },
                invalidHandler: function (event, validator) {
                    console.log("INVALID")
                }
            });
        });

    </script>

@endsection
