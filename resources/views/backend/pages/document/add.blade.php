@extends('backend.template.layout')
@section('content')
    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Product Category <small>new item</small></h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="{{route('backend.product.index')}}" class="btn btn-clean kt-margin-r-10">
                    <i class="la la-arrow-left"></i>
                    <span class="kt-hidden-mobile">Back</span>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form class="kt-form form-item" action="{{route('backend.document.store')}}" method="post" id="kt-form">
                <div class="row">
                    <div class="col-xl-1"></div>
                    <div class="col-xl-10">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.avatar')}}</label>
                                    <div class="col-9">
                                        <div class="avatar" id="avatar">
                                            <img class="image-preview shadow-sm img-fluid" src="{{asset('backend/media/files/image.jpg')}}" />
                                            <input name="avatar" type="hidden" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.name')}}</label>
                                    <div class="col-9">
                                        <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.file')}}</label>
                                    <div class="col-9">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="file_url" placeholder="Chọn file..." readonly>
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button" name="btn_file">Chọn!</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.content')}}</label>
                                    <div class="col-9">
                                        <textarea rows="5" class="form-control" name="content"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.locale')}}</label>
                                    <div class="col-9">
                                        <select name="locale" class="form-control w-50">
                                            <option value="en">English</option>
                                            <option value="vi">Tiếng Việt</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">{{trans('trans.field.status')}}</label>
                                    <div class="col-lg-9 col-md-9 col-sm-12">
                                        <div class="kt-radio-inline">
                                            <label class="kt-radio">
                                                <input type="radio" name="status" value="1" checked> Active
                                                <span></span>
                                            </label>
                                            <label class="kt-radio">
                                                <input type="radio" name="status" value="0"> InActive
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                        <div class="kt-section kt-section--last">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-9 kt-form__actions">
                                    <button type="submit" class="btn btn-primary">{{trans('trans.save')}}</button>
                                    <button type="reset" class="btn btn-secondary">{{trans('trans.cancel')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1"></div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />

@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/components/multiFields/multiFields.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let action = $form.attr('action');
            let $radStatus = $form.find('input[name=status]');
            let $name = $form.find('input[name=name]');
            let $file = $form.find('input[name=file_url]');
            let $content = $form.find('textarea[name=content]');
            let $locale = $form.find('select[name=locale]');
            let avatarSelect = $form.find('#avatar').singleImageSelect();
            $form.find("button[name=btn_file]").on("click",function (e) {
                e.preventDefault();
                selectFile();
            })
            let validator = $form.validate({
                rules: {
                    avatar: {required: true},
                    name: {required: true},
                    status:{required:true},
                    file_url:{required:true}
                }
            });
            $form.on('submit',function(e){
                e.preventDefault();
                let data = {}
                if($form.valid()){
                    let data = {
                        cover_url:avatarSelect.getImageUrl(),
                        name:$name.val(),
                        locale:$locale.val(),
                        file_url:$file.val(),
                        content:$content.val(),
                        status:$radStatus.val(),
                    };
                    console.log(data);
                    axios.post(action,data)
                        .then(function(response){
                            const data = response.data;
                            swal.fire({
                                "title": "@lang('trans.message.system.say')",
                                "text": data.message,
                                "type": data.type,
                                onClose:()=>{
                                    location.href = './'
                                }
                            });
                        })
                        .catch(function(error){
                            console.log(error)
                        });
                }
            })
            function selectFile(){
                CKFinder.modal( {
                    chooseFiles: true,
                    width: 800,
                    height: 500,
                    onInit: function( finder ) {
                        finder.on( 'files:choose', function( evt ) {
                            var file = evt.data.files.first();
                            $("input[name=file_url]").val(file.getUrl());
                        } );
                    }
                } );
            }
        });

    </script>

@endsection

