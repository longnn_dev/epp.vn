@extends('backend.template.layout')
@section('content')
    <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><img src="{{asset('backend/media/flags/'.$locale.'.png')}}" class="flag-icon"/> {{$article->code}} <small>details translation</small></h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="{{route('backend.article.index')}}" class="btn btn-clean kt-margin-r-10">
                    <i class="la la-arrow-left"></i>
                    <span class="kt-hidden-mobile">Back</span>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form class="kt-form form-item" action="{{route('backend.article.trans.save',[$article->_id,$locale])}}" method="post" id="kt-form">
                <div class="row">
                    <div class="col-xl-1"></div>
                    <div class="col-xl-10">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.title')}}</label>
                                    <div class="col-9">
                                        <input class="form-control" name="name" type="text" value="{{$tran->name?$tran->name:''}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.sapo')}}</label>
                                    <div class="col-9">
                                        <textarea class="form-control" name="sapo" type="text" value="">{{!! $tran->sapo?$tran->sapo:''}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">{{trans('trans.field.content')}}</label>
                                    <div class="col-9">
                                        <textarea class="form-control" name="content" type="text" value="">{{!! $tran->content?$tran->content:''}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                        <div class="kt-section kt-section--last">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-9 kt-form__actions">
                                    <button type="submit" class="btn btn-primary">{{trans('trans.save')}}</button>
                                    <a href="{{route('backend.article.index')}}" class="btn btn-secondary">{{trans('trans.cancel')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1"></div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />
    <style type="text/css" rel="stylesheet">
        .social-channel-input{margin-bottom: 2rem;}
    </style>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/vendors/ckeditor/adapters/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/components/multiFields/multiFields.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let action = $form.attr('action');
            let $inputName = $form.find('input[name=name]');
            let $inputSapo = $form.find('textarea[name=sapo]');
            var $editor = $form.find('textarea[name=content]');
            $editor.ckeditor(function(){
                CKFinder.setupCKEditor( this, '/ckfinder/' );
            });
            let validator = $form.validate({
                rules: {
                    name: {required: true},
                },
                submitHandler: function (form) {

                },
                invalidHandler: function (event, validator) {
                    console.log("INVALID")
                }
            });
            $form.on('submit',function (e) {
                e.preventDefault();
                if($form.valid()){
                    let data = {
                        name:$inputName.val(),
                        sapo:$inputSapo.val(),
                        content:$editor.val()
                    };
                    console.log(data);
                    axios.post(action,data)
                        .then(function(response){
                            const data = response.data;
                            swal.fire({
                                "title": '@lang('trans.message.system.say')',
                                "text": data.message,
                                "type": data.type,
                                onClose:()=>{
                                    location.href = '../'
                                }
                            });
                        })
                        .catch(function(error){
                            console.log(error)
                        });
                }
            })
        });
    </script>
@endsection
