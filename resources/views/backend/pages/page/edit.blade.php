@extends('backend.template.layout')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-list"></i>
                        </span>
                    <h3 class="kt-portlet__head-title">
                        Edit SEO Page
                        <small></small>
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="la la-download"></i> Export
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__section kt-nav__section--first">
                                            <span class="kt-nav__section-text">Choose an option</span>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-print"></i>
                                                <span class="kt-nav__link-text">Print</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-copy"></i>
                                                <span class="kt-nav__link-text">Copy</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                <span class="kt-nav__link-text">Excel</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                <span class="kt-nav__link-text">CSV</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                <span class="kt-nav__link-text">PDF</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <a href="{{route('backend.system.page.create')}}" class="btn btn-brand btn-elevate btn-icon-sm"><i class="la la-plus"></i>New Record</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <form class="kt-form form-item" action="{{route('backend.system.page.update',$page->_id)}}" method="post" id="kt-form">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">{{trans('trans.field.title')}}</label>
                                        <div class="col-9">
                                            <input type="text" name="meta_title" value="{{$page->meta_title}}" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">{{trans('trans.field.keywords')}}</label>
                                        <div class="col-9">
                                            <input type="text" name="meta_keywords" value="{{$page->meta_keywords}}" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">{{trans('trans.field.description')}}</label>
                                        <div class="col-9">
                                            <textarea type="text" rows="7" name="meta_description" class="form-control m-input" placeholder="max 300 characters">{{$page->meta_description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">{{trans('trans.field.avatar')}}</label>
                                        <div class="col-9">
                                            <div class="avatar" id="avatar">
                                                <img class="image-preview shadow-sm img-fluid" src="{{$page->meta_image?$page->meta_image:asset('backend/media/files/image.jpg')}}" />
                                                <input name="meta_image" value="{{$page->meta_image}}" type="hidden" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">{{trans('trans.field.locale')}}</label>
                                        <div class="col-9">
                                            <select name="locale" class="form-control">
                                                <option value="">Select...</option>
                                                <option value="en" {{$page->locale=='en'?'selected':''}}>English</option>
                                                <option value="vi" {{$page->locale=='vi'?'selected':''}}>Vietnamese</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">{{trans('trans.field.pageType')}}</label>
                                        <div class="col-9">
                                            <select name="route" class="form-control">
                                                <option value="">Chọn...</option>
                                                @foreach($page_types as $key=>$value)
                                                    <option value="{{$key}}" {{$page->route==$key?'selected':''}}>{{$value}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('route'))
                                                <div class="form-control-feedback"> {{ $errors->first('route') }} </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                            <div class="kt-section kt-section--last">
                                <div class="row">
                                    <div class="col-3"></div>
                                    <div class="col-9 kt-form__actions">
                                        <button type="submit" class="btn btn-primary">{{trans('trans.save')}}</button>
                                        <button type="reset" class="btn btn-secondary">{{trans('trans.cancel')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('page_script')
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            const $form = $("#kt-form");
            let avatarSelect = $form.find('#avatar').singleImageSelect();
            let validator = $form.validate({
                rules: {
                    meta_image: {required: true},
                    meta_title:{required:true},
                    meta_keywords:{required:true},
                    meta_description:{required:true},
                    locale:{required:true},
                    route:{required:true},
                },
                submitHandler: function (form) {
                    form.submit();
                },
                invalidHandler: function (event, validator) {
                    console.log("INVALID")
                }
            });
        });

    </script>

@endsection

