@extends('backend.template.layout')
@section('page_script')
    <script
        src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
        integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{asset('backend/vendors/ckfinder/ckfinder.js')}}"></script>
    <script>CKFinder.config( {connectorPath: '{{url('/ckfinder/connector')}}'});</script>
    <script type="text/javascript" src="{{asset('backend/js/components/imageSelect/imageSelect.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/pages/slideshow/list.js')}}"></script>
@endsection
@section('page_css')
    <link href="{{asset('backend/js/components/imageSelect/imageSelect.css')}}" type="text/css" rel="stylesheet" />
@endsection
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="slides">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-list"></i>
                        </span>
                    <h3 class="kt-portlet__head-title">
                        Slide show Settings
                        <small></small>
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions toolbar">
                            <button type="button" data-control="add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> {{trans('trans.add')}}</button>
                            <button type="button" data-control="clear" class="btn btn-primary btn-sm"><i class="fa fa-recycle"></i> {{trans('trans.remove_all')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-section kt-section--first">
                    <div class="row">
                        <div class="slides items" id="sortable">
                            @foreach($slides as $slide)
                            <div class="col-md-4 mb-3 item" id="{{$slide->url}}">
                                <button type="button" class="item-remove"><i class="la la-trash"></i> </button>
                                <img class="shadow-sm img-fluid" src="{{$slide->url}}">
                                <input type="hidden" name="photo" value="{{$slide->url}}">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg"></div>
                <div class="kt-section kt-section--last">
                    <div class="row">
                        <div class="col-3"></div>
                        <div class="col-9 kt-form__actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
