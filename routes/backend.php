<?php

Route::domain('admin.epp.vn')->group(function () {
    Route::get('/login', 'Backend\AuthController@index')->name('backend.auth.index');
    Route::post('login', 'Backend\AuthController@login')->name('backend.auth.login');
    Route::group(['middleware' => 'isAdmin'], function () {
        Route::get('/', 'Backend\DashboardController@index')->name('backend.dashboard');
        Route::get('logout', 'Backend\AuthController@logout')->name('backend.auth.logout');
        // COMPANY ROUTE
        Route::get('company', 'Backend\CompanyController@index')->name('backend.company.index');
        Route::post('company/datatable', 'Backend\CompanyController@datatable')->name('backend.company.datatable');
        Route::get('company/add', 'Backend\CompanyController@add')->name('backend.company.add');
        Route::get('company/edit/{company}', 'Backend\CompanyController@edit')->name('backend.company.edit');
        Route::post('company/store', 'Backend\CompanyController@store')->name('backend.company.store');
        Route::put('company/update/{company}', 'Backend\CompanyController@update')->name('backend.company.update');
        Route::post('company/{company}/updateStatus', 'Backend\CompanyController@changeStatus')->name('backend.company.changeStatus');
        Route::delete('company/{company}', 'Backend\CompanyController@delete')->name('backend.company.delete');
        Route::get('company/{company}/trans/{locale}', 'Backend\CompanyController@trans')->name('backend.company.trans');
        Route::post('company/{company}/trans/{locale}', 'Backend\CompanyController@saveTrans')->name('backend.company.trans.save');

        // PRODUCT CATEGORY
        Route::get('productCategory', 'Backend\ProductCategoryController@index')->name('backend.productCategory.index');
        Route::post('productCategory/datatable', 'Backend\ProductCategoryController@datatable')->name('backend.productCategory.datatable');
        Route::get('productCategory/add', 'Backend\ProductCategoryController@add')->name('backend.productCategory.add');
        Route::get('productCategory/edit/{productCategory}', 'Backend\ProductCategoryController@edit')->name('backend.productCategory.edit');
        Route::post('productCategory/store', 'Backend\ProductCategoryController@store')->name('backend.productCategory.store');
        Route::put('productCategory/update/{productCategory}', 'Backend\ProductCategoryController@update')->name('backend.productCategory.update');
        Route::post('productCategory/{productCategory}/updateStatus', 'Backend\ProductCategoryController@changeStatus')->name('backend.productCategory.changeStatus');
        Route::delete('productCategory/{productCategory}', 'Backend\ProductCategoryController@delete')->name('backend.productCategory.delete');
        Route::get('productCategory/{productCategory}/trans/{locale}', 'Backend\ProductCategoryController@trans')->name('backend.productCategory.trans');
        Route::post('productCategory/{productCategory}/trans/{locale}', 'Backend\ProductCategoryController@saveTrans')->name('backend.productCategory.trans.save');
        Route::post('productCategory/{productCategory}/setHot','Backend\ProductCategoryController@setHot')->name('backend.productCategory.setHot');

        // PRODUCT
        Route::get('product', 'Backend\ProductController@index')->name('backend.product.index');
        Route::post('product/datatable', 'Backend\ProductController@datatable')->name('backend.product.datatable');
        Route::get('product/add', 'Backend\ProductController@add')->name('backend.product.add');
        Route::get('product/edit/{product}', 'Backend\ProductController@edit')->name('backend.product.edit');
        Route::post('product/store', 'Backend\ProductController@store')->name('backend.product.store');
        Route::put('product/update/{product}', 'Backend\ProductController@update')->name('backend.product.update');
        Route::post('product/{product}/updateStatus', 'Backend\ProductController@changeStatus')->name('backend.product.changeStatus');
        Route::delete('product/{product}', 'Backend\ProductController@delete')->name('backend.product.delete');
        Route::get('product/{product}/trans/{locale}', 'Backend\ProductController@trans')->name('backend.product.trans');
        Route::post('product/{product}/trans/{locale}', 'Backend\ProductController@saveTrans')->name('backend.product.trans.save');
        Route::post('product/{product}/setHot','Backend\ProductController@setHot')->name('backend.product.setHot');

        // ARTICLE CATEGORY
        Route::get('articleCategory', 'Backend\ArticleCategoryController@index')->name('backend.articleCategory.index');
        Route::post('articleCategory/datatable', 'Backend\ArticleCategoryController@datatable')->name('backend.articleCategory.datatable');
        Route::get('articleCategory/add', 'Backend\ArticleCategoryController@add')->name('backend.articleCategory.add');
        Route::get('articleCategory/edit/{articleCategory}', 'Backend\ArticleCategoryController@edit')->name('backend.articleCategory.edit');
        Route::post('articleCategory/store', 'Backend\ArticleCategoryController@store')->name('backend.articleCategory.store');
        Route::put('articleCategory/update/{articleCategory}', 'Backend\ArticleCategoryController@update')->name('backend.articleCategory.update');
        Route::post('articleCategory/{articleCategory}/updateStatus', 'Backend\ArticleCategoryController@changeStatus')->name('backend.articleCategory.changeStatus');
        Route::delete('articleCategory/{articleCategory}', 'Backend\ArticleCategoryController@delete')->name('backend.articleCategory.delete');
        Route::get('articleCategory/{articleCategory}/trans/{locale}', 'Backend\ArticleCategoryController@trans')->name('backend.articleCategory.trans');
        Route::post('articleCategory/{articleCategory}/trans/{locale}', 'Backend\ArticleCategoryController@saveTrans')->name('backend.articleCategory.trans.save');
        Route::post('/articleCategory/{articleCategory}/setHot','Backend\ArticleCategoryController@setHot')->name('backend.articleCategory.setHot');
        // ARTICLE
        Route::get('article', 'Backend\ArticleController@index')->name('backend.article.index');
        Route::post('article/datatable', 'Backend\ArticleController@datatable')->name('backend.article.datatable');
        Route::get('article/add', 'Backend\ArticleController@add')->name('backend.article.add');
        Route::get('article/edit/{article}', 'Backend\ArticleController@edit')->name('backend.article.edit');
        Route::post('article/store', 'Backend\ArticleController@store')->name('backend.article.store');
        Route::put('article/update/{article}', 'Backend\ArticleController@update')->name('backend.article.update');
        Route::post('article/{article}/updateStatus', 'Backend\ArticleController@changeStatus')->name('backend.article.changeStatus');
        Route::delete('article/{article}', 'Backend\ArticleController@delete')->name('backend.article.delete');
        Route::get('article/{article}/trans/{locale}', 'Backend\ArticleController@trans')->name('backend.article.trans');
        Route::post('article/{article}/trans/{locale}', 'Backend\ArticleController@saveTrans')->name('backend.article.trans.save');
        Route::post('article/{article}/setHot','Backend\ArticleController@setHot')->name('backend.article.setHot');
        Route::get('article/articleSlide','Backend\ArticleController@articleSlide')->name('backend.article.slide');
        // CUSTOMER
        Route::get('customer', 'Backend\CustomerController@index')->name('backend.customer.index');
        Route::post('customer/datatable', 'Backend\CustomerController@datatable')->name('backend.customer.datatable');
        Route::get('customer/add', 'Backend\CustomerController@add')->name('backend.customer.add');
        Route::get('customer/edit/{customer}', 'Backend\CustomerController@edit')->name('backend.customer.edit');
        Route::post('customer/store', 'Backend\CustomerController@store')->name('backend.customer.store');
        Route::put('customer/update/{customer}', 'Backend\CustomerController@update')->name('backend.customer.update');
        Route::post('customer/{customer}/updateStatus', 'Backend\CustomerController@changeStatus')->name('backend.customer.changeStatus');
        Route::delete('customer/{customer}', 'Backend\CustomerController@delete')->name('backend.customer.delete');
        Route::get('customer/{customer}/trans/{locale}', 'Backend\CustomerController@trans')->name('backend.customer.trans');
        Route::post('customer/{customer}/trans/{locale}', 'Backend\CustomerController@saveTrans')->name('backend.customer.trans.save');

        // ORDER
        Route::get('order','Backend\OrderController@index')->name('backend.order.index');
        Route::post('order/datatable','Backend\OrderController@datatable')->name('backend.order.datatable');
        Route::post('order/view','Backend\OrderController@view')->name('backend.order.view');
        Route::get('order/add','Backend\OrderController@add')->name('backend.order.add');
        Route::get('order/edit/{order}','Backend\OrderController@edit')->name('backend.order.edit');
        Route::post('order/store','Backend\OrderController@store')->name('backend.order.store');
        Route::put('order/update/{order}','Backend\OrderController@update')->name('backend.order.update');
        Route::post('order/{order}/updateStatus','Backend\OrderController@changeStatus')->name('backend.order.changeStatus');
        Route::delete('order/{order}','Backend\OrderController@delete')->name('backend.order.delete');

        // USER
        Route::get('user','Backend\UserController@index')->name('backend.user.index');
        Route::post('user/datatable', 'Backend\UserController@datatable')->name('backend.user.datatable');
        Route::get('customer/add', 'Backend\CustomerController@add')->name('backend.customer.add');
        Route::get('customer/edit/{customer}', 'Backend\CustomerController@edit')->name('backend.customer.edit');
        Route::post('customer/store', 'Backend\CustomerController@store')->name('backend.customer.store');
        Route::put('customer/update/{customer}', 'Backend\CustomerController@update')->name('backend.customer.update');
        Route::post('customer/{customer}/updateStatus', 'Backend\CustomerController@changeStatus')->name('backend.customer.changeStatus');
        Route::delete('customer/{customer}', 'Backend\CustomerController@delete')->name('backend.customer.delete');

        // PAGE
        Route::post('/page/datatables','Backend\PageController@datatables')->name('backend.system.page.datatables');
        Route::post('/page','Backend\PageController@store')->name('backend.system.page.store');
        Route::get('/page','Backend\PageController@index')->name('backend.system.page.index');
        Route::get('/page/create','Backend\PageController@create')->name('backend.system.page.create');
        Route::post('/page/updateStatus','Backend\PageController@updateStatus')->name('backend.system.page.updateStatus');
        Route::get('/page/{page}','Backend\PageController@show')->name('backend.system.page.show');
        Route::put('/page/{page}','Backend\PageController@update')->name('backend.system.page.update');
        Route::delete('/page/{page}','Backend\PageController@destroy')->name('backend.system.page.destroy');
        Route::get('/page/{page}/edit','Backend\PageController@edit')->name('backend.system.page.edit');

        // FAQ
        Route::post('/faq/datatables','Backend\FaqController@datatables')->name('backend.faq.datatables');
        Route::post('/faq','Backend\FaqController@store')->name('backend.faq.store');
        Route::get('/faq','Backend\FaqController@index')->name('backend.faq.index');
        Route::get('/faq/create','Backend\FaqController@create')->name('backend.faq.create');
        Route::post('/faq/updateStatus','Backend\FaqController@updateStatus')->name('backend.faq.updateStatus');
        Route::get('/faq/{faq}','Backend\FaqController@show')->name('backend.faq.show');
        Route::put('/faq/{faq}','Backend\FaqController@update')->name('backend.faq.update');
        Route::delete('/faq/{faq}','Backend\FaqController@destroy')->name('backend.faq.destroy');
        Route::get('/faq/{faq}/edit','Backend\FaqController@edit')->name('backend.faq.edit');

        // DOCUMENT
        Route::post('/document/datatables','Backend\DocumentController@datatables')->name('backend.document.datatables');
        Route::get('/document','Backend\DocumentController@index')->name('backend.document.index');
        Route::get('/document/create','Backend\DocumentController@create')->name('backend.document.create');
        Route::get('/document/{document}/edit','Backend\DocumentController@edit')->name('backend.document.edit');
        Route::post('/document','Backend\DocumentController@store')->name('backend.document.store');
        Route::put('/document/{document}','Backend\DocumentController@update')->name('backend.document.update');
        Route::delete('/document/{document}','Backend\DocumentController@destroy')->name('backend.document.destroy');
        Route::get('/document/{document}/edit','Backend\DocumentController@edit')->name('backend.document.edit');

        // SLIDE SHOW
        Route::get('/slideshow','Backend\SlideshowController@index')->name('backend.slideshow.index');
        Route::post('/slideshow/save','Backend\SlideshowController@save')->name('backend.slideshow.save');
    });
});
