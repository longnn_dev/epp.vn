<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::domain('epp.vn')->group(function () {
    Route::get('/', 'Beta\HomeController@index')->name('frontend.home');
    Route::get('/lang/{locale}', 'Beta\HomeController@seLocale')->name('frontend.home.seLocale');
    Route::get('/product-category/{productCategory}/{title?}', 'Beta\ProductCategoryController@details')->name('frontend.productCategory');
    Route::get('/product/{product}/{title?}', 'Beta\ProductController@details')->name('frontend.product.details');
    Route::get('/services', 'Beta\ServiceController@index')->name('frontend.services');
    Route::get('/news', 'Beta\NewsController@index')->name('frontend.news');
    Route::get('/news/{news}/{title?}', 'Beta\NewsController@detail')->name('frontend.news.detail.seo');
    Route::get('/contact', 'Beta\ContactController@index')->name('frontend.contact');
    Route::get('/about', 'Beta\AboutController@index')->name('frontend.about');
    Route::get('/partners', 'Beta\PartnerController@index')->name('frontend.partners');
    Route::get('/partner/{partner}/{title?}', 'Beta\PartnerController@detail')->name('frontend.partner.detail');
    Route::get('/documents', 'Beta\DocumentController@index')->name('frontend.documents');
    Route::get('/document/{document}/{title?}', 'Beta\DocumentController@detail')->name('frontend.document.detail');
    Route::post('/cart/partial/products', 'Beta\CartController@partialProduct')->name('frontend.cart.partial.product');
    Route::post('/cart/order', 'Beta\CartController@order')->name('frontend.cart.order');
    Route::get('/search', 'Beta\SearchController@index')->name('frontend.search');
});



Route::group(['prefix'=>'api'],function(){
    Route::get('/articleCategory/getList','Api\CommonDataController@getArticleCategories');
    Route::post('/article/getList','Api\ArticleController@getList');
    Route::post('/article/orderSlide','Api\ArticleController@orderSlide');
});

