<?php

namespace App\Providers;

use App\Model\Article;
use App\Model\ArticleCategory;
use App\Model\Company;
use App\Model\Customer;
use App\Model\Document;
use App\Model\Order;
use App\Model\Product;
use App\Model\ProductCategory;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
        Route::bind('company',function($value){
            $item = Company::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('productCategory',function($value){
            $item = ProductCategory::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('product',function($value){
            $item = Product::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('articleCategory',function($value){
            $item = ArticleCategory::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('article',function($value){
            $item = Article::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('news',function($value){
            $item = Article::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('customer',function($value){
            $item = Customer::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('order',function($value){
            $item = Order::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
        Route::bind('document',function($value){
            $item = Document::where('_id',$value)->first();
            if(!$item) abort(404);
            return $item;
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapBackendRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapBackendRoutes()
    {
        Route::domain('admin.epp.vn')
            ->middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/backend.php'));
    }
}
