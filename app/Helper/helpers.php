<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 21-Nov-18
 * Time: 2:39 AM
 */

if (! function_exists('message')) {
    function message($type,$text,$title=null) {
        if(!isset($title)) $title = 'System Notification';
        return ['type'=>$type,'title'=>$title,'text'=>$text];
    }
    function imageGrid($images,$numCol){
        $count = count($images);
        $html = '<div class="image-grid">';
        $html.= '<div class="grid-row">';
            $imgIndex = 0;
            $numRow = ceil($count / $numCol);
            for($colIndex=0;$colIndex<$numCol;$colIndex++){
                $html.= '<div class="grid-column-3">';
                for($rowIndex=0;$rowIndex<$numRow;$rowIndex++) {
                    if($imgIndex >= $count) break;
                    $html .= '<a href="javascript:;">';
                    $html .= '<img src="' . $images[$imgIndex] . '" class="img-fluid" data-action="open-slide" />';
                    $html .= '</a>';
                    $imgIndex++;
                }
                $html.= '</div>';
            }
        $html.= '</div>';
        $html.= '</div>';
        return $html;
    }
}
