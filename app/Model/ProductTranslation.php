<?php


namespace App\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProductTranslation extends Eloquent
{
    use Sluggable;
    protected $connection = 'mongodb';
    protected $collection = 'product_trans';
    protected $guarded = [];
    public $timestamps = false;

    public function product(){
        return $this->belongsTo('App\Model\Product');
    }

    public function sluggable(): array
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function page(){
        return $this->hasOne('App\Model\Page','item_id');
    }

    public static function boot() {
        parent::boot();
        static::created(function($tran){
            $product = $tran->product;
            $page = new Page();
            $page->meta_title = $tran->name;
            $page->meta_keywords = $tran->name;
            $page->meta_description = $tran->sapo;
            $page->meta_image = $product->avatar;
            $page->route = 'frontend.product.details';
            $page->locale = $tran->locale;
            $tran->page()->save($page);
        });
        static::updated(function($tran){
            $product = $tran->product;
            $page = $tran->page;
            if(!isset($page)){
                $page = new Page();
                $page->meta_title = $tran->name;
                $page->meta_keywords = $tran->name;
                $page->meta_description = $tran->sapo;
                $page->meta_image = $product->avatar;
                $page->route = 'frontend.product.details';
                $page->locale = $tran->locale;
                $tran->page()->save($page);
            }
        });
    }
}


