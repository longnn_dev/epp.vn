<?php


namespace App\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ArticleTranslation extends Eloquent
{
    use Sluggable;
    protected $connection = 'mongodb';
    protected $collection = 'article_trans';
    protected $guarded = [];
    public $timestamps = false;

    public function article(){
        return $this->belongsTo('App\Model\Article');
    }



    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function page(){
        return $this->hasOne('App\Model\Page','item_id');
    }

    public static function boot() {
        parent::boot();
        static::created(function($tran){
            $article = $tran->article;
            $page = new Page();
            $page->meta_title = $tran->name;
            $page->meta_keywords = $tran->name;
            $page->meta_description = $tran->sapo;
            $page->meta_image = $article->avatar;
            $page->route = 'frontend.news.detail.seo';
            $page->locale = $tran->locale;
            $tran->page()->save($page);
        });
        static::updated(function($tran){
            $article = $tran->article;
            $page = $tran->page;
            if(!isset($page)){
                $page = new Page();
                $page->meta_title = $tran->name;
                $page->meta_keywords = $tran->name;
                $page->meta_description = $tran->sapo;
                $page->meta_image = $article->avatar;
                $page->route = 'frontend.news.detail.seo';
                $page->locale = $tran->locale;
                $tran->page()->save($page);
            }
        });
    }
}


