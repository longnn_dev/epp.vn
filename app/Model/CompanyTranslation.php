<?php


namespace App\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CompanyTranslation extends Eloquent
{
    use Sluggable;
    protected $connection = 'mongodb';
    protected $collection = 'company_trans';
    protected $guarded = [];
    public $timestamps = false;

    public function company(){
        return $this->belongsTo('App\Model\Company');
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function page(){
        return $this->hasOne('App\Model\Page','item_id');
    }

    public static function boot() {
        parent::boot();
        static::created(function($tran){
            $company = $tran->company;
            $page = new Page();
            $page->meta_title = $tran->name;
            $page->meta_keywords = $tran->name;
            $page->meta_description = $tran->sapo;
            $page->meta_image = $company->avatar;
            $page->route = 'frontend.partner.detail';
            $page->locale = $tran->locale;
            $tran->page()->save($page);
        });
        static::updated(function($tran){
            $company = $tran->company;
            $page = $tran->page;
            if(!isset($page)){
                $page = new Page();
                $page->meta_title = $tran->name;
                $page->meta_keywords = $tran->name;
                $page->meta_description = $tran->sapo;
                $page->meta_image = $company->avatar;
                $page->route = 'frontend.partner.detail';
                $page->locale = $tran->locale;
                $tran->page()->save($page);
            }
        });
    }
}


