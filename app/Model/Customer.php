<?php
/**
 * Created by PhpStorm.
 * User: LongNN
 * Date: 9/24/2019
 * Time: 5:37 PM
 */


namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Customer extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'customer';
    protected $guarded = [];

    public function orders(){
        return $this->hasMany('App\Model\Order');
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($item) {
            $item->orders()->delete();
        });
    }
}
