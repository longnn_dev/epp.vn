<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 26-Sep-18
 * Time: 4:56 PM
 */

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Page extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'pages';
    protected $guarded = ['_id'];

    public function parent(){
        return $this->belongsTo('App\Model\Page');
    }

    public static function pick($route,$locale='en',$itemId=null){
        $query = Page::query();
        $query->where('route',$route);
        $query->where('locale',$locale);
        if($itemId){
            $query->where('item_id',$itemId);
        }
        return $query->first();
    }

}
