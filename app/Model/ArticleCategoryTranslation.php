<?php


namespace App\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ArticleCategoryTranslation extends Eloquent
{
    use Sluggable;
    protected $connection = 'mongodb';
    protected $collection = 'article_category_trans';
    protected $guarded = [];
    public $timestamps = false;

    public function articleCategory(){
        return $this->belongsTo('App\Model\ArticleCategory');
    }

    public function sluggable(): array
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }



}


