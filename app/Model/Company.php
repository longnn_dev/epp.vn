<?php


namespace App\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Company extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'company';
    protected $guarded = [];

    public function trans(){
        return $this->hasMany('App\Model\CompanyTranslation');
    }

    public function tran($locale){
        return $this->trans()->where('locale',$locale)->first();
    }

    public function hasTran($locale){
        return $this->trans()->where('locale',$locale)->exists();
    }

    public function whereTran($locale,$field,$operation,$value){
        $query = $this->trans()->getQuery();
        $query->where('locale',$locale);
        $query->where($field,$operation,$value);
        return $query->exists();
    }

    public function getUrl($locale){
        $paths = ['news'=>$this->_id];
        $tran = $this->tran($locale);
        if(isset($tran)){
            $paths['title'] = $tran->slug;
        }
        return route('frontend.partner.detail',$paths);
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($company) {
            $company->trans()->delete();
        });
    }
}


