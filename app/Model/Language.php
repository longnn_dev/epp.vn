<?php


namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Language extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'language';
}
