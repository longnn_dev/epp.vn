<?php


namespace App\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProductCategoryTranslation extends Eloquent
{
    use Sluggable;
    protected $connection = 'mongodb';
    protected $collection = 'product_category_trans';
    protected $guarded = [];
    public $timestamps = false;

    public function productCategory(){
        return $this->belongsTo('App\Model\ProductCategory');
    }

    public function sluggable(): array
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

}


