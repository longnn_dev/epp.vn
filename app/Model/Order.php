<?php
/**
 * Created by PhpStorm.
 * User: LongNN
 * Date: 9/24/2019
 * Time: 5:38 PM
 */

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Order extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'order';
    protected $guarded = [];

    public function products(){
        return $this->belongsToMany('App\Model\Product');
    }

    public function customer(){
        return $this->belongsTo('App\Model\Customer');
    }
}
