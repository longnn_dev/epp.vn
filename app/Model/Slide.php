<?php
/**
 * Created by PhpStorm.
 * User: LongNN
 * Date: 9/24/2019
 * Time: 5:37 PM
 */


namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Slide extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'slide';
    protected $guarded = [];

    public static function boot() {
        parent::boot();
    }
}
