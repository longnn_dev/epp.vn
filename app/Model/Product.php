<?php
/**
 * Created by PhpStorm.
 * User: LongNN
 * Date: 9/24/2019
 * Time: 5:40 PM
 */

namespace App\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Product extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'product';
    protected $guarded = [];

    public function category(){
        return $this->belongsTo('App\Model\ProductCategory');
    }

    public function customerRequests(){
        return $this->belongsToMany('App\Model\Order');
    }

    public function trans(){
        return $this->hasMany('App\Model\ProductTranslation');
    }

    public function tran($locale){
        return $this->trans()->where('locale',$locale)->first();
    }

    public function hasTran($locale){
        return $this->trans()->where('locale',$locale)->exists();
    }

    public function whereTran($locale,$field,$operation,$value){
        $query = $this->trans()->getQuery();
        $query->where('locale',$locale);
        $query->where($field,$operation,$value);
        return $query->exists();
    }

    public function hasColor($color){
        if(!isset($this->colors)) return false;
        return in_array($color,$this->colors);
    }

    public function getPriceRange($locale=null){
        $currency = "$";
        if($locale==="vi"){$currency="VNĐ";}
        if(!isset($this->prices)) return '0';
        $priceValues = [];
        foreach($this->prices as $price){
            $priceValues[] = $price['value'];
        }
        sort($priceValues);
        return $currency.$priceValues[0].' - '.$currency.$priceValues[count($priceValues)-1];
    }

    public function getUrl($locale){
        $paths = ['product'=>$this->_id];
        $tran = $this->tran($locale);
        if(isset($tran)){
            $paths['title'] = $tran->slug;
        }
        return route('frontend.product.details',$paths);
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($item) {
            $item->trans()->delete();
        });
    }
}
