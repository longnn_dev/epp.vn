<?php


namespace App\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProductCategory extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'product_category';
    protected $guarded = [];

    public function trans(){
        return $this->hasMany('App\Model\ProductCategoryTranslation');
    }

    public function tran($locale){
        return $this->trans()->where('locale',$locale)->first();
    }

    public function hasTran($locale){
        return $this->trans()->where('locale',$locale)->exists();
    }

    public function whereTran($locale,$field,$operation,$value){
        $query = $this->trans()->getQuery();
        $query->where('locale',$locale);
        $query->where($field,$operation,$value);
        return $query->exists();
    }

    public static function getAll(){
        $data = [];
        $items = ProductCategory::all();
        foreach($items as $item){
            $item->name = $item->tran('en')->name;
            $data[] = $item;
        }
        return $data;
    }

    public function getUrl($locale){
        $paths = ['productCategory'=>$this->_id];
        $tran = $this->tran($locale);
        if(isset($tran)){
            $paths['title'] = $tran->slug;
        }
        return route('frontend.productCategory',$paths);
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($item) {
            $item->trans()->delete();
        });
    }
}


