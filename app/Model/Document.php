<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Document extends Eloquent
{
    use Sluggable;
    protected $connection = 'mongodb';
    protected $collection = 'documents';
    protected $guard_name  = 'web';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function category(){
        return $this->belongsTo('App\Model\DocumentCategory');
    }

    public function author(){
        return $this->belongsTo('App\Model\User');
    }

    public function editor(){
        return $this->belongsTo('App\Model\User');
    }

    public function tags(){
        return $this->embedsMany('App\Model\Tag');
    }
    public function printTags(){
        $tagsText = "";
        if($this->tags){
            for($i=0;$i<count($this->tags);$i++){
                $tagsText.= $this->tags[$i]->name;
                if($i<count($this->tags)-1){
                    $tagsText.=" , ";
                }
            }
        }
        return $tagsText;
    }
    public function getTagIds(){
        $ids = [];
        foreach($this->tags as $tag){
            $ids[] = $tag->_id;
        }
        return $ids;
    }

    public function getUrl($locale){
        $paths = ['document'=>$this->_id];
        $paths['title'] = $this->slug;
        return route('frontend.document.detail',$paths);
    }

    public function sluggable(): array
    {
        // TODO: Implement sluggable() method.
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
