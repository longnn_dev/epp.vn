<?php


namespace App\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ArticleCategory extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'article_category';
    protected $guarded = [];

    public function trans(){
        return $this->hasMany('App\Model\ArticleCategoryTranslation');
    }

    public function tran($locale){
        return $this->trans()->where('locale',$locale)->first();
    }

    public function hasTran($locale){
        return $this->trans()->where('locale',$locale)->exists();
    }

    public function whereTran($locale,$field,$operation,$value){
        $query = $this->trans()->getQuery();
        $query->where('locale',$locale);
        $query->where($field,$operation,$value);
        return $query->exists();
    }

    public static function getAll(){
        $data = [];
        $items = ArticleCategory::all();
        foreach($items as $item){
            $item->name = $item->tran('en')->name;
            $data[] = $item;
        }
        return $data;
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($item) {
            $item->trans()->delete();
        });
    }
}


