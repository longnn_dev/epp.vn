<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 05-Dec-18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Model\ArticleCategory;
use App\Model\District;
use App\Model\DocumentCategory;
use App\Model\ProductCategory;
use App\Model\Province;
use App\Model\Tag;
use Illuminate\Http\Request;
use MongoDB\BSON\ObjectID;

class CommonDataController extends Controller
{
    public function getArticleCategories(Request $request){
        $title = $request->get('name');
        $query = ArticleCategory::query();
        $query->select('_id','name');
        if(!empty($title)){
            $query->whereHas('trans',function ($q) use($title){
                return $q->where('name','like',"%{$title}%");
            });
        }
        $query->where('status',1);
        $query->limit(5);
        $articleCategories = $query->get();
        return response()->json($articleCategories);
    }
    public function getProductCategories(Request $request){
        $name = $request->get('name');
        $query = ProductCategory::query();
        $query->select('_id','name');
        if($name){
            $query->where('name','like','%'.$name.'%');
        }
        $query->where('status',true);
        $articleCategories = $query->get();
        return response()->json($articleCategories);
    }
    public function getDocumentCategories(Request $request){
        $name = $request->get('name');
        $query = DocumentCategory::query();
        $query->select('_id','name');
        if($name){
            $query->where('name','like','%'.$name.'%');
        }
        $query->where('status',true);
        $categories = $query->get();
        return response()->json($categories);
    }
    public function getProvinces(Request $request){
        $limit = 20;
        $name = $request->get('name');
        $rLimit = $request->get('limit');
        $query = Province::query();
        $query->select('_id','name');
        if($name){
            $query = $query->where('name_vi','like','%'.$name.'%');
        }
        if($rLimit){
            $limit = $rLimit;
        }
        $query->limit($limit);
        $items = $query->get();
        return response()->json($items);
    }
    public function getDistricts(Request $request){
        $limit = 20;
        $name = $request->get('name');
        $province_id = $request->get('province_id');
        $rLimit = $request->get('limit');
        $query = District::query();
        $query->select('_id','name');
        if($name){
            $query = $query->where('name','like','%'.$name.'%');
        }
        if($province_id){
            $query = $query->where('province_id',$province_id);
        }
        if($rLimit){
            $limit = $rLimit;
        }
        $query->limit($limit);
        $items = $query->get();
        return response()->json($items);
    }
    public function getTags(Request $request){
        $limit = 20;
        $name = $request->get('name');
        $rLimit = $request->get('limit');
        $query = Tag::query();
        $query->select('_id','name');
        if($name){
            $query = $query->where('name','like','%'.$name.'%');
        }
        if($rLimit){
            $limit = $rLimit;
        }
        $query->limit($limit);
        $tags = $query->get();
        return response()->json($tags);
    }
    public function getFurnitures(Request $request){
        $furnitures = config('product.furnitures');
        return response()->json($furnitures);
    }
    public function search(Request $request){
        $query = $request->post('q');
    }
}
