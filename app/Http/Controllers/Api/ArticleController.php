<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 01-Oct-18
 * Time: 1:36 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Article;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function getList(Request $request){
        $query = Article::query();
        $query->select('_id','name','tags','author_id','updated_at');
        if($request->post('catId')){
            $query->where('category_id',$request->post('catId'));
        }
        if($request->post('authorId')){
            $query->where('author_id',$request->post('authorId'));
        }
        if($request->post('updatedDate')){
            $requestDate = $request->post('updatedDate');
            $start = Carbon::parse($requestDate." 00:00:00");
            $end = Carbon::parse($requestDate." 23:59:59");
            $query->whereBetween('updated_at',[$start,$end]);
        }
        $tags = [];
        if($request->post('tags')){
            $tags = $request->post('tags');
            $query->whereIn('tags.name',$tags);
        }
        $articles = $query->get();
        for($i=0;$i<count($articles);$i++){
            $article = $articles[$i];
            $article->author = User::find($article->author_id)->name;
            $article->updatedAt = Carbon::parse($article->updated_at)->format('d/m/Y');
            unset($article->author_id);
            unset($article->updated_at);
            $articles[$i] = $article;
        }
        return response()->json($articles);
    }
    public function orderSlide(Request $request){
        $ids = $request->post('ids');
        if(!$ids || !is_array($ids)){
            return response()->json([
                'errorCode'=>-1
            ]);
        }
        $currentList = Article::where('slide',true)->get();
        foreach($currentList as $item){
            if(!in_array($item->_id,$ids)){
                $item->slide = false;
                $item->save();
            }
        }
        $orderNum = 1;
        foreach($ids as $id){
            $article = Article::find($id);
            $article->slide = true;
            $article->orderNum = $orderNum;
            $article->save();
            $orderNum++;
        }
    }
}