<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 01-Oct-18
 * Time: 1:36 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\ArticleCategory;
use Illuminate\Http\Request;

class ArticleCategoryController extends Controller
{
    public function getList(Request $request){
        $name = $request->get('name');
        $query = ArticleCategory::query();
        $query->select('_id','name');
        if($name){
            $query->where('name','like','%'.$name.'%');
        }
        $query->where('status',true);
        $query->limit(5);
        $articleCategories = $query->get();
        return response()->json($articleCategories);
    }
}