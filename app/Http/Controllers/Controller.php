<?php

namespace App\Http\Controllers;

use App\Model\Company;
use App\Model\Page;
use App\Model\ProductCategory;
use App\Model\Setting;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $pCats,$locale,$page;
    public function __construct(){
        $routeName = Route::currentRouteName();
        if(Session::has('locale'))
        {
            App::setLocale(Session::get('locale'));
        }
        $this->locale = App::getLocale();
        $company = Company::where('type','main')->first();
        $companyTrans = $company->tran($this->locale);
        $this->pCats = ProductCategory::get();
        //SEO
        $routeName = Route::currentRouteName();
        $this->page = Page::where('route',$routeName)->where('locale',App::getLocale())->first();
        if(!isset($this->page)){
            $this->page = Page::where('route','frontend.home')->where('locale',App::getLocale())->first();
        }
        view()->share('locale',$this->locale);
        view()->share('ourCompany',$company);
        view()->share('ourCompanyTrans',$companyTrans);
        view()->share('routeName',$routeName);
        view()->share('pCats',$this->pCats);
        view()->share('meta',$this->page);
    }
}
