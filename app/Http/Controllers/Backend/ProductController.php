<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\CompanyTranslation;
use App\Model\Product;
use App\Model\ProductCategory;
use App\Model\ProductTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{

    public function datatable(Request $request){
        $columns = [
            0 => 'avatar',
            1 => 'code',
            2 => 'created_at',
            3 => 'updated_at',
            4 => 'status',
            5 => 'type',
            6 => 'trans',
            7 => '_id'
        ];
        $limit = intval($request->input('length'));
        $offset = intval($request->input('start'));
        $orderColumn = $columns[$request->input('order.0.column')];
        $orderDir = $request->input('order.0.dir');
        // FILTER
        $code = $request->input('columns.1.search.value');
        $status = $request->input('columns.5.search.value');
        $total = Product::count();
        $query = Product::query();
        $appLocales = config('app.locales');
        if(!empty($code)){
            $query->where('code','like',"%{$code}%");
        }
        if(in_array($status,["0","1"])){
            $query->where('status',intval($status));
        }
        $totalFiltered = $query->count();
        $query->limit($limit);
        $query->offset($offset);
        $query->orderBy($orderColumn,$orderDir);
        $items = $query->get();
        $data = [];
        $locale = App::getLocale();
        $appLocales = config('app.locales');
        foreach($items as $item){
            $itemLangs = [];
            foreach($appLocales as $appLocale){
                $itemLang = new \stdClass();
                $itemLang->locale = $appLocale;
                $itemLang->available = $item->hasTran($appLocale)?true:false;
                $itemLangs[] = $itemLang;
            }
                $nItem = [];
                $nItem['id'] = $item->_id;
                $nItem['code'] = $item->code;
                $nItem['avatar'] = $item->avatar;
                $nItem['status'] = $item->status;
                $nItem['langs'] = $itemLangs;
                $nItem['type'] = isset($item->type)?$item->type:0;
                $nItem['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
                $nItem['updated_at'] = \Carbon\Carbon::parse($item->updated_at)->format('d/m/Y H:i');
                $data[] = $nItem;
            }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($total),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);

    }
    public function index(){
        return view('backend.pages.product.list');
    }
    public function add(Request $request){
        $productCats = ProductCategory::getAll();
        return view('backend.pages.product.add')->with('categories',$productCats);
    }
    public function edit(Product $product){
        $productCats = ProductCategory::getAll();
        return view('backend.pages.product.edit')->with('product',$product)->with('categories',$productCats);
    }
    public function store(Request $request){
        $catId = $request->post('catId');
        $category = ProductCategory::find($catId);
        $product = new Product();
        $product->code = $request->post('code');
        $product->category()->associate($category);
        $product->colors = $request->post('colors');
        $product->prices = $request->post('prices');
        $product->min_order = $request->post('minOrder');
        $product->lead_times = $request->post('leadTimes');
        $product->sizes = $request->post('sizes');
        $product->avatar = $request->post('avatar');
        $product->photos = $request->post('photos');
        $product->videos = $request->post('videos');
        $product->status = $request->post('status');
        $product->save();
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success'),
            "type"=>'success'
        ];
        return response()->json($result);
    }
    public function update(Product $product){
        $catId = Input::get('catId');
        $category = ProductCategory::find($catId);
        $product->code = Input::get('code');
        $product->category()->associate($category);
        $product->colors = Input::get('colors');
        $product->prices = Input::get('prices');
        $product->min_order = Input::get('minOrder');
        $product->lead_times = Input::get('leadTimes');
        $product->sizes = Input::get('sizes');
        $product->avatar = Input::get('avatar');
        $product->photos = Input::get('photos');
        $product->videos = Input::get('videos');
        $product->status = Input::get('status');
        $product->save();
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success'),
            "type"=>'success'
        ];
        return response()->json($result);
    }
    public function changeStatus(Product $item){
        if($item->status == 0){
            $item->status = 1;
        }else{
            $item->status = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->status,
            'message'=>'success'
        ],200);
    }
    public function delete(Product $item){
        $item->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }
    public function setHot(Product $item){
        if($item->type == 0){
            $item->type = 1;
        }else{
            $item->type = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->type,
            'message'=>'success'
        ],200);
    }
    //Translation
    public function trans(Product $product,$locale){
        $tran = $product->tran($locale);
        if(!isset($tran)){
            $tran = new ProductTranslation();
        }
        return view('backend.pages.product.translation')
            ->with('product',$product)
            ->with('locale',$locale)
            ->with('tran',$tran);
    }
    public function saveTrans(Product $product,$locale){
        $translation = new ProductTranslation();
        if($product->hasTran($locale)){
            $translation = $product->tran($locale);
        }
        $translation->locale = $locale;
        $translation->name = Input::get('name');
        $translation->quick_details = Input::get('quickDetails');
        $translation->supply_ability = Input::get('supplyAbility');
        $translation->packaging_delivery = Input::get('packagingDelivery');
        $product->trans()->save($translation);
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success'),
            "type"=>'success'
            ];
        return response()->json($result);
    }
}
