<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\ProductCategory;
use App\Model\ProductCategoryTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class ProductCategoryController extends Controller
{
    public function index(){
        return view('backend.pages.productCategory.list');
    }
    public function datatable(Request $request){
        $columns = [
            0 => 'avatar',
            1 => 'code',
            2 => 'name',
            3 => 'trans',
            4 => 'created_at',
            5 => 'updated_at',
            6 => 'status',
            7 => 'type',
            8 => '_id'
        ];
        $limit = intval($request->input('length'));
        $offset = intval($request->input('start'));
        $orderColumn = $columns[$request->input('order.0.column')];
        $orderDir = $request->input('order.0.dir');
        // FILTER
        $code = $request->input('columns.1.search.value');
        $name = $request->input('columns.2.search.value');
        $status = $request->input('columns.5.search.value');
        $total = ProductCategory::count();
        $query = ProductCategory::query();
        $appLocales = config('app.locales');
        if(!empty($name)){
            $query->whereHas('trans',function($q) use($name){
                return $q->where('name','like',"%{$name}%");
            });
        }
        if(!empty($code)){
            $query->where('code','like',"%{$code}%");
        }
        if(in_array($status,["0","1"])){
            $query->where('status',intval($status));
        }
        $totalFiltered = $query->count();
        $query->limit($limit);
        $query->offset($offset);
        $query->orderBy($orderColumn,$orderDir);
        $items = $query->get();
        $data = [];
        $locale = App::getLocale();
        $appLocales = config('app.locales');
        foreach($items as $item){
            $itemLangs = [];
            foreach($appLocales as $appLocale){
                $itemLang = new \stdClass();
                $itemLang->locale = $appLocale;
                $itemLang->available = $item->hasTran($appLocale)?true:false;
                $itemLangs[] = $itemLang;
            }
                $nItem = [];
                $nItem['id'] = $item->_id;
                $nItem['code'] = $item->code;
                $nItem['name'] = $item->hasTran($locale)?$item->tran($locale)->name:'N/A';
                $nItem['avatar'] = $item->avatar;
                $nItem['status'] = $item->status;
                $nItem['langs'] = $itemLangs;
                $nItem['type'] = isset($item->type)?$item->type:0;
                $nItem['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
                $nItem['updated_at'] = \Carbon\Carbon::parse($item->updated_at)->format('d/m/Y H:i');
                $data[] = $nItem;
            }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($total),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);

    }
    public function add(Request $request){
        return view('backend.pages.productCategory.add');
    }
    public function edit(ProductCategory $productCategory){
        return view('backend.pages.productCategory.edit')->with('productCategory',$productCategory);
    }
    public function store(Request $request){
        $productCategory = new ProductCategory();
        $productCategory->avatar = $request->post('avatar');
        $productCategory->code = $request->post('code');
        $productCategory->status = $request->post('status');
        $productCategory->save();
        return redirect()->route('backend.productCategory.index')->with('message',message('success',trans('trans.message.productCategory.save.success')));
    }
    public function update(ProductCategory $productCategory){
        $productCategory->avatar = Input::get('avatar');
        $productCategory->code = Input::get('code');
        $productCategory->status = Input::get('status');
        $productCategory->save();
        return redirect()->route('backend.productCategory.index')->with('message',message('success',trans('trans.message.productCategory.save.success')));
    }
    public function changeStatus(ProductCategory $item){
        if($item->status == 0){
            $item->status = 1;
        }else{
            $item->status = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->status,
            'message'=>'success'
        ],200);
    }
    public function setHot(ProductCategory $item){
        if($item->type == 0){
            $item->type = 1;
        }else{
            $item->type = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->type,
            'message'=>'success'
        ],200);
    }
    public function delete(ProductCategory $item){
        try{
            $item->delete();
            return response()->json([
                'errorCode'=>0,
                'message'=>'success'
            ],200);
        }catch (\Exception $e){
            return response()->json([
                'errorCode'=>1,
                'message'=>'error'
            ],500);
        }



    }

    //Translation
    public function trans(ProductCategory $productCategory,$locale){
        $tran = $productCategory->tran($locale);
        if(!isset($tran)){
            $tran = new ProductCategoryTranslation();
        }
        return view('backend.pages.productCategory.translation')
            ->with('productCategory',$productCategory)
            ->with('locale',$locale)
            ->with('tran',$tran);
    }
    public function saveTrans(ProductCategory $productCategory,$locale){
        $translation = new ProductCategoryTranslation();
        if($productCategory->hasTran($locale)){
            $translation = $productCategory->tran($locale);
        }
        $translation->locale = $locale;
        $translation->name = Input::get('name');
        $productCategory->trans()->save($translation);
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.productCategory.save.success'),
            "type"=>'success'
            ];
        return response()->json($result);
    }
}
