<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\ArticleCategory;
use App\Model\Company;
use App\Model\CompanyTranslation;
use App\Model\Article;
use App\Model\ArticleTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class ArticleController extends Controller
{
    public function index(){
        return view('backend.pages.article.list');
    }
    public function datatable(Request $request){
        $columns = [
            0 => 'avatar',
            1 => 'name',
            2 => 'created_at',
            3 => 'updated_at',
            4 => 'status',
            5 => 'type',
            6 => 'trans',
            7 => '_id'
        ];
        $limit = intval($request->input('length'));
        $offset = intval($request->input('start'));
        $orderColumn = $columns[$request->input('order.0.column')];
        $orderDir = $request->input('order.0.dir');
        // FILTER
        $status = $request->input('columns.5.search.value');
        $total = Article::count();
        $query = Article::query();
        $appLocales = config('app.locales');
        if(in_array($status,["0","1"])){
            $query->where('status',intval($status));
        }
        $totalFiltered = $query->count();
        $query->limit($limit);
        $query->offset($offset);
        $query->orderBy($orderColumn,$orderDir);
        $items = $query->get();
        $data = [];
        $locale = App::getLocale();
        $appLocales = config('app.locales');
        foreach($items as $item){
            $itemLangs = [];
            foreach($appLocales as $appLocale){
                $itemLang = new \stdClass();
                $itemLang->locale = $appLocale;
                $itemLang->available = $item->hasTran($appLocale)?true:false;
                $itemLangs[] = $itemLang;
            }
                $nItem = [];
                $nItem['id'] = $item->_id;
                $nItem['name'] = $item->hasTran($locale)?$item->tran($locale)->name:'N/A';
                $nItem['avatar'] = $item->avatar;
                $nItem['status'] = $item->status;
                $nItem['type'] = $item->type?$item->type:0;
                $nItem['langs'] = $itemLangs;
                $nItem['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
                $nItem['updated_at'] = \Carbon\Carbon::parse($item->updated_at)->format('d/m/Y H:i');
                $data[] = $nItem;
            }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($total),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);

    }
    public function add(Request $request){
        $categories = ArticleCategory::getAll();
        return view('backend.pages.article.add')->with('categories',$categories);
    }
    public function edit(Article $article){
        $categories = ArticleCategory::getAll();
        return view('backend.pages.article.edit')->with('article',$article)->with('categories',$categories);
    }
    public function store(Request $request){
        $catId = $request->post('catId');
        $category = ArticleCategory::find($catId);
        $article = new Article();
        $article->avatar = $request->post('avatar');
        $article->status = $request->post('status');
        $article->category()->associate($category);
        $article->save();
        return redirect()->route('backend.article.trans',[$article->_id,'en'])->with('message',message('success',trans('trans.message.save.success')));
    }
    public function update(Article $article){
        $catId = Input::get('catId');
        $category = ArticleCategory::find($catId);
        $article->avatar = Input::get('avatar');
        $article->status = Input::get('status');
        $article->category()->associate($category);
        $article->save();
        return redirect()->route('backend.article.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function changeStatus(Article $item){
        if($item->status == 0){
            $item->status = 1;
        }else{
            $item->status = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->status,
            'message'=>'success'
        ],200);
    }
    public function delete(Article $item){
        $item->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }
    public function setHot(Article $item){
        if($item->type == 0){
            $item->type = 1;
        }else{
            $item->type = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->type,
            'message'=>'success'
        ],200);
    }
    public function articleSlide(){
        $query = Article::query();
        $query->where('slide',null)->orwhere('slide',false);
        $availableArticles = $query->get();
        $selectedArticles = Article::where('slide',true)->orderBy('orderNum','asc')->get();
        return view('backend.pages.article.slide')
            ->with('availableArticles',$availableArticles)
            ->with('selectedArticles',$selectedArticles);
    }
    //Translation
    public function trans(Article $article,$locale){
        $tran = $article->tran($locale);
        if(!isset($tran)){
            $tran = new ArticleTranslation();
        }
        return view('backend.pages.article.translation')
            ->with('article',$article)
            ->with('locale',$locale)
            ->with('tran',$tran);
    }
    public function saveTrans(Article $article,$locale){
        $translation = new ArticleTranslation();
        if($article->hasTran($locale)){
            $translation = $article->tran($locale);
        }
        $translation->locale = $locale;
        $translation->name = Input::get('name');
        $translation->sapo = Input::get('sapo');
        $translation->content = Input::get('content');
        $article->trans()->save($translation);
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success'),
            "type"=>'success'
            ];
        return response()->json($result);
    }
}
