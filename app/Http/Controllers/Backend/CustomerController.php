<?php


namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Model\CustomerCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class CustomerController extends Controller
{

    public function index(){
        return view('backend.pages.customer.list');
    }
    public function datatable(Request $request){
        $columns = [
            0 => 'name',
            1 => 'email',
            2 => 'phone_number',
            3 => 'address',
            4 => 'company',
            5 => 'created_at',
            6 => '_id'
        ];
        $limit = intval($request->input('length'));
        $offset = intval($request->input('start'));
        $orderColumn = $columns[$request->input('order.0.column')];
        $orderDir = $request->input('order.0.dir');
        // FILTER
        $status = $request->input('columns.5.search.value');
        $total = Customer::count();
        $query = Customer::query();
        if(in_array($status,["0","1"])){
            $query->where('status',intval($status));
        }
        $totalFiltered = $query->count();
        $query->limit($limit);
        $query->offset($offset);
        $query->orderBy($orderColumn,$orderDir);
        $items = $query->get();
        $data = [];
        $locale = App::getLocale();
        foreach($items as $item){
            $nItem = [];
            $nItem['id'] = $item->_id;
            $nItem['name'] = $item->name;
            $nItem['email'] = $item->email;
            $nItem['phone_number'] = $item->phone_number;
            $nItem['address'] = $item->address;
            $nItem['company'] = $item->company;
            $nItem['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
            $data[] = $nItem;
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($total),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);

    }
    public function add(Request $request){
        return view('backend.pages.customer.add');
    }
    public function edit(Customer $customer){
        return view('backend.pages.customer.edit')->with('customer',$customer);
    }
    public function store(Request $request){
        $customer = new Customer();
        $customer->name = $request->post('name');
        $customer->email = $request->post('email');
        $customer->phone_number = $request->post('phone_number');
        $customer->address = $request->post('address');
        $customer->company = $request->post('company');
        $customer->status = $request->post('status');
        $customer->save();
        return redirect()->route('backend.customer.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function update(Customer $customer){
        $customer->name = Input::get('name');
        $customer->email = Input::get('email');
        $customer->phone_number = Input::get('phone_number');
        $customer->address = Input::get('address');
        $customer->company = Input::get('company');
        $customer->status = Input::get('status');
        $customer->save();
        return redirect()->route('backend.customer.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function changeStatus(Customer $item){
        if($item->status == 0){
            $item->status = 1;
        }else{
            $item->status = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->status,
            'message'=>'success'
        ],200);
    }
    public function delete(Customer $item){
        $item->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }
}
