<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Slide;
use Symfony\Component\HttpFoundation\Request;

class SlideshowController extends Controller
{
    public function index(){
        $slides = Slide::query()->get();
        return view('backend.pages.slide.list')->with('slides',$slides);
    }
    public function save(Request $request){
        $ids = $request->post("ids");
        Slide::query()->delete();
        foreach ($ids as $id){
            $slide = new Slide();
            $slide->url = $id;
            $slide->save();
        }
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success'),
            "type"=>'success'
        ];
        return response()->json($result);
    }
}
