<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 27-Oct-18
 * Time: 12:17 AM
 */

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function index(){
        $pages = Page::all();
        return view('backend.pages.page.list')->with('pages',$pages);
    }
    public function datatables(Request $request){
        $columns = array(
            0=>'id',
            1=>'meta_image',
            2=>'meta_title',
            3=>'item_type',
            4=>'tags',

        );
        $totalData = Page::count();
        $totalFiltered = $totalData;
        $limit = (int)$request->input('length');
        $start = (int)$request->input('start');
        $order = "updated_at";//$columns[$request->input('order.0.column')];
        $dir = "desc";//$request->input('order.0.dir');
        if(empty($request->input('search.value')))
        {
            $items = Page::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $items =  Page::where('meta_title','LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
            $totalFiltered = Page::where('meta_title','LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        $page_types = config('page.page_type');
        if(!empty($items))
        {
            foreach ($items as $item)
            {
                $nestedData['id'] = $item->_id;
                $nestedData['meta_image'] = $item->meta_image;
                $nestedData['meta_title'] = $item->meta_title;
                $nestedData['route'] = $page_types[$item->route];
                $nestedData['locale'] = $item->locale;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }
    public function create(){
        $page_types = config('page.page_type');
        return view('backend.pages.page.add')->with('page_types',$page_types);
    }
    public function show(Page $page){
    }
    public function edit(Page $page){
        $page_types = config('page.page_type');
        return view('backend.pages.page.edit')
            ->with('page',$page)
            ->with('page_types',$page_types);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'meta_title'=>['required'],
            'meta_keywords'=>['required'],
            'meta_description'=>['required'],
            'meta_image'=>['required'],
            'locale'=>['required'],
            'route'=>['required']
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }else{
            $page = new Page();
            $page->meta_title = $request->post('meta_title');
            $page->meta_keywords = $request->post('meta_keywords');
            $page->meta_description = $request->post('meta_description');
            $page->meta_image = $request->post('meta_image');
            $page->route = $request->post('route');
            $page->locale = $request->post('locale');
            $page->save();
            return redirect()->route('backend.system.page.index')->with('message',message('success',trans('trans.message.save.success')));
        }
    }
    public function update(Page $page){
        $validator = Validator::make(Input::all(),[
            'meta_title'=>['required'],
            'meta_keywords'=>['required'],
            'meta_description'=>['required'],
            'meta_image'=>['required'],
            'route'=>['required'],
            'locale'=>['required'],
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }else{
            $page->meta_title = Input::get('meta_title');
            $page->meta_keywords = Input::get('meta_keywords');
            $page->meta_description = Input::get('meta_description');
            $page->meta_image = Input::get('meta_image');
            $page->route = Input::get('route');
            $page->locale = Input::get('locale');
            $page->save();
            return redirect()->route('backend.system.page.index')->with('message',message('success',trans('trans.message.save.success')));
        }
    }
    public function destroy(Page $page){
        $page->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }
    public function updateStatus(Request $request){
        $_id = $request->post('_id');
        $item = Page::find($_id);
        if($item){
            if($item->status){
                $item->status = false;
            }else{
                $item->status = true;
            }
            $item->save();
            return response()->json([
                'errorCode'=>0,
                'status' => $item->status,
                'message'=>'success'
            ],200);
        }
        return response()->json([
            'errorCode'=>-1,
            'message'=>'error'
        ],200);
    }
    public function pageSlide(){
        $query = Page::query();
        $query->where('slide',null)->orwhere('slide',false);
        $availablePages = $query->get();
        $selectedPages = Page::where('slide',true)->orderBy('orderNum','asc')->get();
        return view('backend.pages.page.slide')
            ->with('availablePages',$availablePages)
            ->with('selectedPages',$selectedPages);
    }
}
