<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\CompanyTranslation;
use App\Model\ArticleCategory;
use App\Model\ArticleCategoryTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class ArticleCategoryController extends Controller
{
    public function index(){
        return view('backend.pages.articleCategory.list');
    }
    public function datatable(Request $request){
        $columns = [
            0 => 'avatar',
            1 => 'name',
            2 => 'created_at',
            3 => 'updated_at',
            4 => 'status',
            5 => 'type',
            6 => 'trans',
            7 => '_id'
        ];
        $limit = intval($request->input('length'));
        $offset = intval($request->input('start'));
        $orderColumn = $columns[$request->input('order.0.column')];
        $orderDir = $request->input('order.0.dir');
        // FILTER
        $status = $request->input('columns.5.search.value');
        $title = $request->input('columns.1.search.value');
        $total = ArticleCategory::count();
        $query = ArticleCategory::query();
        $appLocales = config('app.locales');
        if(in_array($status,["0","1"])){
            $query->where('status',intval($status));
        }
        if(!empty($title)){
            $query->whereHas('trans',function ($q) use($title){
               return $q->where('name','like',"%{$title}%");
            });
        }
        $totalFiltered = $query->count();
        $query->limit($limit);
        $query->offset($offset);
        $query->orderBy($orderColumn,$orderDir);
        $items = $query->get();
        $data = [];
        $locale = App::getLocale();
        $appLocales = config('app.locales');
        foreach($items as $item){
            $itemLangs = [];
            foreach($appLocales as $appLocale){
                $itemLang = new \stdClass();
                $itemLang->locale = $appLocale;
                $itemLang->available = $item->hasTran($appLocale)?true:false;
                $itemLangs[] = $itemLang;
            }
                $nItem = [];
                $nItem['id'] = $item->_id;
                $nItem['name'] = $item->hasTran($locale)?$item->tran($locale)->name:'N/A';
                $nItem['avatar'] = $item->avatar;
                $nItem['status'] = $item->status;
                $nItem['type'] = $item->type?$item->type:0;
                $nItem['langs'] = $itemLangs;
                $nItem['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
                $nItem['updated_at'] = \Carbon\Carbon::parse($item->updated_at)->format('d/m/Y H:i');
                $data[] = $nItem;
            }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($total),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);

    }
    public function add(Request $request){
        return view('backend.pages.articleCategory.add');
    }
    public function edit(ArticleCategory $articleCategory){
        return view('backend.pages.articleCategory.edit')->with('articleCategory',$articleCategory);
    }
    public function store(Request $request){
        $articleCategory = new ArticleCategory();
        $articleCategory->avatar = $request->post('avatar');
        $articleCategory->status = $request->post('status');
        $articleCategory->save();
        return redirect()->route('backend.articleCategory.trans',[$articleCategory->_id,'en'])->with('message',message('success',trans('trans.message.save.success')));
    }
    public function update(ArticleCategory $articleCategory){
        $articleCategory->avatar = Input::get('avatar');
        $articleCategory->status = Input::get('status');
        $articleCategory->save();
        return redirect()->route('backend.articleCategory.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function changeStatus(ArticleCategory $item){
        if($item->status == 0){
            $item->status = 1;
        }else{
            $item->status = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->status,
            'message'=>'success'
        ],200);
    }
    public function setHot(ArticleCategory $item){
        if($item->type == 0){
            $item->type = 1;
        }else{
            $item->type = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->type,
            'message'=>'success'
        ],200);
    }
    public function delete(ArticleCategory $item){
        $item->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }

    //Translation
    public function trans(ArticleCategory $articleCategory,$locale){
        $tran = $articleCategory->tran($locale);
        if(!isset($tran)){
            $tran = new ArticleCategoryTranslation();
        }
        return view('backend.pages.articleCategory.translation')
            ->with('articleCategory',$articleCategory)
            ->with('locale',$locale)
            ->with('tran',$tran);
    }
    public function saveTrans(ArticleCategory $articleCategory,$locale){
        $translation = new ArticleCategoryTranslation();
        if($articleCategory->hasTran($locale)){
            $translation = $articleCategory->tran($locale);
        }
        $translation->locale = $locale;
        $translation->name = Input::get('name');
        $articleCategory->trans()->save($translation);
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success'),
            "type"=>'success'
            ];
        return response()->json($result);
    }
}
