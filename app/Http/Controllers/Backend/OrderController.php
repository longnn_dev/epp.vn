<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Model\Order;
use App\Model\OrderCategory;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    public function index(){
        return view('backend.pages.order.list');
    }
    public function view(Request $request){
        $id = $request->post('item_id');
        $order = Order::find($id);
        return response()->json($order);
    }
    public function datatable(Request $request){
        $columns = [
            0 => 'name',
            1 => 'email',
            2 => 'phone_number',
            3 => 'message',
            4 => 'created_at',
            5 => 'status',
            6 => '_id'
        ];
        $limit = intval($request->input('length'));
        $offset = intval($request->input('start'));
        $orderColumn = $columns[$request->input('order.0.column')];
        $orderDir = $request->input('order.0.dir');
        // FILTER
        $status = $request->input('columns.5.search.value');
        $total = Order::count();
        $query = Order::query();
        if(in_array($status,["0","1"])){
            $query->where('status',intval($status));
        }
        $totalFiltered = $query->count();
        $query->limit($limit);
        $query->offset($offset);
        $query->orderBy($orderColumn,$orderDir);
        $items = $query->get();
        $data = [];
        $locale = App::getLocale();
        foreach($items as $item){
            $nItem = [];
            $nItem['id'] = $item->_id;
            $nItem['name'] = $item->customer->name;
            $nItem['email'] = $item->customer->email;
            $nItem['phone_number'] = $item->customer->phone_number;
            $nItem['message'] = $item->message;
            $nItem['status'] = $item->status;
            $nItem['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
            $data[] = $nItem;
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($total),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);

    }
    public function add(Request $request){
        return view('backend.pages.order.add');
    }
    public function edit(Order $order){
        return view('backend.pages.order.edit')->with('order',$order);
    }
    public function store(Request $request){
        $order = new Order();
        $order->name = $request->post('name');
        $order->email = $request->post('email');
        $order->phone_number = $request->post('phone_number');
        $order->address = $request->post('address');
        $order->company = $request->post('company');
        $order->status = $request->post('status');
        $order->save();
        return redirect()->route('backend.order.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function update(Order $order){
        $order->name = Input::get('name');
        $order->email = Input::get('email');
        $order->phone_number = Input::get('phone_number');
        $order->address = Input::get('address');
        $order->company = Input::get('company');
        $order->status = Input::get('status');
        $order->save();
        return redirect()->route('backend.order.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function changeStatus(Order $item){
        if($item->status == 0){
            $item->status = 1;
        }else{
            $item->status = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->status,
            'message'=>'success'
        ],200);
    }
    public function delete(Order $item){
        $item->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }
}
