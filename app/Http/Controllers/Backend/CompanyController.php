<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\CompanyTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class CompanyController extends Controller
{
    public function index(){
        return view('backend.pages.company.list');
    }
    public function datatable(Request $request){
        $columns = [
            0 => 'logo',
            1 => 'brand',
            2 => 'created_at',
            3 => 'updated_at',
            4 => 'status',
            5 => 'type',
            6 => 'trans',
            7 => '_id'
        ];
        $limit = intval($request->input('length'));
        $offset = intval($request->input('start'));
        $orderColumn = $columns[$request->input('order.0.column')];
        $orderDir = $request->input('order.0.dir');
        // FILTER
        $name = $request->input('columns.1.search.value');
        $status = $request->input('columns.5.search.value');
        $type = $request->input('columns.6.search.value');
        $total = Company::count();
        $query = Company::query();
        $appLocales = config('app.locales');
        if(!empty($name)){
            $query->where('brand','like',"%{$name}%");
        }
        if(in_array($status,["0","1"])){
            $query->where('status',intval($status));
        }
        if($type){
            $query->where('type',$type);
        }
        $totalFiltered = $query->count();
        $query->limit($limit);
        $query->offset($offset);
        $query->orderBy($orderColumn,$orderDir);
        $items = $query->get();
        $data = [];
        $locale = App::getLocale();
        $appLocales = config('app.locales');
        foreach($items as $item){
            $itemLangs = [];
            foreach($appLocales as $appLocale){
                $itemLang = new \stdClass();
                $itemLang->locale = $appLocale;
                $itemLang->available = $item->hasTran($appLocale)?true:false;
                $itemLangs[] = $itemLang;
            }
                $nItem = [];
                $nItem['id'] = $item->_id;
                $nItem['brand'] = $item->brand;
                $nItem['logo'] = $item->logo;
                $nItem['status'] = $item->status;
                $nItem['type'] = $item->type;
                $nItem['langs'] = $itemLangs;
                $nItem['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
                $nItem['updated_at'] = \Carbon\Carbon::parse($item->updated_at)->format('d/m/Y H:i');
                $data[] = $nItem;
            }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($total),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);

    }
    public function add(Request $request){
        $type = $request->has('type')?$request->get('type'):'partner';
        return view('backend.pages.company.add')->with('type',$type);
    }
    public function edit(Company $company){
        return view('backend.pages.company.edit')->with('company',$company);
    }
    public function store(Request $request){
        $company = new Company();
        $company->logo = $request->post('logo');
        $company->brand = $request->post('brand');
        $company->phone_numbers = $request->post('phone_numbers');
        $company->emails = $request->post('emails');
        $company->type = $request->post('type');
        $company->status = $request->post('status');
        $company->save();
        return redirect()->route('backend.company.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function update(Company $company){
        $company->logo = Input::get('logo');
        $company->brand = Input::get('brand');
        $company->phone_numbers = Input::get('phone_number');
        $company->emails = Input::get('email');
        $company->type = Input::get('type');
        $company->status = Input::get('status');
        $company->save();
        return redirect()->route('backend.company.index')->with('message',message('success',trans('trans.message.save.success')));
    }
    public function changeStatus(Company $item){
        if($item->status == 0){
            $item->status = 1;
        }else{
            $item->status = 0;
        }
        $item->save();
        return response()->json([
            'errorCode'=>0,
            'status' => $item->status,
            'message'=>'success'
        ],200);
    }
    public function delete(Company $item){
        $item->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }

    //Translation
    public function trans(Company $company,$locale){
        $tran = $company->tran($locale);
        if(!isset($tran)){
            $tran = new CompanyTranslation();
        }
        return view('backend.pages.company.translation')
            ->with('company',$company)
            ->with('locale',$locale)
            ->with('tran',$tran);
    }
    public function saveTrans(Company $company,$locale){
        $translation = new CompanyTranslation();
        if($company->hasTran($locale)){
            $translation = $company->tran($locale);
        }
        $translation->locale = $locale;
        $translation->name = Input::get('name');
        $translation->address = Input::get('address');
        $translation->alt_address = Input::get('alt_address');
        $translation->facebook = Input::get('facebook');
        $translation->twitter = Input::get('twitter');
        $translation->instagram = Input::get('instagram');
        $translation->youtube = Input::get('youtube');
        $translation->about = Input::get('about');
        $translation->properties = Input::get('properties');
        $translation->avatar = Input::get('avatar');
        $translation->bom = Input::get('bom');
        $translation->videos = Input::get('videos');
        $translation->photos = Input::get('photos');
        $company->trans()->save($translation);
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.company.save.success'),
            "type"=>'success'
            ];
        return response()->json($result);
    }
}
