<?php
/**
 * Created by PhpStorm.
 * User: longn
 * Date: 13-Feb-19
 * Time: 10:34 PM
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Model\CustomerRequest;
use App\Model\Document;
use App\Model\DocumentCategory;
use App\Model\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    public function datatables(Request $request){
        $columns = array(
            0=>'id',
            1=>'name',
            3=>'content',
            4=>'file_url',
            5=>'created_at',
            6=>'updated_at',
        );
        $totalData = Document::count();
        $totalFiltered = $totalData;
        $limit = (int)$request->input('length');
        $start = (int)$request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value')))
        {
            $list = Document::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $list =  Document::where('_id','LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Document::where('_id','LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($list))
        {
            foreach ($list as $item)
            {
                //$show =  route('backend.data.document.show',$item->_id);
                //$edit =  route('backend.data.document.edit',$item->_id);
                $nestedData['id'] = $item->_id;
                $nestedData['cover_url'] = $item->cover_url;
                $nestedData['file_url'] = $item->name;
                $nestedData['name'] = $item->name;
                $nestedData['content'] = $item->content;
                $nestedData['locale'] = $item->locale;
                $nestedData['status'] = $item->status;
                $nestedData['created_at'] = \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i');
                $nestedData['updated_at'] = \Carbon\Carbon::parse($item->updated_at)->format('d/m/Y H:i');
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }
    public function index(){
        $items = Document::all();
        return view('backend.pages.document.list')->with('document',$items);
    }

    public function create(){
        return view('backend.pages.document.add');
    }

    public function edit(Document $document){
        return view('backend.pages.document.edit')
            ->with('document',$document);
    }

    public function store(Request $request){
        $document = new Document();
        $document->name = $request->post('name');
        $document->content = $request->post('content');
        $document->cover_url = $request->post('cover_url');
        $document->file_url = $request->post('file_url');
        $document->locale = $request->post('locale');
        $document->status = $request->post('status');
        $document->save();
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success'),
            "type"=>'success'
        ];
        return response()->json($result);
    }

    public function update(Document $document){
        $document->name = Input::get('name');
        $document->content = Input::get('content');
        $document->cover_url = Input::get('cover_url');
        $document->file_url = Input::get('file_url');
        $document->locale = Input::get('locale');
        $document->status = Input::get('status');
        $document->save();
        $result = [
            "status"=>0,
            "message"=>trans('trans.message.save.success.success'),
            "type"=>'success'
        ];
        return response()->json($result);
    }

    public function destroy(Document $document){
        $document->delete();
        return response()->json([
            'errorCode'=>0,
            'message'=>'success'
        ],200);

    }
}
