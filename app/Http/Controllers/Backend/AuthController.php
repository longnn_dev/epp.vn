<?php

/**
 * Created by PhpStorm.
 * User: longn
 * Date: 04-Aug-19
 * Time: 10:59 PM
 */
namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends \App\Http\Controllers\Controller
{
    use RedirectsUsers, ThrottlesLogins;
    public function index(){
        return view('backend.pages.login');
    }
    public function login(Request $request){
        $rules = [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ];
        $credentials = [
            'email' => $request->post('email'),
            'password' => $request->post('password'),
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if(Auth::attempt($credentials,$request->has('remember'))){
            $user = Auth::user();
            if($user->isAdmin()){
                return redirect()->route('backend.dashboard');
            }else{
                return redirect()->back()->withErrors('You must be administrator');
            }
        }else{
            return redirect()->back()->withErrors('Your email and password are not correct');
        }
    }
    public function logout(Request $request)
    {
        Auth::guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('backend.auth.index');
    }
}
