<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\Document;
use Illuminate\Support\Facades\App;

class DocumentController extends Controller
{
    public function index()
    {
        $docs = Document::where('locale', App::getLocale())->get();
        return view('beta.document')
            ->with('docs', $docs)
            ->with('locale', App::getLocale());
    }

    public function detail(Company $partner)
    {
        $tran = $partner->tran(App::getLocale());
        return view('beta.partnerDetail')
            ->with('partner', $partner)
            ->with('tran', $tran)
            ->with('locale', App::getLocale());
    }
}
