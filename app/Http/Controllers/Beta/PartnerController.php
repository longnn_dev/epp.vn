<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Company;
use Illuminate\Support\Facades\App;

class PartnerController extends Controller
{
    public function index()
    {
        $partners = Company::where('type', 'partner')->get();
        return view('beta.partner')
            ->with('partners', $partners)
            ->with('locale', App::getLocale());
    }

    public function detail(Company $partner)
    {
        $tran = $partner->tran(App::getLocale());
        return view('beta.partnerDetail')
            ->with('partner', $partner)
            ->with('tran', $tran)
            ->with('locale', App::getLocale());
    }
}
