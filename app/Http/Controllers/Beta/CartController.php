<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Model\Order;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CartController extends Controller
{
    public function partialProduct(Request $request)
    {
        $inputProducts = $request->input('products', []);
        $products = Product::whereIn('_id', $inputProducts)->get();
        return view('beta.partial.cart-product')->with(compact('products'))->with('locale',App::getLocale());
    }

    public function order(Request $request)
    {
        $inputProducts = $request->input('products', []);
        $inputInfo = $request->input('info');
        $order = new Order();
        $customer = Customer::where('email', $inputInfo['email'])->first();
        if (empty($customer)) {
            $customer = new Customer();

            $customer->email = $inputInfo['email'];
            $customer->name = $inputInfo['name'];
            $customer->phone = $inputInfo['phone'];
            $customer->save();
        }
        //product
        $products = Product::whereIn('_id', $inputProducts)->get();
        $order->customer()->associate($customer);
        $order->products()->saveMany($products);
        $order->message = $inputInfo['message'];
        $order->phone = $inputInfo['phone'];
        $order->status = 0;
        $order->save();
        return response()->json(['message' => 'success.']);
    }
}
