<?php


namespace App\Http\Controllers\Beta;


use App\Model\Product;
use App\Model\ProductTranslation;
use App\Model\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SearchController
{
    public function index(Request $request){
        $queryString = $request->get('q');
        $locale = App::getLocale();
        $query  = ProductTranslation::query();
        $query->where('name','LIKE',"%{$queryString}%");
        $query->where('locale',$locale);
        $query->limit(5);
        $trans = $query->get();
        $result = [];
        foreach($trans as $tran){
            $product = $tran->product;
            $item = new \stdClass();
            $item->name = $tran->name;
            $item->url = $product->getUrl($locale);
            $result[] = $item;
        }
        return response()->json($result);
    }
}
