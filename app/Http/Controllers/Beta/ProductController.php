<?php

namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\Product;
use Illuminate\Support\Facades\App;

class ProductController extends Controller
{
    public function details(Product $product){
        $fProducts = Product::where('type',1)->paginate(3);
        $pQuery = Company::query();
        $pQuery->select('logo');
        $pQuery->where('type','partner');
        $partners = $pQuery->get();
        $tran = $product->tran(App::getLocale());
        if(isset($tran->page)){
            $this->page = $tran->page;
        }
        return view('beta.product')
            ->with('product',$product)
            ->with('tran',$tran)
            ->with('partners',$partners)
            ->with('fProducts',$fProducts)
            ->with('locale',App::getLocale())
            ->with('meta',$this->page);
    }
}
