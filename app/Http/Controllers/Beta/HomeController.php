<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\Product;
use App\Model\ProductCategory;
use App\Model\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index(){
        $featuredProductCat = ProductCategory::where('type',1)->paginate(3);
        for($i=0;$i<count($featuredProductCat);$i++){
            $featuredProductCat[$i]->products = Product::where('category_id',$featuredProductCat[$i]->_id)->paginate(3);
        }
        $pQuery = Company::query();
        $pQuery->select('logo');
        $pQuery->where('type','partner');
        $partners = $pQuery->get();
        $slides = Slide::all();
        return view('beta.home')->with('productCat',$featuredProductCat)->with('partners',$partners)->with('locale',App::getLocale())->with('slides',$slides);
    }

    public function seLocale($locale)
    {
        Session::put('locale',$locale);
        return redirect()->route('frontend.home');
    }
}
