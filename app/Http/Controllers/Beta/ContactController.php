<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\Product;
use App\Model\ProductCategory;
use Illuminate\Support\Facades\App;

class ContactController extends Controller
{
    public function index()
    {
        $company = Company::where('type', 'main')->first();
        $tran = $company->tran(App::getLocale());
        return view('beta.contact')
        ->with('partner', $company)
        ->with('tran',$tran)
        ->with('locale',App::getLocale());
    }
}
