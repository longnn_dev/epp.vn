<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\Product;
use App\Model\ProductCategory;
use Illuminate\Support\Facades\App;

class ProductCategoryController extends Controller
{
    public function details(ProductCategory $productCategory){
        $pQuery = Product::query();
        $pQuery->where('category_id',$productCategory->_id);
        $pQuery->orderBy('updated_at','desc');
        $products = $pQuery->get();
        $fProducts = Product::where('type',1)->paginate(3);
        $pQuery = Company::query();
        $pQuery->select('logo');
        $pQuery->where('type','partner');
        $partners = $pQuery->get();
        return view('beta.productCategory')
            ->with('productCategory',$productCategory)
            ->with('products',$products)
            ->with('partners',$partners)
            ->with('fProducts',$fProducts)
            ->with('locale',App::getLocale());
    }
}
