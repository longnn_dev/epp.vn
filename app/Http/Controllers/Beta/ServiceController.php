<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\Product;
use App\Model\ProductCategory;
use Illuminate\Support\Facades\App;

class ServiceController extends Controller
{
    public function index(){
        return view('beta.service')->with('locale',App::getLocale());
    }
}
