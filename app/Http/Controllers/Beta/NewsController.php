<?php


namespace App\Http\Controllers\Beta;


use App\Http\Controllers\Controller;
use App\Model\Article;
use App\Model\ArticleCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class NewsController extends Controller
{
    public function index()
    {
        $slideArticles = Article::where('slide',true)->orderBy('orderNum','asc')->get();
        $activeCategory = ArticleCategory::where('type',1)->first();
        $activeArticles = Article::where('category_id',$activeCategory->_id)->orderBy('updated_at','desc')->paginate(10);
        $randomArticles = Article::where('category_id','!=',$activeCategory->_id)->orderBy('updated_at','desc')->limit(6)->get();
        return view('beta.news')
            ->with('category',$activeCategory)
            ->with('slides',$slideArticles)
            ->with('articles',$activeArticles)
            ->with('randomArticles',$randomArticles)
            ->with('locale',App::getLocale());
    }

    public function detail(Article $article,$title='')
    {
        $category = $article->category;
        $randomArticles = Article::where('category_id','!=',$category->_id)->orderBy('updated_at','desc')->limit(6)->get();
        $sameTopics = Article::where('category_id',$category->_id)->orderBy('updated_at','desc')->limit(3)->get();
        $previous = Article::where('_id', '<', $article->_id)->first();
        $next = Article::where('_id', '>', $article->_id)->first();
        $tran = $article->tran(App::getLocale());
        if(isset($tran->page)){
            $this->page = $tran->page;
        }
        return view('beta.news-detail')
            ->with('article',$article)
            ->with('nextPost',$next)
            ->with('previousPost',$previous)
            ->with('randomArticles',$randomArticles)
            ->with('sameTopics',$sameTopics)
            ->with('locale',App::getLocale())
            ->with('meta',$this->page);

    }

    public function partialRecent(Request $request)
    {
        $inputRecent = $request->input('recents');
        array_reverse($inputRecent);
        $recents = Article::whereIn('_id', $inputRecent)->paginate(6);
        return view('beta.partial.news-recent')->with(compact('recents'))->with('locale',App::getLocale());
    }
}
